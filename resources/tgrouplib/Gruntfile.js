module.exports = function (grunt) {
	"use strict";

	var config = {
		uglify: {
			options: {
				mangle: true,
				compress: {
					drop_console: true,
					dead_code: false,
					unused: false
				}
			},
			app: {
				files: [{
					expand: true,
					cwd: 'webapp/',
					src: ['**/*.js', '!dist/**', '!localService/**'],
					dest: 'dist/'
				}]
			}
		},
		clean: {
			dist: {
				src: ["dist/**"]
			}
		},
		copy: {
			main: {
				files: [{
					expand: true,
					cwd: 'webapp/',
					src: ["**"], //, "!**/*.js"],
					dest: "dist/",
					rename: function (sDestination, sSource) {
						if (sSource.match(/.js$/)) {
							var sFile = sSource.replace(/.js$/, "-dbg.js");
							return (sDestination + sFile);
						} else {
							return (sDestination + sSource);
						}
					}
				}]
			},
			liball: {
				files: [{
					expand: true,
					src: 'library-preload.js',
					dest: 'dist/',
					cwd: 'dist/',
					rename: function (sDestination, sSource) {
						return sDestination + "library-all.js";
					}
				}]
			}
		},
		openui5_preload: {
			library: {
				options: {
					resources: {
						cwd: "webapp",
						prefix: "com/tgroup/lib"
					},
					dest: 'dist',
					compatVersion: "1.52"
				},
				libraries: 'com/tgroup/lib'
			}
		}
	};

	grunt.config.merge(config);

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks("grunt-openui5");
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', [
		"clean",
		"copy",
		"uglify",
		"openui5_preload",
		"copy:liball"
	]);

};