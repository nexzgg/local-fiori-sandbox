sap.ui.define(["sap/ui/core/Core"],
	function(poCore) {
		"use strict";

		sap.ui.getCore().initLibrary({
			name: "com.tgroup.lib",
			dependencies: ["sap.ui.core"],
			types: [],
			interfaces: [],
			controls: [],
			elements: [],
			noLibraryCSS: true,
			version: "1.5.0"
		});

		return com.tgroup.lib;

	}, true);