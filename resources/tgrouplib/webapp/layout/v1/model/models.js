/** @module com/tgroup/lib/v1/layout */
sap.ui.define([
	"com/tgroup/lib/message/v1/model/models"
], function(poMessageModel) {
	"use strict";

	return {

		/**
		 * Read the Layouts and Fields for the app. 
		 * @public
		 * @param {String} psAppName  app name. 
		 * @param {function} pfnSuccess success function
		 * @param {function} pfnError  error function
		 */
		readLayouts: function(psAppName, pfnSuccess, pfnError) {
			// clear the messages
			poMessageModel.clearMessageModel();

			var oDataProvider = sap.ui.getCore().getModel("dataprovider");
			// Building up the map of parameters used for our gateway read
			var mParameters = {
				success: pfnSuccess,
				error: pfnError,
				urlParameters: {
					$expand: "AppToLayout/LayoutToElement/ElementToComboBox,AppToLayout/LayoutToElement/ElementToFilter,AppToTab"
				}
			};
			// starting the gateway read
			oDataProvider.read("/AppSet('" + psAppName + "')", mParameters);
		}
	};

});