/** @module com/tgroup/lib/layout */
sap.ui.define([
	"com/tgroup/lib/message/util/messageHandler",
	"com/tgroup/lib/layout/util/helper",
	"com/tgroup/lib/layout/model/models"
], function(poMessageHandler, poHelper, poModels) {
	"use strict";

	return {
		
		/**
		 * Method for reading layouts
		 * @public
		 * @param {string} psAppName The name of app
		 * @param {object} poRouter Routing object
		 * @param {string} psTarget Target for routing
		 */
		readLayouts: function(psAppName, pmSetting) {
			// success function
			var fnSuccess = (pmSetting.success) ? pmSetting.success : function(oData) {
				poHelper.convertLayout(oData, sap.ui.getCore().getModel("layout"));
				// load router from manifest and create the views based on the url/hash
				if (typeof pmSetting.router !== "undefined") {
					pmSetting.router.initialize();
				}
			};

			// error function
			var fnError = (pmSetting.error) ? pmSetting.error : function() {
				poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
			};

			poModels.readLayouts(psAppName, fnSuccess, fnError);
		}
	};
});