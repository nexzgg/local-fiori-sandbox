/** @module com/tgroup/lib/layout */
sap.ui.define([], function () {
	"use strict";

	return {

		/**
		 * method for converting the layout data
		 * @public
		 * @param {object} poData oData
		 * @param {object} poModel the json model
		 * */
		convertLayout: function (poData, poModel) {
			var oLayouts = {};
			if (poData.AppToLayout.results.length !== 0) {
				for (var i = 0; i <= poData.AppToLayout.results.length - 1; i++) {
					var oCurrentLayout = poData.AppToLayout.results[i];
					var oLayout = {};
					var sLayout = oCurrentLayout.LayoutName;
					if (oCurrentLayout.LayoutToElement.results.length !== 0) {
						var oElements = {};
						for (var j = 0; j <= oCurrentLayout.LayoutToElement.results.length - 1; j++) {
							var oCurrentElement = oCurrentLayout.LayoutToElement.results[j];
							var oElement = {};
							for (var sProperty in oCurrentElement) {
								if (oCurrentElement.hasOwnProperty(sProperty)) {
									if (sProperty !== "LayoutName" && sProperty !== "AppName") {
										// set ComboBox properties
										if (sProperty === "ElementToComboBox") {
											oElement.ComboBox = oCurrentElement[sProperty];
										} else if (sProperty === "ElementToFilter") {
											var oFilter = oCurrentElement[sProperty];
											if (oFilter && oFilter.results) {
												oElement.Filters = oFilter.results;
											}
										}
										oElement[sProperty] = oCurrentElement[sProperty];
									}
								}
							}
							var sElement = oCurrentElement.ElementName;
							oElements[sElement] = oElement;
						}
					}
					oLayout.Fields = oElements;
					oLayout.EntitySet = oCurrentLayout.EntitySet;
					oLayout.Model = oCurrentLayout.Model;
					oLayouts[sLayout] = oLayout;
				}
			}

			//Checking if we do have tabs
			if (poData.AppToTab.results.length !== 0) {
				var sTabGroup = "";
				var oTabs = {};
				var oCurrentTab;
				for (i = 0; i <= poData.AppToTab.results.length - 1; i++) {
					oCurrentTab = poData.AppToTab.results[i];
					//First get the tab groups
					if (sTabGroup !== oCurrentTab.TabGroup) {
						//Add this group to our array
						oTabs[oCurrentTab.TabGroup] = [];
						sTabGroup = oCurrentTab.TabGroup;
					}
				}
				//Now it's time to set the tabs into our groups
				for (sProperty in oTabs) {
					if (oTabs.hasOwnProperty(sProperty)) {
						for (i = 0; i <= poData.AppToTab.results.length - 1; i++) {
							oCurrentTab = poData.AppToTab.results[i];
							// tooltip JSON parse
							if (oCurrentTab.Tooltip && oCurrentTab.Tooltip !== "") {
								//Convert it
								var sTooltip = "";
								try {
									var aLines = JSON.parse(oCurrentTab.Tooltip);
									if (aLines.length !== 0) {
										for (var t = 0; t <= aLines.length - 1; t++) {
											sTooltip = sTooltip + aLines[t].TDLINE + "\n";
										}
										oCurrentTab.Tooltip = sTooltip;
									}
								} catch (oError) {
									// oError: already parsed the tooltip
								}
							}
							if (oCurrentTab.TabGroup === sProperty) {
								oTabs[sProperty].push(oCurrentTab);
							}
						}
					}
				}
			}

			// We do need to check if all relevant attributes have been provided with information
			// Otherwise we have to take the annotations
			oLayouts = this._checkAnnotations(oLayouts);
			// set app name 
			oLayouts.AppName = (poData.AppName) ? (poData.AppName) : "";
			// set also the tabs 
			if (oTabs && typeof oTabs !== "undefined") {
				oLayouts.Tabs = oTabs;
			}

			// Read navigation data
			oLayouts.Navigation = poData.AppToNavigation.results.map(function (oNavItem) {
				oNavItem.NavigationToParameter = oNavItem.NavigationToParameter.results;
				return oNavItem;
			})

			poModel.setData(oLayouts);
		},

		/**
		 * Method for filling default values in comboboxes
		 * @public
		 * @param {object} poDefaultValues The object containing our default values
		 * */
		fillDefaultValues: function (poDefaultValues) {
			var oLayoutModel = sap.ui.getCore().getModel("layout");
			var oLayoutData = oLayoutModel.getData();
			if (oLayoutData && typeof oLayoutData !== "undefined") {
				for (var sProperty in oLayoutData) {
					if (oLayoutData.hasOwnProperty(sProperty)) {
						if (typeof oLayoutData[sProperty] === 'object') {
							var oCurrentLayout = oLayoutData[sProperty];
							var oField = oCurrentLayout.Fields;
							for (var sField in oField) {
								if (oField.hasOwnProperty(sField)) {
									for (var sDefaultValue in poDefaultValues) {
										if (poDefaultValues.hasOwnProperty(sDefaultValue)) {
											if (sDefaultValue === sField) {
												var oCurrentField = oField[sField];
												if (oCurrentField.ComboBox && typeof oCurrentField.ComboBox !== "undefined") {
													oLayoutModel.setProperty("/" + sProperty + "/Fields/" + sField + "/ComboBox/DefaultValue", poDefaultValues[sDefaultValue]);
													break;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		},

		/**
		 * Method for checking if relevant field informatione have been provided.
		 * If not then we're using the annotations
		 * @private
		 * @param {object} poLayouts The object containing all the layouts
		 * @return {object} oLayouts The layout object containing all relevant field information
		 * */
		_checkAnnotations: function (poLayouts) {
			if (poLayouts && typeof poLayouts !== "undefined") {
				for (var sLayout in poLayouts) {
					if (poLayouts.hasOwnProperty(sLayout)) {
						var oCurrentLayout = poLayouts[sLayout];
						// The whole procedure is only necessary if we do have a model and an entityset provided
						if (oCurrentLayout.EntitySet !== "" && oCurrentLayout.Model !== "") {
							// If we have an entityset and a model we can create an initial object
							var oModel = sap.ui.getCore().getModel(oCurrentLayout.Model);
							var oContext = oModel.createEntry(oCurrentLayout.EntitySet);
							if (oContext && typeof oContext !== "undefined") {
								var oObject = oContext.getObject();
								if (oObject && typeof oObject !== "undefined") {
									// Let's start with looping over the field properties to find out if we do have to read the annotations
									for (var sField in oCurrentLayout.Fields) {
										if (oCurrentLayout.Fields.hasOwnProperty(sField)) {
											var oCurrentField = oCurrentLayout.Fields[sField];
											for (var sObjectField in oObject) {
												if (oObject.hasOwnProperty(sObjectField)) {
													if (sObjectField === sField) {
														for (var sProperty in oCurrentField) {
															if (oCurrentField.hasOwnProperty(sProperty)) {
																var sPath = "/" + oCurrentLayout.EntitySet + "/" + sField + "/#@";

																// Checking the decimals
																if (sProperty === "Decimals") {
																	if (oCurrentField[sProperty] === "000000") {
																		// The value is initial so let's take the annotation
																		oCurrentField[sProperty] = oModel.getProperty(sPath + "scale") ? oModel.getProperty(sPath + "scale") : "";
																	}
																}

																// Checking the maximal length
																if (sProperty === "OutputLength") {
																	if (oCurrentField[sProperty] === "000000") {
																		// The value is initial so let's take the annotation
																		oCurrentField[sProperty] = oModel.getProperty(sPath + "maxLength") ? oModel.getProperty(sPath + "maxLength") : "";
																	}

																}

																// Checking the label
																if (sProperty === "Text") {
																	if (oCurrentField[sProperty] === "") {
																		// The value is initial so let's take the annotation
																		oCurrentField[sProperty] = oModel.getProperty(sPath + "sap:label");
																	}
																}
															}
														}
													}
												}
											}
											// Adding also the type of the current field
											oCurrentField.Type = oModel.getProperty("/" + oCurrentLayout.EntitySet + "/" + sField + "/#@type");
										}
									}
								}
								oModel.deleteCreatedEntry(oContext);
							}
						}
					}
				}
			}
			return (poLayouts);
		},

		/**
		 * Method for getting an empty object from a layout
		 * @public
		 * @param {string} psLayoutName The name of the layout
		 * @return {object} oObject The object containing all the fields
		 * */
		getObjectFromLayout: function (psLayoutName) {
			var oObject;
			if (psLayoutName && typeof psLayoutName !== "undefined" && psLayoutName !== "") {
				var oLayoutModel = sap.ui.getCore().getModel("layout");
				var oFields = oLayoutModel.getProperty("/" + psLayoutName + "/Fields");
				if (oFields && typeof oFields !== "undefined") {
					oObject = {};
					for (var sProperty in oFields) {
						if (oFields.hasOwnProperty(sProperty)) {
							var oCurrentField = oFields[sProperty];
							switch (oCurrentField.InputType) {
							case "DATETIME":
								oObject[sProperty] = new Date();
								break;
							case "TIME":
								oObject[sProperty] = new Date();
								break;
							default:
								oObject[sProperty] = undefined;
							};
						}
					}
				}
			}
			return (oObject);
		}
	};
});