/** @module com/tgroup/lib/v1/general */
sap.ui.define([
	"sap/ui/core/UIComponent",
	"com/tgroup/lib/general/v1/model/models",
	"com/tgroup/lib/message/v1/model/models",
	"com/tgroup/lib/general/v1/util/helper",
	"com/tgroup/lib/controls/v1/Button",
	"com/tgroup/lib/controls/v1/CheckBox",
	"com/tgroup/lib/controls/v1/ComboBox",
	"com/tgroup/lib/controls/v1/DatePicker",
	"com/tgroup/lib/controls/v1/DynamicInput",
	"com/tgroup/lib/controls/v1/DynamicOutput",
	"com/tgroup/lib/controls/v1/FilterBar",
	"com/tgroup/lib/controls/v1/Gauge",
	"com/tgroup/lib/controls/v1/Speedmeter",
	"com/tgroup/lib/controls/v1/IconTabBar",
	"com/tgroup/lib/controls/v1/Input",
	"com/tgroup/lib/controls/v1/Label",
	"com/tgroup/lib/controls/v1/MultiComboBox",
	"com/tgroup/lib/controls/v1/MultiInput",
	"com/tgroup/lib/controls/v1/SmartFilterBar",
	"com/tgroup/lib/controls/v1/SmartTable",
	"com/tgroup/lib/controls/v1/Table",
	"com/tgroup/lib/controls/v1/TimePicker",
	"com/tgroup/lib/controls/v1/SortTable",
	"com/tgroup/lib/controls/v1/ChartContainer",
	"com/tgroup/lib/controls/v1/BusyDialog",
	"com/tgroup/lib/controls/v1/TextArea",
	"com/tgroup/lib/controls/v1/Bullet"
], function(UIComponent, poGeneralModels, poMessageModels, poGeneralHelper) {
	"use strict";
	return UIComponent.extend("com.tgroup.lib.general.v1.component.Component", {

		/**
		 * Variables and properties used in the whole component
		 * */
		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(poGeneralModels.createDeviceModel(), "device");

			// initializing the poGeneralModels we're using
			poGeneralModels.declareModels(this);

			// initializing the poMessageModels we're using
			poMessageModels.declareMessageModel(this);

			// getting the startup parameters
			this._getStartupParameters();

			// Calling the extension of the init functionality for different actions separated per application
			this.initExtend();
		},

		/**
		 * init extend function. 
		 * can be overridden in subapp.
		 * @public
		 */
		initExtend: function() {

		},

		/**
		 * Functionality for getting the startup parameters
		 * @private
		 * */
		_getStartupParameters: function() {
			var oStartupParameters = {};
			var oNavigationParameter = this.getComponentData();
			if (oNavigationParameter && oNavigationParameter.startupParameters) {
				// set first object from all properties
				for (var property in oNavigationParameter.startupParameters) {
					if (oNavigationParameter.startupParameters.hasOwnProperty(property)) {
						if (oNavigationParameter.startupParameters[property].length > 0) {
							oStartupParameters[property] = oNavigationParameter.startupParameters[property][0];
						}
					}
				}
			}

			//Checking if we do have startup parameters --> if yes set them in the general helper
			if (oStartupParameters && typeof oStartupParameters !== "undefined") {
				poGeneralHelper.setStartupParameters(oStartupParameters);
			}
		}
	});
});