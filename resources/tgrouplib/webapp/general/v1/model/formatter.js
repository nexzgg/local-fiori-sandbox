/** @module com/tgroup/lib/v1/general */
sap.ui.define([], function() {
	"use strict";

	return {
		/**
		 * Method for formatting a backend date for output
		 * @public
		 * @param {date} pdValue The date object itself
		 * @return {string} sValue The return value with the formatted date
		 * */
		formatDate: function(pdValue) {
			var sValue;
			if (pdValue) {
				var oFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
					style: "medium"
				});
				sValue = oFormat.format(pdValue);
			}
			return (sValue);
		},
		
		/**
		 * format date value to date filter something. 
		 * calculate with the timezone offset.
		 * 
		 * @param {date} pdValue The date object itself
		 * @reutrn {date} dValue the date for filter
		 */
		formatDateForFilter: function(pdValue) {
			var dValue;
			
			if (pdValue) {
				dValue = new Date(pdValue.getTime() - pdValue.getTimezoneOffset() * 60 * 1000);
			}
			
			return dValue;
		},
		
		/**
		 * method for formatting a backend time to javascript date
		 * @public
		 * @param {string} poValue Edm.Time Object
		 * @return {date} dValue date value
		 */
		formatTime: function(poValue) {
			var dValue;
			
			if (poValue && poValue.ms) {
				// add timezone offset
				dValue = new Date(poValue.ms  + new Date().getTimezoneOffset() * 1000 * 60);
			}
			
			return dValue;
		},

		/**
		 * parse string to float.
		 * @param {string} sValue the value.
		 * @return {float} nValue value as float
		 */
		parseFloat: function(sValue) {
			var nValue = 0;

			if (sValue && sValue !== "") {
				nValue = parseFloat(sValue);
				if (isNaN(nValue)) {
					nValue = 0;
				}
			}

			return nValue;
		},

		/**
		 * Dummy functionality for checking if formatter function can be accessed
		 * @public
		 * @param {string} psValue Value of a string
		 * */
		testFormatter: function(psValue) {
			return (psValue);
		}
	};
});