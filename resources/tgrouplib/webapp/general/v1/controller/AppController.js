/** @module com/tgroup/lib/v1/general */
sap.ui.define([
	"com/tgroup/lib/general/v1/controller/BaseController"
], function(poBaseController) {
	"use strict";

	return poBaseController.extend("com.tgroup.lib.general.v1.controller.AppController", {
		/**
		 * Method executed after the app has been rendered.
		 * @public
		 * */
		onAfterRendering: function() {
			//Get the message class of the application
			var sAppName = this.base.generalHelper.getStartupParameters().AppName;
			if (sAppName && typeof sAppName !== "undefined" && sAppName !== "") {
				this.base.messageClassHandler.getMessageClassList(sAppName);
			}
		}
	});

});