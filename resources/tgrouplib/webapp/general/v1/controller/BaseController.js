/** @module com/tgroup/lib/v1/general */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/tgroup/lib/general/v1/model/formatter",
	"com/tgroup/lib/general/v1/util/helper",
	"com/tgroup/lib/layout/v1/util/helper",
	"com/tgroup/lib/layout/v1/util/layoutHandler",
	"com/tgroup/lib/layout/v1/model/models",
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/general/v1/model/models",
	"com/tgroup/lib/message/v1/util/messageHandler",
	"com/tgroup/lib/message/v1/model/models",
	"com/tgroup/lib/messageClass/v1/util/messageClassHandler",
	"com/tgroup/lib/controls/v1/BusyDialog",
	"com/tgroup/lib/login/v1/util/loginHandler",
	"sap/ui/core/routing/History",
	"sap/ui/core/ComponentContainer"
], function(poController, poGeneralFormatter, poGeneralHelper, poLayoutHelper, poLayoutHandler, poLayoutModel, poControlsHelper,
	poGeneralModels, poMessageHandler, poMessageModels, poMessageClassHandler, poBusyDialog, poLoginHandler, poHistory) {
	"use strict";

	return poController.extend("com.tgroup.lib.general.v1.controller.BaseController", {
		base: {
			generalFormatter: poGeneralFormatter,
			generalHelper: poGeneralHelper,
			layoutHelper: poLayoutHelper,
			layoutHandler: poLayoutHandler,
			layoutModels: poLayoutModel,
			controlsHelper: poControlsHelper,
			generalModels: poGeneralModels,
			messageHandler: poMessageHandler,
			messageModels: poMessageModels,
			messageClassHandler: poMessageClassHandler,
			loginHandler: poLoginHandler,
			busyDialog: poBusyDialog
		},

		_oAppDialog: undefined,

		/**
		 * Method for navigation to specific view
		 * @public
		 * @param {string} psTarget Parameter containing the string for the target navigation
		 * @param {mapping} pmParameters? Parameters for navigation
		 * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry) 
		 */
		navTo: function(psTarget, pmParameters, pbReplace) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			// nav to page and set the path from the model Collection
			oRouter.navTo(psTarget, pmParameters, pbReplace);
		},

		/**
		 * Method for navigation back to specific view
		 * @public
		 * @param {string} psTarget Parameter containing the string for the target navigation
		 * @param {boolean} pbHistory? Nav back with browser history
		 */
		backTo: function(psTarget, pbHistory) {
			if (pbHistory) {
				var oHistory = poHistory.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
					window.history.go(-1);
				} else {
					this.navTo(psTarget, null, true);
				}
			} else {
				this.navTo(psTarget, null, true);
			}
		},

		/**
		 * Functionality for creating a dynamic input structure.
		 * @public
		 * @param {string} psLayout The layout containing the corresponding fields to the entity set
		 * @param {boolean} pbReadOnly input fields readonly => only output (pbReadOnly => not editable)
		 * */
		createDynamicInput: function(psLayout, pbReadOnly) {
			poBusyDialog.openBusyDialog();

			var oControl;

			var aObjects = poControlsHelper.getDynamicObjectFromLayout(psLayout);
			oControl = poControlsHelper.createDynamicInput(aObjects, psLayout, pbReadOnly);

			poBusyDialog.closeBusyDialog();

			return (oControl);
		},

		/**
		 * loop fields from layout and add one column per field.
		 * @public
		 * @param {String} psId table id
		 * @param {String} psLayoutName layout name.
		 * @param {String} psModelName model name
		 * @param {Object} poBindingInfo binding Informations
		 * @param {Boolean} pbSortable sortable
		 * @param {Boolean} pbFilterable sortable
		 * @return {Table} table
		 */
		getTableFromLayout: function(psId, psLayoutName, psModelName, poBindingInfo, pbSortable, pbFilterable) {
			return (poControlsHelper.getTableFromLayout(psId, psLayoutName, psModelName, poBindingInfo, pbSortable, pbFilterable));
		},

		/**
		 * get the f4 searchhelp fields and attributes.
		 * @public
		 * @param {String} psAppName app name
		 * @param {String} psFieldname field name
		 * @param {function} pfnCallBack function
		 * 
		 */
		getF4SearchHelp: function(psAppName, psFieldname, pfnCallBack) {
			poControlsHelper.getF4SearchHelp(psAppName, psFieldname, pfnCallBack);
		},

		/**
		 * show value help request. 
		 */
		onValueHelpRequest: function(poEvent) {
			poControlsHelper.onValueHelpRequest(poEvent.getSource(), "F4");
		}
	});

});