/** @module com/tgroup/lib/v1/general */
sap.ui.define([
	"sap/m/Dialog",
	"sap/m/Button",
	"sap/m/Text"
], function(poDialog, poButton, poText) {
	"use strict";
	return {
		_mPredefinedSettings: {
			dataLoss: {
				state: sap.ui.core.ValueState.Warning,
				title: "dataLoss.title",
				icon: "",
				dialogText: "dataLoss.dialogText",
				proceedButtonText: "dataLoss.proceedButtonText",
				cancelButtonText: "dataLoss.cancelButtonText"
			},
			deleteConfirmation: {
				state: sap.ui.core.ValueState.Warning,
				title: "deleteConfirmation.title",
				icon: "",
				dialogText: "deleteConfirmation.dialogText",
				proceedButtonText: "deleteConfirmation.proceedButtonText",
				cancelButtonText: "deleteConfirmation.cancelButtonText"
			}
		},

		/**
		 * This dialog can be used to inform the user, that there are unsaved changes and the data will be lost if he proceeds with his action.
		 * @public
		 * @param {mapping} pmSettings A map containing all dialog settings
		 * @param {sap.ui.core.mvc.View} pmSettings.view View which contains the dialog
		 * @param {requestCallback} pmSettings.fnCallbackProceed? Callback function for proceeding
		 * @param {requestCallback} pmSettings.fnCallbackCancel? Callback function for cancel
		 */
		dataLossConfirmation: function(pmSettings) {
			var mSettings = this._matchUserSettings(this._mPredefinedSettings.dataLoss, pmSettings);

			this.buildDialog(mSettings).open();
		},

		/**
		 * This dialog can be called to get a confirmation from the user in case data will be deleted.
		 * @public
		 * @param {mapping} pmSettings A map containing all dialog settings
		 * @param {sap.ui.core.mvc.View} pmSettings.view View which contains the dialog
		 * @param {requestCallback} pmSettings.fnCallbackProceed? Callback function for proceeding
		 * @param {requestCallback} pmSettings.fnCallbackCancel? Callback function for cancel
		 */
		deleteConfirmation: function(pmSettings) {
			var mSettings = this._matchUserSettings(this._mPredefinedSettings.deleteConfirmation, pmSettings);

			this.buildDialog(mSettings).open();
		},

		/**
		 * This method creates a generic dialog with the given settings
		 * @public
		 * @param {mapping} pmSettings A map containing all dialog settings
		 * @param {sap.ui.core.mvc.View} pmSettings.view View which contains the dialog
		 * @param {sap.ui.core.ValueState} pmSettings.state? State of the dialog
		 * @param {string} pmSettings.title? Title of the dialog
		 * @param {string} pmSettings.icon? Icon-Name of the dialog
		 * @param {string} pmSettings.dialogText Text? for the content
		 * @param {string} pmSettings.proceedButtonText? Text for the proceed button
		 * @param {string} pmSettings.cancelButtonText? Text for the cancel button
		 * @param {requestCallback} pmSettings.fnCallbackProceed? Callback function for proceeding
		 * @param {requestCallback} pmSettings.fnCallbackCancel? Callback function for cancel
		 * @returns {sap.m.Dialog} The generated dialog
		 */
		buildDialog: function(pmSettings) {
			var oDialog;

			oDialog = new poDialog({
				state: pmSettings.state,
				type: sap.m.DialogType.Message,
				title: pmSettings.title,
				icon: pmSettings.icon,
				content: [
					new poText({
						text: pmSettings.dialogText
					})
				],
				buttons: [
					new poButton({
						text: pmSettings.proceedButtonText,
						press: pmSettings.fnCallbackProceed.bind(this)
					}),
					new poButton({
						text: pmSettings.cancelButtonText,
						press: pmSettings.fnCallbackCancel.bind(this)
					})
				]
			});

			jQuery.sap.syncStyleClass("sapUiSizeCompact", pmSettings.view, this._oDialog);
			jQuery.sap.syncStyleClass("sapUiSizeCozy", pmSettings.view, this._oDialog);

			pmSettings.view.addDependent(this._oDialog);
			pmSettings.view.attachBeforeExit(function() {
				pmSettings.view.removeDependent(this._oDialog);
			}.bind(this));

			return oDialog;
		},
		
		/**
		 * Loads the texte elements from the properties and replace the IDs with the real texts
		 * @private
		 * @param {mapping} pmSettings The settings map
		 * @return {mapping} The settings map with the replaced text IDs
		 */
		_loadTextElements: function(pmSettings){
			var oResourceModel = new sap.ui.model.resource.ResourceModel({
					bundleUrl: jQuery.sap.getModulePath("com.tgroup.lib.general") + "/" + "i18n/i18n.properties",
					bundleLocale: sap.ui.getCore().getConfiguration().getLanguage()
				}),
				oResourceBundle = oResourceModel.getResourceBundle();
			
			pmSettings.title = oResourceBundle.getText(pmSettings.title);
			pmSettings.dialogText = oResourceBundle.getText(pmSettings.dialogText);
			pmSettings.proceedButtonText = oResourceBundle.getText(pmSettings.proceedButtonText);
			pmSettings.cancelButtonText = oResourceBundle.getText(pmSettings.cancelButtonText);
			
			return pmSettings;
		},

		/**
		 * Matches the settings from the predefined modes with the user settings
		 * @private
		 * @param {mapping} pmSettings The settings map
		 * @param {mapping} pmUserSettings The settings given from the user
		 * @return {mapping} The matched settings
		 */
		_matchUserSettings: function(pmSettings, pmUserSettings) {
			var mDialogSettings = {};
			
			pmSettings.fnCallbackProceed = function(oEvent) {
				oEvent.getSource().getParent().close();
				if (pmUserSettings.fnCallbackProceed) {
					pmUserSettings.fnCallbackProceed(oEvent);
				}
			};

			pmSettings.fnCallbackCancel = function(oEvent) {
				oEvent.getSource().getParent().close();
				if (pmUserSettings.fnCallbackCancel) {
					pmUserSettings.fnCallbackCancel(oEvent);
				}
			};
			
			pmSettings = this._loadTextElements(pmSettings);

			jQuery.extend(mDialogSettings, pmUserSettings, pmSettings);

			return mDialogSettings;
		}
	};
});