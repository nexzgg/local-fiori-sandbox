/** @module com/tgroup/lib/v1/general */
sap.ui.define([

], function() {
	"use strict";

	return {

		/**
		 * universal functions.
		 * @public
		 */
		mFunctions: {
			fnDestroy: function() {
				this.destroy();
			},
			fnClose: function() {
				this.close();
			}
		},

		/**
		 * Local variables used in the helper
		 * */
		_oStartupParamters: undefined,

		/**
		 * Helper for copying a javascript object
		 * @public
		 * @param {object} poCopy The copy which should be copyied
		 * @return {object} oPaste The object to be returned
		 */
		copyObject: function(poCopy) {
			var oPaste = null;
			try {
				if (poCopy) {
					oPaste = JSON.parse(JSON.stringify(poCopy));
				}
			} catch (e) {
				jQuery.sap.log.error(e);
			}
			return oPaste;
		},

		/**
		 * get object first match of object.
		 * check equals of properties.
		 * @public
		 * @param {array} paList array of object
		 * @param {map} pmProperty object of properties and values 
		 * @return {object} oObject the searched object
		 */
		getObject: function(paList, pmProperty) {
			var oObject;
			try {
				if (paList && pmProperty) {
					for (var i = 0; i < paList.length; i++) {
						var bMatch = true;
						var oObj = paList[i];
						for (var property in pmProperty) {
							if (oObj.hasOwnProperty(property) || oObj.getProperty) {
								if (oObj[property] !== pmProperty[property] &&
									(!oObj.getProperty || oObj.getProperty && oObj.getProperty(property) !== pmProperty[property])) {
									bMatch = false;
									break;
								}
							} else {
								bMatch = false;
								break;
							}
						}
						// object matches with the properties
						if (bMatch) {
							oObject = oObj;
							break;
						}
					}
				}
			} catch (oEx) {
				jQuery.sap.log.error(oEx);
			}

			return oObject;
		},

		/**
		 * get the model and the path which we want to remove from a list.
		 * @public
		 * @param {string} psPathArray array path in model
		 * @param {string} psPathRemove path from object we want to remove from the array
		 * @param {object} poModel the model
		 */
		removeObjectFromModelArray: function(psPathArray, psPathRemove, poModel) {
			if (poModel) {
				// set object which we want to remove undefined.
				var aArray = poModel.getProperty(psPathArray);
				if (aArray && aArray.length > 0) {
					poModel.setProperty(psPathRemove, undefined);
					// remove undefined objects
					aArray = aArray.filter(function(poEntry) {
						return poEntry !== undefined;
					});
					poModel.setProperty(psPathArray, aArray);
				}
			}
		},

		/**
		 * Functionality for getting the date in the right format as string for the backend
		 * @public
		 * @param {object} poDate The date object to be formatted
		 * @return {string} sDate Date in format YYYYMMDD
		 * */
		getDateBackend: function(poDate) {
			if (poDate) {
				var iYear, iMonth, iDay, sDate;
				iYear = poDate.getFullYear();
				iMonth = this._getLeadingZeroes(poDate.getMonth() + 1);
				iDay = this._getLeadingZeroes(poDate.getDate());
				sDate = iYear.toString() + iMonth.toString() + iDay.toString();

				return (sDate);
			}
		},

		/**
		 * Functionality for getting the time in the right format as string for the backend
		 * @public
		 * @param {object} poTime The time object to be formatted
		 * @return {string} sTime Time in format HHMMSS
		 * */
		getTimeBackend: function(poTime) {
			if (poTime) {
				var iHours, iMinutes, iSeconds, sTime;
				iHours = poTime.getHours();
				iMinutes = this._getLeadingZeroes(poTime.getMinutes());
				iSeconds = this._getLeadingZeroes(poTime.getSeconds());
				sTime = iHours.toString() + iMinutes.toString() + iSeconds.toString();

				return (sTime);
			}
		},

		/**
		 * Internal functionality for checking if we need leading zeroes for numbers <= 9
		 * @private
		 * @param {integer} piValue The number to be checked
		 * @return {integer} piValue The formatted number
		 * */
		_getLeadingZeroes: function(piValue) {
			var iValue;
			if (piValue) {
				if (piValue <= 9) {
					iValue = 0 + piValue.toString();
				} else {
					iValue = piValue;
				}
			}
			return (iValue);
		},

		/**
		 * Functionality for setting the startup parameters provided at the beginning
		 * @public
		 * @param {object} poStartupParameters The object storing all our values
		 * */
		setStartupParameters: function(poStartupParameters) {
			this._oStartupParameters = poStartupParameters;
		},

		/**
		 * Functionality for getting all our startup parameters 
		 * @public
		 * @return {object} this._oStartupParameters The local object storing all our parameters
		 * */
		getStartupParameters: function() {
			return (this._oStartupParameters);
		},

		/**
		 * close myself.
		 * @public
		 * @param {Control} poControl control
		 */
		close: function(poControl) {
			if (poControl && poControl.close) {
				poControl.close();
			}
		},

		/**
		 * destroy myself. 
		 * @public
		 * @param {Control} poControl control
		 */
		destroy: function(poControl) {
			if (poControl && poControl.destroy) {
				poControl.destroy();
			}
		},

		/**
		 * get base64 string of file.
		 * @public
		 * @param {control} poFileUploader file uploader control
		 * @param {function} pfnCallback callback function. get the base64 string
		 */
		getFileBase64: function(poFile, pfnCallBack) {
			var sBase64 = "";
			if (poFile) {
				var oReader = new FileReader();
				// Creating new file reader and convert the whole thing
				// to a base64 string
				oReader.onload = function(poData) {
					sBase64 = poData.target.result;
					// callback
					if (pfnCallBack && typeof pfnCallBack === "function") {
						pfnCallBack(sBase64, poFile.type);
					}
				};
				oReader.readAsDataURL(poFile);
			} else {
				// callback
				if (pfnCallBack && typeof pfnCallBack === "function") {
					pfnCallBack(sBase64, poFile.type);
				}
			}

		}
	};
});