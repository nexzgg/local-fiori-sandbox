/** @module com/tgroup/lib/general */
sap.ui.define([
	"sap/ui/core/UIComponent",
	"com/tgroup/lib/general/model/models",
	"com/tgroup/lib/message/model/models",
	"com/tgroup/lib/general/util/helper",
	"com/tgroup/lib/controls/Button",
	"com/tgroup/lib/controls/CheckBox",
	"com/tgroup/lib/controls/ComboBox",
	"com/tgroup/lib/controls/DatePicker",
	"com/tgroup/lib/controls/DynamicInput",
	"com/tgroup/lib/controls/DynamicOutput",
	"com/tgroup/lib/controls/FilterBar",
	"com/tgroup/lib/controls/FeedListItem",
	"com/tgroup/lib/controls/Gauge",
	"com/tgroup/lib/controls/Speedmeter",
	"com/tgroup/lib/controls/IconTabBar",
	"com/tgroup/lib/controls/Input",
	"com/tgroup/lib/controls/Label",
	"com/tgroup/lib/controls/Link",
	"com/tgroup/lib/controls/MultiComboBox",
	"com/tgroup/lib/controls/MultiInput",
	"com/tgroup/lib/controls/SmartFilterBar",
	"com/tgroup/lib/controls/SmartTable",
	"com/tgroup/lib/controls/Table",
	"com/tgroup/lib/controls/Text",
	"com/tgroup/lib/controls/TimePicker",
	"com/tgroup/lib/controls/SortTable",
	"com/tgroup/lib/controls/ChartContainer",
	"com/tgroup/lib/controls/BusyDialog",
	"com/tgroup/lib/controls/TextArea",
	"com/tgroup/lib/controls/Bullet",
	"com/tgroup/lib/controls/RichTextEditor",
	"com/tgroup/lib/controls/Toolbar"
], function (UIComponent, poGeneralModels, poMessageModels, poGeneralHelper) {
	"use strict";
	return UIComponent.extend("com.tgroup.lib.general.component.Component", {

		/**
		 * Variables and properties used in the whole component
		 * */
		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// polyfills
			this._initPolyfills();

			// set the device model
			this.setModel(poGeneralModels.createDeviceModel(), "device");

			// initializing the poGeneralModels we're using
			poGeneralModels.declareModels(this);

			// initializing the poMessageModels we're using
			poMessageModels.declareMessageModel(this);

			// getting the startup parameters
			this._getStartupParameters();

			poGeneralHelper.setComponent(this);

			// Calling the extension of the init functionality for different actions separated per application
			this.initExtend();
		},

		/** 
		 * init poly fills. e. g. startwith / endswith
		 * @public
		 */
		_initPolyfills: function () {
			if (!String.prototype.endsWith) {
				String.prototype.endsWith = function (searchString, position) {
					var subjectString = this.toString();
					if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
						position = subjectString.length;
					}
					position -= searchString.length;
					var lastIndex = subjectString.indexOf(searchString, position);
					return lastIndex !== -1 && lastIndex === position;
				};
			}
			if (!String.prototype.startsWith) {
				String.prototype.startsWith = function (searchString, position) {
					position = position || 0;
					return this.substr(position, searchString.length) === searchString;
				};
			}
		},

		/**
		 * init extend function. 
		 * can be overridden in subapp.
		 * @public
		 */
		initExtend: function () {

		},

		/**
		 * Functionality for getting the startup parameters
		 * @private
		 * */
		_getStartupParameters: function () {
			var oStartupParameters = {};
			var oNavigationParameter = this.getComponentData();
			if (oNavigationParameter && oNavigationParameter.startupParameters) {
				// set first object from all properties
				for (var property in oNavigationParameter.startupParameters) {
					if (oNavigationParameter.startupParameters.hasOwnProperty(property)) {
						if (oNavigationParameter.startupParameters[property].length > 0) {
							oStartupParameters[property] = oNavigationParameter.startupParameters[property][0];
						}
					}
				}
			}

			//Checking if we do have startup parameters --> if yes set them in the general helper
			if (oStartupParameters && typeof oStartupParameters !== "undefined") {
				poGeneralHelper.setStartupParameters(oStartupParameters);
			}
		}
	});
});