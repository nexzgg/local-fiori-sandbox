/** @module com/tgroup/lib/general */
sap.ui.define([
	"com/tgroup/lib/general/controller/BaseController"
], function(poBaseController) {
	"use strict";

	return poBaseController.extend("com.tgroup.lib.general.controller.AppController", {
		/**
		 * Method executed after the app has been rendered.
		 * @public
		 * */
		onAfterRendering: function() {
			//Get the message class of the application
			var sAppName = this.base.generalHelper.getStartupParameters().AppName;
			if (sAppName && typeof sAppName !== "undefined" && sAppName !== "") {
				this.base.messageClassHandler.getMessageClassList(sAppName);
			}
		}
	});

});