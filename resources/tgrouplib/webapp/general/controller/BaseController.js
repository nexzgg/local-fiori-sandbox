/** @module com/tgroup/lib/general */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/tgroup/lib/general/model/formatter",
	"com/tgroup/lib/general/util/helper",
	"com/tgroup/lib/layout/util/helper",
	"com/tgroup/lib/layout/util/layoutHandler",
	"com/tgroup/lib/layout/model/models",
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/general/model/models",
	"com/tgroup/lib/message/util/messageHandler",
	"com/tgroup/lib/message/util/BatchMessageParser",
	"com/tgroup/lib/message/model/models",
	"com/tgroup/lib/messageClass/util/messageClassHandler",
	"com/tgroup/lib/controls/BusyDialog",
	"com/tgroup/lib/login/util/loginHandler",
	"sap/ui/core/routing/History",
	"com/tgroup/lib/variants/util/variantHandler",
	"sap/ui/core/ComponentContainer"
], function (poController, poGeneralFormatter, poGeneralHelper, poLayoutHelper, poLayoutHandler, poLayoutModel, poControlsHelper,
	poGeneralModels, poMessageHandler, BatchMessageParser, poMessageModels, poMessageClassHandler, poBusyDialog, poLoginHandler, poHistory,
	poVariantHandler) {
	"use strict";

	return poController.extend("com.tgroup.lib.general.controller.BaseController", {
		base: {
			generalFormatter: poGeneralFormatter,
			generalHelper: poGeneralHelper,
			layoutHelper: poLayoutHelper,
			layoutHandler: poLayoutHandler,
			layoutModels: poLayoutModel,
			controlsHelper: poControlsHelper,
			generalModels: poGeneralModels,
			messageHandler: poMessageHandler,
			messageModels: poMessageModels,
			messageClassHandler: poMessageClassHandler,
			messageParser: new BatchMessageParser(),
			loginHandler: poLoginHandler,
			busyDialog: poBusyDialog,
			variantHandler: poVariantHandler
		},

		_oAppDialog: undefined,
		_filesToUpload: 0,
		_filesUploaded: 0,

		onInit: function () {
			/*
			 * Adds a json parser which can convert string dates to js date objects
			 */
			if (!this.base.JSON) {
				var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
				var reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

				var dateParser = function (key, value) {
					if (typeof value === 'string') {
						var a = reISO.exec(value);
						if (a)
							return new Date(value);
						a = reMsAjax.exec(value);
						if (a) {
							var b = a[1].split(/[-+,.]/);
							return new Date(b[0] ? +b[0] : 0 - +b[1]);
						}
					}
					return value;
				};
				this.base.JSON = {
					dateParser: dateParser
				};
			}

			if (!Object.values) {
				Object.values = function objectValues(obj) {
					var res = [];
					for (var i in obj) {
						if (obj.hasOwnProperty(i)) {
							res.push(obj[i]);
						}
					}
					return res;
				}
			}

		},

		/**
		 * Method for navigation to specific view
		 * @public
		 * @param {string} psTarget Parameter containing the string for the target navigation
		 * @param {mapping} pmParameters? Parameters for navigation
		 * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry) 
		 */
		navTo: function (psTarget, pmParameters, pbReplace) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			// nav to page and set the path from the model Collection
			oRouter.navTo(psTarget, pmParameters, pbReplace);
		},

		/**
		 * Method for navigation back to specific view
		 * @public
		 * @param {string} psTarget Parameter containing the string for the target navigation
		 * @param {boolean} pbHistory? Nav back with browser history
		 */
		backTo: function (psTarget, pbHistory) {
			if (pbHistory) {
				var oHistory = poHistory.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
					window.history.go(-1);
				} else {
					this.navTo(psTarget, null, true);
				}
			} else {
				this.navTo(psTarget, null, true);
			}
		},

		/**
		 * Functionality for creating a dynamic input structure.
		 * @public
		 * @param {string} psLayout The layout containing the corresponding fields to the entity set
		 * @param {boolean} pbReadOnly input fields readonly => only output (pbReadOnly => not editable)
		 * */
		createDynamicInput: function (psLayout, pbReadOnly) {
			poBusyDialog.openBusyDialog();

			var oControl;

			var aObjects = poControlsHelper.getDynamicObjectFromLayout(psLayout);
			oControl = poControlsHelper.createDynamicInput(aObjects, psLayout, pbReadOnly);

			poBusyDialog.closeBusyDialog();

			return (oControl);
		},

		/**
		 * loop fields from layout and add one column per field.
		 * @public
		 * @param {String} psId table id
		 * @param {String} psLayoutName layout name.
		 * @param {String} psModelName model name
		 * @param {Object} poBindingInfo binding Informations
		 * @param {Boolean} pbSortable sortable
		 * @param {Boolean} pbFilterable sortable
		 * @return {Table} table
		 */
		getTableFromLayout: function (psId, psLayoutName, psModelName, poBindingInfo, pbSortable, pbFilterable) {
			return (poControlsHelper.getTableFromLayout(psId, psLayoutName, psModelName, poBindingInfo, pbSortable, pbFilterable));
		},

		/**
		 * get the f4 searchhelp fields and attributes.
		 * @public
		 * @param {String} psAppName app name
		 * @param {String} psFieldname field name
		 * @param {function} pfnCallBack function
		 * 
		 */
		getF4SearchHelp: function (psAppName, psFieldname, pfnCallBack) {
			poControlsHelper.getF4SearchHelp(psAppName, psFieldname, pfnCallBack);
		},

		/**
		 * show value help request. 
		 */
		onValueHelpRequest: function (poEvent) {
			poControlsHelper.onValueHelpRequest(poEvent.getSource(), "F4");
		},

		/**
		 * Toggle fullscreen in fiori launchpad.
		 */
		onToggleFullscreen: function () {
			// Lauchpad UI5 1.48
			var oShell1 = $('[id^="application-"][id$="-display"],[id^="application-"][id$="-url"]');
			oShell1 && oShell1.hasClass('sapUShellApplicationContainerLimitedWidth') ? oShell1.removeClass(
				'sapUShellApplicationContainerLimitedWidth') : oShell1.addClass('sapUShellApplicationContainerLimitedWidth');

			// Stand alone UI5 1.28
			var oShell2 = $('.sapMShell');
			oShell2 && oShell2.hasClass('sapMShellAppWidthLimited') ? oShell2.removeClass('sapMShellAppWidthLimited') : oShell2.addClass(
				'sapMShellAppWidthLimited');
		},

		/**
		 * file size exceeded. 
		 * @public
		 * @param {object} poEvent event
		 */
		onFileSizeExceeded: function (poEvent) {
			var oMessage = {
				Type: "E",
				Class: "/CAT/5BFW",
				Msgnr: "017",
				Param1: poEvent.getSource().getMaximumFileSize()
			};
			this.base.messageHandler.addMessage(oMessage, sap.ui.getCore().getModel("message"), sap.ui.getCore().getModel("messageClass"), true);
		},

		/**
		 * file length exceeded.
		 * @public
		 * @param {object} poEvent event
		 */
		onFilenameLengthExceed: function (poEvent) {
			var oMessage = {
				Type: "E",
				Class: "/CAT/5BFW",
				Msgnr: "018",
				Param1: poEvent.getSource().getMaximumFilenameLength()
			};
			this.base.messageHandler.addMessage(oMessage, sap.ui.getCore().getModel("message"), sap.ui.getCore().getModel("messageClass"), true);
		},

		/**
		 * before upload to backend started
		 * set slug filename and head id
		 * @public
		 * @param {object} poEvent event
		 * @param {string} psAdditionalSlug additional slug data
		 */
		onBeforeUploadStarts: function (poEvent, psAdditionalSlug) {
			var oMessage = {
				Type: "S",
				ID: "/CAT/5BFW",
				Number: "019"
			};

			this.iAmBusy(0, this.base.messageClassHandler.getMessageText(oMessage));
			var sSlug = psAdditionalSlug ? poEvent.getParameter("fileName") + "|" + psAdditionalSlug : poEvent.getParameter("fileName");
			var oCustomerHeader = new sap.m.UploadCollectionParameter({
				name: "slug",
				value: encodeURIComponent(sSlug)
			});
			poEvent.getParameters().addHeaderParameter(oCustomerHeader);
		},

		/**
		 * on change event set csrf token to header.
		 * @public
		 * @param {object} poEvent event
		 * @param {string} psCsrfToken x-csrf-token
		 */
		onChange: function (poEvent, psCsrfToken) {
			// add csrf token
			this._filesToUpload = poEvent.getParameter("files").length;
			var oUploadCollection = poEvent.getSource();
			var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
				name: "x-csrf-token",
				value: psCsrfToken
			});
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
		},

		/**
		 * upload collection upload completed. refresh the attachments.
		 * @public
		 */
		onUploadComplete: function (poEvent, pfnCallback) {
			this._filesUploaded++;
			if (this._filesToUpload === this._filesUploaded) {
				this._filesToUpload = 0;
				this._filesUploaded = 0;
				this.iAmNotBusy();
				if (pfnCallback && typeof pfnCallback === "function") {
					pfnCallback();
				}
			}
		},

		/**
		 * attach http events on sb time model
		 */
		attachRequestEvents: function (poModel) {
			if (poModel) {
				poModel.attachRequestSent(function () {
					this.iAmBusy();
				}.bind(this));
				poModel.attachRequestCompleted(function () {
					this.iAmNotBusy();
				}.bind(this));
			}
		},

		/**
		 * Deactivates the global busy indicator.
		 *
		 * @memberof de.teamcon.core.controller.BaseController
		 * @instance
		 */
		iAmNotBusy: function () {
			sap.ui.core.BusyIndicator.hide();
		},

		/**
		 * Activates the global busy indicator.
		 *
		 * @param {int} [iDelay=1000] The delay in milliseconds
		 * @param {string} [sText] The text shown in the global indicator (can also be HTML)
		 * @returns {object} The DOM element with the sText content
		 * @memberof de.teamcon.core.controller.BaseController
		 * @instance
		 */
		iAmBusy: function (iDelay, sText) {
			sap.ui.core.BusyIndicator.show(iDelay);

			// Create global indicator text
			var oTitle = jQuery("#sapUiBusyIndicatorGlobalText");
			if (oTitle.size() === 0) {
				oTitle = jQuery("<div>").attr("id", "sapUiBusyIndicatorGlobalText").css({
					"font-size": "23px",
					"color": "white",
					"padding": "35px 15px 15px 15px",
					"position": "absolute",
					"top": "50%",
					"left": "0",
					"right": "0",
					"text-align": "center"
				}).appendTo(jQuery("#sapUiBusyIndicator > .sapUiLocalBusyIndicator"));
			}
			oTitle.html(sText ? sText : "");
			return oTitle;
		}
	});

});