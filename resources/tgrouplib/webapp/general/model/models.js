/** @module com/tgroup/lib/general */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(poJSONModel, poDevice) {
	"use strict";

	return {

		/**
		 * create the poDeviceModel.
		 * @returns {poJSONModel} oModel
		 */
		createDeviceModel: function() {
			var oModel = new poJSONModel(poDevice);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		/**
		 * General function responsible for creating and initializing all models
		 * @public
		 * @param {object} poComponent The complete component for setting models to it
		 * */
		declareModels: function(poComponent) {
			if (poComponent && typeof poComponent !== "undefined") {
				// setting models available for the core
				var oCore = sap.ui.getCore();
				oCore.setModel(poComponent.getModel("dataprovider"), "dataprovider");
				oCore.setModel(poComponent.getModel("layout"), "layout");
				oCore.setModel(poComponent.getModel("F4Layout"), "F4Layout");
				oCore.setModel(poComponent.getModel("F4"), "F4");
				oCore.setModel(poComponent.getModel("messageClass"), "messageClass");
				if (poComponent.getModel("appState")) {
					oCore.setModel(poComponent.getModel("appState"), "appState");
				}
				if (poComponent.getModel("currUser")) {
					oCore.setModel(poComponent.getModel("currUser"), "currUser");
				}
			}
		},

		/**
		 * create object odata create. 
		 * @param {Object} poObject object
		 * @param {Model} poModel odata model
		 * @param {String} psSet set
		 * @param {function} pfnSuccess success function
		 * @param {function} pfnError error function
		 */
		createObject: function(poObject, poModel, psSet, pfnSuccess, pfnError) {
			var mParameters = {
				success: pfnSuccess,
				error: pfnError
			};
			poModel.create(psSet, poObject, mParameters);
		}
	};

});