/* global File Blob */
sap.ui.define([
	"jquery.sap.global",
	"sap/ui/base/Metadata",
	"sap/ui/base/Interface"
], function(jQuery, Metadata, Interface) {
	"use strict";

	/**
	 * @class ImageHandler
	 * The ImageHandler class provides image-handling functionality. There exists also a class to
	 * manage uploads. For this, have a look at {@link de.tserv.core.objects.UploadHandler}. The
	 * class initialization itself holds no properties and only provides instance methods.
	 * 
	 * @extends external:"sap.ui.base.Metadata"
	 * @alias de.tserv.core.extends.ImageHandler
	 * @todo Move to ./objects
	 */
	var ImageHandler = Metadata.createClass("com.tgroup.lib.general.util.ImageHandler", {
		constructor: function() {
			// Silence is golden.
		}
	});

	/**
	 * Checks, if a given file is an image.
	 * 
	 * @param {File|Blob} oFile The file object
	 * @returns {boolean}
	 */
	ImageHandler.prototype.isImage = function(oFile) {
		if (oFile && oFile.type) {
			var aValid = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
			return jQuery.inArray(oFile.type, aValid) > -1;
		} else {
			return false;
		}
	};

	/**
	 * Converts a base64 encoded string to a Blob object which can also be used like a File object.
	 * 
	 * @param {string} sUri The base64 string
	 * @param {boolean} [bReturnMime] If true the function will return a string of type instead of a blob 
	 * @returns {Blob|string}
	 * @see http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
	 */
	ImageHandler.prototype.dataUriToBlob = function(sUri, bReturnMime) {
		// convert base64/URLEncoded data component to raw binary data held in a string
		var byteString;
		if (sUri.split(",")[0].indexOf("base64") >= 0) {
			byteString = atob(sUri.split(',')[1]);
		} else {
			byteString = unescape(sUri.split(',')[1]);
		}
		// separate out the mime component
		var mimeString = sUri.split(',')[0].split(':')[1].split(';')[0];
		if (bReturnMime) {
			return mimeString;
		}

		// write the bytes of the string to a typed array
		var ia = new Uint8Array(byteString.length);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}

		return new Blob([ia], {
			type: mimeString
		});
	};

	/**
	 * Start to compress an image. Please consider that this function does not fix the orientation. For this purposes you should
	 * use the fixImage() method before!
	 * 
	 * @param {string|File|Blob} mSrc The image, can also be an URI or base64 string
	 * @param {int} iFitToWidth The maximum width (px) of the image
	 * @param {float} dQuality 0.5 for 50% quality, recommenend is 0.8
	 * @param {function} onSuccess Success handler (params: result in base64 code, total bytes, compression details (!if originalFilesize is setted), mime type
	 * @param {function} [onError] on error handler
	 * @param {float} [dOriginalFilesize] original file size to generate compression details (optional)
	 * @param {int} [iFitToHeight] The maximum height (px) of the image	 
	 */
	ImageHandler.prototype.compress = function(mSrc, iFitToWidth, dQuality, onSuccess, onError, dOriginalFilesize, iFitToHeight) {
		jQuery.when(this._create(mSrc)).then(function(oImage, sMimeType) {
			var sBase64 = this._resize(oImage, iFitToWidth, dQuality, sMimeType, iFitToHeight),
				dTotal = (sBase64.length * 3) / 4 - (sBase64.match(/\=/g) || []).length,
				sCompressionDetails = {};

			if (typeof dOriginalFilesize !== "undefined" && dTotal < dOriginalFilesize) {
				sCompressionDetails.originalSize = this.humanFileSize(dOriginalFilesize, true);
				sCompressionDetails.resultSize = this.humanFileSize(dTotal, true);
				sCompressionDetails.imageQuality = (dQuality * 100) + "%";
				sCompressionDetails.savedPercent = (100.0 - (dTotal / dOriginalFilesize * 100)).toFixed(2) + "%";
			}
			onSuccess(sBase64, dTotal, sCompressionDetails, sMimeType);
		}.bind(this), onError || function() {
			jQuery.sap.log.error(arguments);
		});
	};

	/**
	 * Convert bytes to a human readable string.
	 * 
	 * @param {float} dBytes The size in bytes
	 * @param {boolean} [bSi] Determines if 1000 byte or 1024 tresh is used
	 * @returns {string} Human readable string
	 * @see http://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable
	 */
	ImageHandler.prototype.humanFileSize = function(dBytes, bSi) {
		var iTresh = bSi ? 1000 : 1024;
		if (Math.abs(dBytes) < iTresh) {
			return dBytes + " B";
		}
		var aUnits = bSi ? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"] : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
		var u = -1;
		do {
			dBytes /= iTresh;
			++u;
		} while (Math.abs(dBytes) >= iTresh && u < aUnits.length - 1);
		return dBytes.toFixed(1) + " " + aUnits[u];
	};

	/**
	 * Resets the image orientation if it is an image.
	 * 
	 * @param {Blob|File} oFile The file
	 * @returns {Deferred} Deferred will always be resolved with a valid blob
	 * @see de.tserv.core.extends.ImageHandler#extractOrientation
	 * @see de.tserv.core.extends.ImageHandler#resetOrientation
	 */
	ImageHandler.prototype.fixImage = function(oFile) {
		var oDef = jQuery.Deferred();
		if (this.isImage(oFile)) {
			this.extractOrientation(oFile).done(function(iOrientation) {
				this.resetOrientation(oFile, iOrientation).done(oDef.resolve);
			}.bind(this));
		} else {
			oDef.resolve(oFile);
		}
		return oDef;
	};

	/**
	 * Resets the orientation of a given file.
	 * 
	 * @param {Blob|File} oFile The image
	 * @param {int} iOrientation The orientation extracted with the extractOrientation method
	 * @returns {Deferred} Deferred will always be resolved with a valid blob
	 * @see de.tserv.core.extends.ImageHandler#extractOrientation
	 * @see http://stackoverflow.com/questions/20600800/js-client-side-exif-orientation-rotate-and-mirror-jpeg-images/40867559#40867559
	 */
	ImageHandler.prototype.resetOrientation = function(oFile, iOrientation) {
		var img = new Image(),
			reader = new FileReader(),
			oDef = jQuery.Deferred();

		// Is there a valid orientation?
		if (iOrientation < 0) {
			oDef.resolve(oFile);
		}

		reader.onload = function(event) {
			img.onload = function() {
				var width = img.width,
					height = img.height,
					canvas = document.createElement("canvas"),
					ctx = canvas.getContext("2d");

				// set proper canvas dimensions before transform & export
				if ([5, 6, 7, 8].indexOf(iOrientation) > -1) {
					canvas.width = height;
					canvas.height = width;
				} else {
					canvas.width = width;
					canvas.height = height;
				}

				// transform context before drawing image
				switch (iOrientation) {
					case 2:
						ctx.transform(-1, 0, 0, 1, width, 0);
						break;
					case 3:
						ctx.transform(-1, 0, 0, -1, width, height);
						break;
					case 4:
						ctx.transform(1, 0, 0, -1, 0, height);
						break;
					case 5:
						ctx.transform(0, 1, 1, 0, 0, 0);
						break;
					case 6:
						ctx.transform(0, 1, -1, 0, height, 0);
						break;
					case 7:
						ctx.transform(0, -1, -1, 0, height, width);
						break;
					case 8:
						ctx.transform(0, -1, 1, 0, 0, width);
						break;
					default:
						ctx.transform(1, 0, 0, 1, 0, 0);
				}

				// draw image
				ctx.drawImage(img, 0, 0);

				// Export as blob
				oDef.resolve(this.dataUriToBlob(canvas.toDataURL()));
			}.bind(this);

			// Create the image
			img.src = event.target.result;
		}.bind(this);

		// Read the file
		reader.readAsDataURL(oFile);
		return oDef;
	};

	/**
	 * Extracts the orientation of a file.
	 * 
	 * @param {File|Blob} oFile The image
	 * @returns {Deferred}    The deferred always gets resolved with a given orientation number. For references have a look at the jsfiddle.
	 *                                                                                       If there is no orientation or an error occured, the deferred is resolved with -1 or -2
	 * @see https://jsfiddle.net/wunderbart/dtwkfjpg/
	 */
	ImageHandler.prototype.extractOrientation = function(oFile) {
		var reader = new FileReader(),
			oDef = jQuery.Deferred();

		// Browser supports data view?
		if (!DataView) {
			oDef.resolve(-1);
		}

		reader.onload = function(event) {
			var view = new DataView(event.target.result);

			if (view.getUint16(0, false) != 0xFFD8) {
				oDef.resolve(-2);
			}

			var length = view.byteLength,
				offset = 2;
			while (offset < length) {
				var marker = view.getUint16(offset, false);
				offset += 2;
				if (marker === 0xFFE1) {
					if (view.getUint32(offset += 2, false) != 0x45786966) {
						oDef.resolve(-1);
					}
					var little = view.getUint16(offset += 6, false) == 0x4949;
					offset += view.getUint32(offset + 4, little);
					var tags = view.getUint16(offset, little);
					offset += 2;

					for (var i = 0; i < tags; i++) {
						if (view.getUint16(offset + (i * 12), little) == 0x0112) { //NOSONAR
							oDef.resolve(view.getUint16(offset + (i * 12) + 8, little));
						}
					}
				} else if ((marker & 0xFF00) != 0xFF00) {
					break;
				} else {
					offset += view.getUint16(offset, false);
				}
			}
			oDef.resolve(-1);
		};

		reader.readAsArrayBuffer(oFile);
		return oDef;
	};

	/**
	 * Resizes a HTML image object.
	 * 
	 * @param {object} oImage Image object (new window.Image())
	 * @param {integer} iFitToWidth The maximum width (px) of the image

	 * @param {float} [dQuality=0.8] 0.5 for 50% quality, recommenend is 0.8
	 * @param {string} [sMimeType="image/jpeg"] The mime type for the base64 output
	 * @param {integer} [iFitToHeight] The maximum height (px) of the image 
	 * @returns {string} Data URL for image (base64)
	 */
	ImageHandler.prototype._resize = function(oImage, iFitToWidth, dQuality, sMimeType, iFitToHeight) {
		var oCanvas = document.createElement("canvas"),
			oContext = oCanvas.getContext("2d");

		var iResultWidth = oImage.width,
			iResultHeight = oImage.height;	
		if (iResultWidth > iFitToWidth) {
			// Resize it
			var dPercentage = iFitToWidth / iResultWidth * 100;
			iResultWidth = iFitToWidth;
			iResultHeight = iResultHeight / 100 * dPercentage;
		}
		if(iFitToHeight > 0 && iFitToHeight < iResultHeight) {
			dPercentage = iFitToHeight / iResultHeight * 100;
			iResultHeight = iFitToHeight;
			iResultWidth = iResultWidth / 100 * dPercentage;
		}
		// Draw the image
		oCanvas.width = iResultWidth;
		oCanvas.height = iResultHeight;
		oContext.drawImage(oImage, 0, 0, iResultWidth, iResultHeight);
		return oCanvas.toDataURL(sMimeType || "image/jpeg", dQuality || 0.8);
	};

	/**
	 * Creates an Image object.
	 * 
	 * @param {string|File|Blob} mSrc Image url or base64 coded string or File object
	 * @returns {Deferred} jQuery Deferred object which gets resolved with window.Image object and MIME type
	 */
	ImageHandler.prototype._create = function(mSrc) {
		var oPromise = jQuery.Deferred(),
			oImage = new Image(),
			fInitImage = function(src, sMimeType) {
				oImage.onload = function() {
					oPromise.resolve(oImage, sMimeType);
				};
				oImage.src = src;
			};

		// Determine file type
		if (mSrc instanceof File || mSrc instanceof Blob) {
			var reader = new FileReader();
			reader.onload = function(event) {
				fInitImage(event.target.result, mSrc.type);
			};
			reader.readAsDataURL(mSrc);
		} else {
			fInitImage(mSrc, this.dataUriToBlob(mSrc, true));
		}
		return oPromise.promise();
	};

	/*
	 * Returns the public interface of the object.
	 *
	 * @return {sap.ui.base.Interface} the public interface of the object
	 * @public
	 */
	ImageHandler.prototype.getInterface = function() {
		// New implementation that avoids the overhead of a dedicated member for the interface
		// initially, an Object instance has no associated Interface and the getInterface
		// method is defined only in the prototype. So the code here will be executed.
		// It creates an interface (basically the same code as in the old implementation)
		var oInterface = new Interface(this, this.getMetadata().getAllPublicMethods());
		// Now this Object instance gets a new, private implementation of getInterface
		// that returns the newly created oInterface. Future calls of getInterface on the
		// same Object therefore will return the already created interface
		this.getInterface = jQuery.sap.getter(oInterface);
		// as the first caller doesn't benefit from the new method implementation we have to
		// return the created interface as well.
		return oInterface;
	};

	return ImageHandler;
});