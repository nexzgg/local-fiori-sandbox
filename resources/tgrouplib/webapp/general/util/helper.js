/** @module com/tgroup/lib/general */
sap.ui.define([
	"com/tgroup/lib/message/model/models",
	"com/tgroup/lib/message/util/messageHandler"
], function (poMessageModel, poMessageHandler) {
	"use strict";

	return {

		_oComponent: undefined,

		/** 
		 * get component.
		 * @return {object} _oComponent component
		 */
		getComponent: function () {
			return this._oComponent;
		},

		/** 
		 * set component
		 * @public
		 * @param {object} poComponent component
		 */
		setComponent: function (poComponent) {
			this._oComponent = poComponent;
		},

		/**
		 * universal functions.
		 * @public
		 */
		mFunctions: {
			fnDestroy: function () {
				this.destroy();
			},
			fnClose: function () {
				this.close();
			}
		},

		/**
		 * Local variables used in the helper
		 * */
		_oStartupParamters: undefined,

		/**
		 * Helper for copying a javascript object
		 * @public
		 * @param {object} poCopy The copy which should be copyied
		 * @return {object} oPaste The object to be returned
		 */
		copyObject: function (poCopy) {
			var oPaste = null;
			try {
				if (poCopy) {
					oPaste = JSON.parse(JSON.stringify(poCopy));
				}
			} catch (e) {
				jQuery.sap.log.error(e);
			}
			return oPaste;
		},

		/**
		 * get object first match of object.
		 * check equals of properties.
		 * @public
		 * @param {array} paList array of object
		 * @param {map} pmProperty object of properties and values 
		 * @return {object} oObject the searched object
		 */
		getObject: function (paList, pmProperty) {
			var oObject;
			try {
				if (paList && pmProperty) {
					for (var i = 0; i < paList.length; i++) {
						var bMatch = true;
						var oObj = paList[i];
						for (var property in pmProperty) {
							if (oObj.hasOwnProperty(property) || oObj.getProperty) {
								if (oObj[property] !== pmProperty[property] &&
									(!!oObj[property] || (!oObj.getProperty || oObj.getProperty && oObj.getProperty(property) !== pmProperty[property]))) {
									bMatch = false;
									break;
								}
							} else {
								bMatch = false;
								break;
							}
						}
						// object matches with the properties
						if (bMatch) {
							oObject = oObj;
							break;
						}
					}
				}
			} catch (oEx) {
				jQuery.sap.log.error(oEx);
			}

			return oObject;
		},

		/**
		 * get the model and the path which we want to remove from a list.
		 * @public
		 * @param {string} psPathArray array path in model
		 * @param {string} psPathRemove path from object we want to remove from the array
		 * @param {object} poModel the model
		 */
		removeObjectFromModelArray: function (psPathArray, psPathRemove, poModel) {
			if (poModel) {
				// set object which we want to remove undefined.
				var aArray = poModel.getProperty(psPathArray);
				if (aArray && aArray.length > 0) {
					poModel.setProperty(psPathRemove, undefined);
					// remove undefined objects
					aArray = aArray.filter(function (poEntry) {
						return poEntry !== undefined;
					});
					poModel.setProperty(psPathArray, aArray);
				}
			}
		},

		/**
		 * Functionality for getting the date in the right format as string for the backend
		 * @public
		 * @param {object} poDate The date object to be formatted
		 * @return {string} sDate Date in format YYYYMMDD
		 * */
		getDateBackend: function (poDate) {
			if (poDate) {
				var iYear, iMonth, iDay, sDate;
				iYear = poDate.getFullYear();
				iMonth = this._getLeadingZeroes(poDate.getMonth() + 1);
				iDay = this._getLeadingZeroes(poDate.getDate());
				sDate = iYear.toString() + iMonth.toString() + iDay.toString();
				try {
					sDate = iYear.toString() + iMonth.toString() + iDay.toString();
					return (sDate);
				} catch (e) {
					return '00000000';
				}
			}
		},

		/**
		 * Functionality for getting the time in the right format as string for the backend
		 * @public
		 * @param {object} poTime The time object to be formatted
		 * @return {string} sTime Time in format HHMMSS
		 * */
		getTimeBackend: function (poTime) {
			if (poTime) {
				var iHours, iMinutes, iSeconds, sTime;
				iHours = this._getLeadingZeroes(poTime.getHours());
				iMinutes = this._getLeadingZeroes(poTime.getMinutes());
				iSeconds = this._getLeadingZeroes(poTime.getSeconds());
				try {
					sTime = iHours.toString() + iMinutes.toString() + iSeconds.toString();
					return (sTime);
				} catch (e) {
					return '000000';
				}
			}
		},

		/**
		 * Internal functionality for checking if we need leading zeroes for numbers <= 9
		 * @private
		 * @param {integer} piValue The number to be checked
		 * @return {integer} piValue The formatted number
		 * */
		_getLeadingZeroes: function (piValue) {
			var iValue;
			if (piValue) {
				if (piValue <= 9) {
					iValue = 0 + piValue.toString();
				} else {
					iValue = piValue;
				}
			}
			return (iValue);
		},

		/**
		 * Functionality for setting the startup parameters provided at the beginning
		 * @public
		 * @param {object} poStartupParameters The object storing all our values
		 * */
		setStartupParameters: function (poStartupParameters) {
			this._oStartupParameters = poStartupParameters;
		},

		/**
		 * Functionality for getting all our startup parameters 
		 * @public
		 * @return {object} this._oStartupParameters The local object storing all our parameters
		 * */
		getStartupParameters: function () {
			return (this._oStartupParameters);
		},

		/**
		 * close myself.
		 * @public
		 * @param {Control} poControl control
		 */
		close: function (poControl) {
			if (poControl && poControl.close) {
				poControl.close();
			}
		},

		/**
		 * destroy myself. 
		 * @public
		 * @param {Control} poControl control
		 */
		destroy: function (poControl) {
			if (poControl && poControl.destroy) {
				poControl.destroy();
			}
		},

		/**
		 * get base64 string of file.
		 * @public
		 * @param {control} poFileUploader file uploader control
		 * @param {function} pfnCallback callback function. get the base64 string
		 */
		getFileBase64: function (poFile, pfnCallBack) {
			var sBase64 = "";
			if (poFile) {
				var oReader = new FileReader();
				// Creating new file reader and convert the whole thing
				// to a base64 string
				oReader.onload = function (poData) {
					sBase64 = poData.target.result;
					// callback
					if (pfnCallBack && typeof pfnCallBack === "function") {
						pfnCallBack(sBase64, poFile.type);
					}
				};
				oReader.readAsDataURL(poFile);
			} else {
				// callback
				if (pfnCallBack && typeof pfnCallBack === "function") {
					pfnCallBack(sBase64, poFile.type);
				}
			}

		},

		/**
		 * download an BASE64 File in the browser. 
		 * @public
		 * @param {string} psBase64
		 * @param {string} psType
		 * @param {string} psName
		 */
		downloadBase64File: function (psBase64, psType, psName, poWindow) {
			try {
				var aByteCharacters = atob(psBase64);
				return this._downloadFile(aByteCharacters, psType, psName, poWindow);
			} catch (poError) {
				jQuery.sap.log.error(poError);
			}
		},

		/**
		 * download an Binary File in the browser. 
		 * @public
		 * @param {string} psBinary
		 * @param {string} psType
		 * @param {string} psName
		 */
		downloadBinaryFile: function (psBinary, psType, psName) {
			try {
				var aByteCharacters = btoa(psBinary);
				this._downloadFile(aByteCharacters, psType, psName);
			} catch (poError) {
				jQuery.sap.log.error(poError);
			}
		},

		/**
		 * download file
		 * @private
		 * @param {array} paByteCharacters
		 * @param {string} psType
		 * @param {string} psName
		 */
		_downloadFile: function (paByteCharacter, psType, psName, poWindow) {
			var aByteNumbers = new Array(paByteCharacter.length);
			for (var i = 0; i < paByteCharacter.length; i++) {
				aByteNumbers[i] = paByteCharacter.charCodeAt(i);
			}
			var aByteArray = new Uint8Array(aByteNumbers);
			var oBlob = new Blob([aByteArray], {
				type: psType,
				endings: "native"
			});
			var sBlobUrl = URL.createObjectURL(oBlob);
			if (sap.ui.Device.browser.msie) {
				if (paByteCharacter.length != 0) {
					window.navigator.msSaveOrOpenBlob(oBlob, psName);
				}
			} else {
				if (poWindow) {
					poWindow.location = sBlobUrl;
				} else {
					return window.open(sBlobUrl, psName, "width=500, height=500");
				}
			}
		},

		/** 
		 * to json with no quotes to a valid json with double quotes.
		 * @param {string} psJSON  invalid JSON
		 * @return {string} sJSON valid JSON
		 */
		convertInvalidJSON: function (psJSON) {
			var sJSON = "";
			try {
				if (psJSON && psJSON !== "") {
					sJSON = psJSON.replace(/(['"])?([a-zA-Z0-9_]+)(['"])?: "/g, '"$2": "');
				}
			} catch (e) {
				jQuery.sap.log.error(e);
			}
			return sJSON;
		},

		/**
		 * convert underscore to camel case of the key. 
		 * @param {string} psKey key of the object
		 * @param {string} psValue value of the key
		 * @return {string} value return the value
		 */
		convertUnderScoreToCamelCase: function (psKey, psValue) {
			if (psKey) {
				var sKey = this.convertToCamelCase(psKey);
				this[sKey] = psValue;
				delete this[psKey];
			}
			return psValue;
		},

		/** 
		 * convert string to camel case. underscore will be replaced.
		 * @param {string} psValue value
		 * @return {string} sCamel camel string
		 */
		convertToCamelCase: function (psValue) {
			var sCamel;

			if (psValue && typeof psValue === "string") {
				psValue = psValue.toLowerCase();
				sCamel = psValue.charAt(0).toUpperCase() + psValue.slice(1);
				sCamel = sCamel.replace(/(\_\w)/g, function (sMatch) {
					return sMatch[1].toUpperCase();
				});
			}

			return sCamel;
		},

		/**
		 * nav in tab bar to the next tab.
		 * @param {object} poIconTabBar icon tab bar control
		 */
		iconTabBarNext: function (poIconTabBar) {
			var iIndex = 0;
			if (poIconTabBar && typeof poIconTabBar.getSelectedKey === "function" && typeof poIconTabBar.getItems === "function") {
				// search match in content
				var aItem = poIconTabBar.getItems();
				for (var i = 0; i < aItem.length; i++) {
					var oItem = aItem[i];
					if (typeof oItem.getKey === "function" && oItem.getKey() === poIconTabBar.getSelectedKey()) {
						iIndex = i + 1;
						break;
					}
				}
				// next tab, if not the last tab
				if (iIndex > 0 && iIndex < aItem.length) {
					var oNext = aItem[iIndex];
					if (oNext) {
						poIconTabBar.setSelectedKey(oNext.getKey());
					}
				}
			}
		},

		/**
		 * nav in tab bar to the next tab.
		 * @param {object} poIconTabBar icon tab bar control
		 */
		iconTabBarPrevious: function (poIconTabBar) {
			var iIndex = 0;
			if (poIconTabBar && typeof poIconTabBar.getSelectedKey === "function" && typeof poIconTabBar.getItems === "function") {
				// search match in content
				var aItem = poIconTabBar.getItems();
				for (var i = 0; i < aItem.length; i++) {
					var oItem = aItem[i];
					if (typeof oItem.getKey === "function" && oItem.getKey() === poIconTabBar.getSelectedKey()) {
						iIndex = i - 1;
						break;
					}
				}
				// next tab, if not the last tab
				if (iIndex >= 0) {
					var oPrevious = aItem[iIndex];
					if (oPrevious) {
						poIconTabBar.setSelectedKey(oPrevious.getKey());
					}
				}
			}
		},

		/**
		 * get selection set from filterbar.
		 * @public
		 * @param {object} poFilterBar filter bar control
		 * @return {array} aSelectionSet selection set
		 */
		getFilterBarSelectionSet: function (poFilterBar) {
			var aFilters = poFilterBar.getFilterGroupItems();
			var aSelectionSet = [];
			for (var i = 0; i <= aFilters.length - 1; i++) {
				aSelectionSet.push(poFilterBar.determineControlByFilterItem(aFilters[i]));
			}
			return aSelectionSet;
		},

		/** 
		 * crossside navigation application.
		 * @param {string} psSemantic semantic object
		 * @param {string} psAction action
		 * @param {string} psNavMode navMode
		 * @param {object} [poParameters] parameters
		 * @param {object} [poAppStateData] app state data 
		 */
		navToApplication: function (psSemantic, psAction, psNavMode, poParameters, poAppStateData) {
			if (psSemantic && psAction && sap.ushell) {
				// execute navigation
				var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
				var oAppState = oCrossAppNavigator.createEmptyAppState(this.getComponent());
				oAppState.setData(poAppStateData);
				oAppState.save();

				var oHashChanger = sap.ui.core.routing.HashChanger.getInstance();
				var sOldHash = oHashChanger.getHash();
				var sNewHash = sOldHash + "?" + "sap-iapp-state=" + oAppState.getKey();
				oHashChanger.replaceHash(sNewHash);

				var oNavConfig = {
					target: {
						semanticObject: psSemantic,
						action: "display"
					},
					params: poParameters,
					appStateKey: oAppState.getKey()
				};

				if (psNavMode === "LP") {
					oCrossAppNavigator.toExternal(oNavConfig);
				} else {
					var hash = oCrossAppNavigator.hrefForExternal(oNavConfig);
					var url = window.location.href.split('#')[0] + hash;
					sap.m.URLHelper.redirect(url, true);
				}
			}
		},

		/** 
		 * do app state
		 * @public
		 * @param {object} poComponent component
		 * @param {function} pfnCallback callback success
		 */
		doAppState: function (poComponent, pfnCallback) {
			if (poComponent && pfnCallback && typeof pfnCallback === "function") {
				var oHashChanger = sap.ui.core.routing.HashChanger.getInstance();
				var sHash = oHashChanger.getHash();
				if (sHash && sap.ushell) {
					var sAppStateKey = /(?:sap-iapp-state=)([^&=]+)/.exec(sHash)[1];
					oHashChanger.setHash("");
					sap.ushell.Container
						.getService("CrossApplicationNavigation")
						.getAppState(poComponent, sAppStateKey)
						.done(function (poSavedAppState) {
							pfnCallback(poSavedAppState);
						});
				}
			}
		},

		/**
		 * read the current user informations. 
		 */
		readCurrentUserData: function () {
			// success function
			var fnSuccess = function (oData) {
				if (oData.results && oData.results[0]) {
					var oCurrUserModel = sap.ui.getCore().getModel("currUser");
					oCurrUserModel.setProperty("/", oData.results[0]);
				}
			};

			// error function
			var fnError = function () {
				poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
			};

			// clear the messages
			poMessageModel.clearMessageModel();

			var oDataProvider = sap.ui.getCore().getModel("dataprovider");
			// Building up the map of parameters used for our gateway read
			var mParameters = {
				success: fnSuccess,
				error: fnError
			};
			// starting the gateway read
			oDataProvider.read("/CurrentUserSet", mParameters);
		},

		/** 
		 * get url parameter value from window location
		 * @param psKey {string} parameter key
		 * @returns sValue {string} value string
		 */
		getUrlParameter: function (psKey) {
			var sQuery = window.location.search.substring(1);
			var aVars;
			var sValue;
			if (sQuery !== "") {
				aVars = sQuery.split("&");
				for (var m = 0; m <= aVars.length - 1; m++) {
					var oParameter = aVars[m];
					var oPair = oParameter.split("=");
					var sKey = oPair[0];
					var sPairValue = oPair[1];
					if (sKey === psKey) {
						sValue = sPairValue;
					}
				}
			}

			return sValue;
		}
	};
});