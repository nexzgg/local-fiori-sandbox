sap.ui.define([
	"com/tgroup/lib/general/util/externalLibraries/powerpoint/pptxgen"
], function(Pptxgen) {
	"use strict";

	var pptxUtil = function() {
		/**
		 * private variables
		 * */
		this._oPptx = new Pptxgen();
		this._oSlide = null;
		this._aColumns = [];
		this._mDimension = {
			nWidth: 0,
			nHeight: 0
		};
		this._nDpi = 96;
		this._nSlideCounter = 0;
	};
	/**
	 * function: define Slide Master
	 * @params {map} pmOptions - Slide master object options
	 */
	pptxUtil.prototype.fnDefineSlideMaster = function(pmOptions) {
		
	this._oPptx.defineSlideMaster(pmOptions);
	
	};
	/**
	 * function for adding a column to current row
	 * @public
	 * @param {string} psText - value 
	 * @param {boolean} [pblsBold] - text is bold?
	 * @param {number} [pnColSpan] - number of cols to merge
	 * @param {string} [psColor] - text color of cell
	 * @param {string} [psFill] - background color of cell
	 * @param {number} [pnRowSpan] - number of rows to merge
	 * @param {strimg} [psAlign] - alignment of cell
	 * */
	pptxUtil.prototype.fnAddColumn = function(psText, pblsBold, pnColSpan, psColor, psFill, pnRowSpan, psAlign) {
		var mOptions = {};
		var sText = "";
		if (typeof psAlign === "string" && psAlign !== "") {
			mOptions.align = psAlign;
		}
		if (pnRowSpan > 0) {
			mOptions.rowspan = pnRowSpan;
		}
		if (pnColSpan > 0) {
			mOptions.colspan = pnColSpan;
		}
		if (pblsBold === true) {
			mOptions.bold = pblsBold;
		}
		if (typeof psColor === "string" && psColor !== "") {
			mOptions.color = psColor;
		}
		if (typeof psFill === "string" && psFill !== "") {
			mOptions.fill = psFill;
		}
		if(psText) {
			sText = psText.toString();
		}
		this._aColumns.push({
			text: sText,
			options: mOptions
		});
	};
	/**
	 * @public
	 * @param {string} psText - value
	 * @param {map} [pmOptions] - Textoptions
	 */
	pptxUtil.prototype.fnAddColumn2 = function(psText, pmOptions) {
		var sText = "";
		var mOptions = {};
		if(psText)
			sText = psText.toString();
		if(pmOptions) 
			mOptions = pmOptions;
		this._aColumns.push({
			text: sText,
			options: mOptions
		});
	};
	/**
	 * Method for merge rows for a specific column of current row
	 * @public
	 * @param {number} pnRows - number of rows 
	 * @param {number} [pnIndex] - index of array; default value is last index
	 * */
	pptxUtil.prototype.fnSetRowSpan = function(pnRows, pnIndex) {
		var nIndex = 0;
		if (pnRows > 0) {
			if (this._aColumns.length > 0) {
				if (pnIndex > 0 || pnIndex === 0) {
					nIndex = pnIndex;
				} else {
					nIndex = this._aColumns.length - 1;
				}
				this._aColumns[nIndex].options.rowspan = pnRows;
			}
		}
	};
	/**
	 * Method for initializing columns / start new row
	 * @public
	 * */
	pptxUtil.prototype.fnInitRow = function() {
		this._aColumns = [];
	};
	/**
	 * Method for initiailizing rows / start new table
	 * @public
	 * */
	pptxUtil.prototype.fnInitTable = function() {
		this._aRows = [];
	};
	/**
	 * Method for adding table to row
	 * @public
	 * */
	pptxUtil.prototype.fnAddRow = function() {
		this._aRows.push(this._aColumns);
		this._fnInitRow();
	};
	/**
	 * Method: create table with only one column
	 * @public
	 * @param {map} pmTableOptions - table options  
	 * @param {array} paData - array with rows 
	 * */
	pptxUtil.prototype.fnCreateSingleColumnTable = function(pmTableOptions, paData) {
		this._fnInitTable();
		this._fnInitRow();
		for (var i = 0; i < paData.length; i++) {
			if (i === 0) {
				this._fnAddColumn(paData[i], true, null, null, this._mColors.darkGrey);
			} else {
				this._fnAddColumn(paData[i]);
			}
			this._fnAddRow();
		}
		this._oPptx.fnAddTable(this._aRows, pmTableOptions, pmTableOptions);
	};
	/**
	 * Method: set meta info author
	 * @public
	 * @param {string} psAuthor - author
	 * */
	pptxUtil.prototype.setAuthor = function(psAuthor) {
		this._oPptx.setAuthor(psAuthor);
	};
	/**
	 * Method: set meta info company
	 * @public
	 * @param {string} psCompany - value 
	 * */
	pptxUtil.prototype.setCompany = function(psCompany) {
		this._oPptx.setCompany(psCompany);
	};
	/**
	 * Method: set meta info revision
	 * @public
	 * @param {string} psRevision - value 
	 * */
	pptxUtil.prototype.setRevision = function(psRevision) {
		this._oPptx.setRevision(psRevision);
	};
	/**
	 * Method: set meta info subject
	 * @public
	 * @param {string} psSubject - value 
	 * */
	pptxUtil.prototype.setSubject = function(psSubject) {
		this._oPptx.setSubject(psSubject);
	};
	/**
	 * Method: set meta info Title
	 * @public
	 * @param {string} psTitle - value 
	 * */
	pptxUtil.prototype.setTitle = function(psTitle) {
		this._oPptx.setTitle(psTitle);
	};
	/**
	 * Method: set Layout by custom values
	 * @public
	 * @param {number} pnWidth - width in inch
	 * @param {number} pnHeight - height in inch
	 * */
	pptxUtil.prototype.fnSetCustomLayout = function(pnWidth, pnHeight) {
		this._fnSetHeight(pnHeight);
		this._fnSetWidth(pnWidth);
		var nWidth = pnWidth;
		var nHeight = pnHeight;
		this._oPptx.setLayout({
			name: 'custom',
			width: nWidth,
			height: nHeight
		});
	};
	/**
	 * Method: set Layout name
	 * @public
	 * @param {string} psName - Layout 
	 */ 
	pptxUtil.prototype.fnSetLayout = function(psName) {
		if(psName) {
			this._oPptx.setLayout(psName);
		}
	};
	/**
	 * Method: create a new slide
	 * @public
	 * */
	pptxUtil.prototype.fnAddSlide = function(psMasterSlide) {
		var sMasterSlide = (psMasterSlide) ? psMasterSlide.toString() : "";
		this._nSlideCounter++;
		this._oSlide = this._oPptx.addNewSlide(sMasterSlide);
	};
	/**
	 * Method: save document
	 * @public
	 * @param {string} psName - name
	 * */
	pptxUtil.prototype.fnSave = function(psName, pfCallback) {
		this._oPptx.save(psName);
		try {
			if(pfCallback) {
				pfCallback();
			}
		} catch(e) {}
	};
	/**
	 * Method: set width
	 * @private
	 * @param {number} pnWidth - inch 
	 * */
	pptxUtil.prototype._fnSetWidth = function(pnWidth) {
		this._mDimension.nWidth = pnWidth;
	};
	/**
	 * Method: set height
	 * @private
	 * @param {number} pnHeight - inch
	 * */
	pptxUtil.prototype._fnSetHeight = function(pnHeight) {
		this._mDimension.nHeight = pnHeight;
	};
	/**
	 * Method: convert inch to pixel
	 * @private
	 * @param {number} pninch - pixel
	 * @returns {number} - inch
	 * */
	pptxUtil.prototype._fnPx2In = function(pnPixel) {
		return pnPixel / this.fnGetDpi();
	};
	/**
	 * Method: convert inch to pixel
	 * @private
	 * @param {number} pnInch - inch 
	 * @returns {number} - pixel 
	 * */
	pptxUtil.prototype._fnIn2Px = function(pnInch) {
		return pnInch * this.fnGetDpi();
	};
	/**
	 * Method: get dpi
	 * @public
	 * @param {number} - dpi 
	 * */
	pptxUtil.prototype.fnGetDpi = function() {
		return this._nDpi;
	};
	/**
	 * Method: add text to document
	 * @public
	 * @param {string} psText - some value (not null or "undefined")
	 * @param {map} pmOptions - text options
	 * */
	pptxUtil.prototype.fnAddText = function(psText, pmOptions) {
		var nX = pmOptions.nX;
		var nY = pmOptions.nY;
		var sColor = "000000";
		var nSize = 12;
		var sFontFace = "Arial";
		var blsBold = false;
		var blsItalic = false;
		var blsUnderline = false;
		var mOptions = {};
		if (typeof pmOptions.sColor === "string" && pmOptions.sColor !== "") {
			sColor = pmOptions.sColor;
		}
		if (typeof pmOptions.nSize === "number") {
			nSize = pmOptions.nSize;
		}
		if (typeof pmOptions.sFontFace === "string" && pmOptions.sFontFace !== "") {
			sFontFace = pmOptions.sFontFace;
		}
		if (pmOptions.blsBold === true) {
			blsBold = true;
		}
		if (pmOptions.blsItalic === true) {
			blsItalic = true;
		}
		if (pmOptions.blsUnderline === true) {
			blsUnderline = true;
		}
		mOptions = {
			x: nX,
			y: nY,
			fontSize: nSize,
			fontFace: sFontFace,
			color: sColor,
			bold: blsBold,
			italic: blsItalic,
			underline: blsUnderline
		};
		if(pmOptions.nCharSpacing >= 1) {
			mOptions.charSpacing = pmOptions.nCharSpacing
		}
		if (typeof pmOptions.sFill === "string" && pmOptions.sFill !== "") {
			mOptions.fill = pmOptions.sFill;
		}
		if (pmOptions.nWidth > 0) {
			mOptions.w = pmOptions.nWidth;
		}
		if (pmOptions.nHeight > 0) {
			mOptions.h = pmOptions.nHeight;
		}
		if (typeof pmOptions.sAlign === "string" && pmOptions.sAlign !== "") {
			mOptions.align = pmOptions.sAlign;
		}
		if (typeof pmOptions.sValign === "string" && pmOptions.sValign !== "") {
			mOptions.valign = pmOptions.sValign;
		}		
		this._oSlide.addText(psText, mOptions);
	};
	/**
	 * Method: add image to document
	 * @public
	 * @param {number} pnX - x in inch
	 * @param {number} pnY - y in inch
	 * @param {number} pnWidth - width in inch
	 * @param {number} pnHeight - height in inch
	 * @param {string} [psPath] - url
	 * @param {string} [psData] - DataURL (base64)
	 * */
	pptxUtil.prototype.fnAddImage = function(pnX, pnY, pnWidth, pnHeight, psPath, psData, pmOptions) {
		var nX = pnX;
		var nY = pnY;
		var nWidth = pnWidth;
		var nHeight = pnHeight;
		var mOptions = {};
		
		mOptions.x = pnX;
		mOptions.y = pnY;
		mOptions.w = pnWidth;
		mOptions.h = pnHeight;
		if (typeof psPath === "string" && psPath !== "") {
			mOptions.path = psPath;
		}
		if (typeof psData === "string" && psData !== "") {
			mOptions.path = psData;
		}
		if(pmOptions) {
			if(pmOptions.hyperlink) {
					mOptions.hyperlink = {};
					mOptions.hyperlink.url = pmOptions.hyperlink;
			}
			if(pmOptions.sizing) {
				mOptions.sizing = pmOptions.sizing;
			}
		}
		this._oSlide.addImage(mOptions);
	};
	/**
	 * Method: draw line to document
	 * @public
	 * @param {number} pnX - x in inch
	 * @param {number} pnY - y in inch
	 * @param {number} pnWidth - width in inch
	 * @param {number} [pnSize] - line size
	 * @param {string} [psColor] - line color
	 * @param {string} [psTail] - tail, e.g.: bullet
	 * */
	pptxUtil.prototype.fnAddLine = function(pnX, pnY, pnWidth, pnSize, psColor, psTail) {
		var nX = pnX;
		var nY = pnY;
		var nWidth = pnWidth;

		var sColor = "000000";
		var sTail = "none";
		var nSize = 1;

		if (typeof pnSize === "number" && pnSize > 0)
			nSize = pnSize;
		if (typeof psTail === "string" && psTail !== "")
			sTail = psTail;
		if (typeof psColor === "string" && psColor !== "")
			sColor = psColor;

		this._oSlide.addShape(this._oPptx.shapes.LINE, {
			x: nX,
			y: nY,
			w: nWidth,
			h: 0,
			line: sColor,
			lineSize: nSize,
			line_tail: sTail
		});
	};
	/**
	 * Method: draw rectangle to document
	 * @public
	 * @param {number} pnX - x in inch
	 * @param {number} pnY - y in inch
	 * @param {number} pnWidth - width in inch
	 * @param {number} pnHeight - height in inch
	 * @param {string} [psColor] - Color
	 * */
	pptxUtil.prototype.fnAddRectangle = function(pnX, pnY, pnWidth, pnHeight, psColor) {
		var nX = pnX;
		var nY = pnY;
		var nWidth = pnWidth;
		var nHeight = pnHeight;
		this._oSlide.addShape(this._oPptx.shapes.RECTANGLE, {
			x: nX,
			y: nY,
			w: nWidth,
			h: nHeight,
			fill: psColor
		});
	};
	/**
	 * Method: draw oval to document
	 * @public
	 * @param {number} pnX - x in inch
	 * @param {number} pnY - y in inch
	 * @param {number} pnWidth - width in inch
	 * @param {number} pnHeight - height in inch
	 * @param {string} [psColor] - color
	 * @param {number} [pnAlpha] - alpha
	 * @param {string} [psType] - type
	 * */
	pptxUtil.prototype.addOval = function(pnX, pnY, pnWidth, pnHeight, psColor, pnAlpha, psType) {
		var nX = pnX;
		var nY = pnY;
		var nWidth = pnWidth;
		var nHeight = pnHeight;
		var nAlpha = 25;
		var sType = "solid";
		if (typeof psType === "string" && psType !== "") {
			sType = psType;
		}
		if (typeof pnAlpha === "number") {
			nAlpha = pnAlpha;
		}
		this._oSlide.addShape(this._oPptx.shapes.OVAL, {
			x: nX,
			y: nY,
			w: nWidth,
			h: nHeight,
			fill: {
				type: sType,
				color: psColor,
				alpha: nAlpha
			}
		});
	};
	/**
	 * Method: add table to document
	 * @public
	 * @param {array} paRows - rows
	 * @param {pmTableOptions} [pmTableOptions] - options / settings
	 * */
	pptxUtil.prototype.fnAddTable = function(paRows, pmTableOptions) {
		var mTableOptions = {};
		if (pmTableOptions && typeof pmTableOptions !== "undefined") {
			mTableOptions = pmTableOptions;
			if (typeof mTableOptions.x === "number") {
				mTableOptions.x = mTableOptions.x;
			}
			if (typeof mTableOptions.y === "number") {
				mTableOptions.y = mTableOptions.y;
			}
			if (typeof mTableOptions.w === "number") {
				mTableOptions.w = mTableOptions.w;
			}
			if (typeof mTableOptions.rowH === "number") {
				mTableOptions.rowH = mTableOptions.rowH;
			}
			if (typeof mTableOptions.colW !== "undefined" && mTableOptions.colW) {
				for (var i = 0; i < mTableOptions.colW.length; i++) {
					mTableOptions.colW[i] = mTableOptions.colW[i];
				}
			}
		}
		this._oSlide.addTable(paRows, pmTableOptions);
	};
	/**
	 * Method: get slide counter
	 * @public
	 * @returns {number} - number of slides
	 * */
	pptxUtil.prototype.fnGetSlideCounter = function() {
		return this._nSlideCounter;
	};
	return pptxUtil;

});