/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/ui/richtexteditor/RichTextEditor"
], function (poRichTextEditor) {
	"use strict";
	return poRichTextEditor.extend("com.tgroup.lib.controls.RichTextEditor", {

		_bEventChangeDone: false,
		_sInterval: undefined,
		_sEditableInterval: undefined,
		_sHeightInterval: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				growing: {
					type: "boolean",
					defaultValue: true
				},
				minHeight: {
					type: "string",
					defaultValue: "30"
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} oControl The control itself
		 * */
		renderer: function (poRm, oControl) {
			sap.ui.richtexteditor.RichTextEditorRenderer.render(poRm, oControl);
		},

		_fnChangeTextArea: function (poEvent) {
			var sNew = poEvent.getParameter("newValue");
			var sOld = poEvent.getParameter("oldValue");

			// enter also trigger the change, so do not resize
			if (sNew !== sOld) {
				this._adjustHeight();
			}
		},

		/**
		 * Functionality for setting the growing of the textarea
		 * @public
		 * @param {boolean} pbGrowing growing flag
		 * */
		setGrowing: function (pbGrowing) {
			this.setProperty("growing", !!pbGrowing, true);

			if (pbGrowing) {
				this._adjustHeight();
				this.attachChange(this._fnChangeTextArea, this);
				this._bEventChangeDone = true;
			} else {
				this.detachChange(this._fnChangeTextArea, this);
			}
		},

		/**
		 * TODO: Bugfix from standard. this._oEditor is undefined but used in this method.
		 * @public
		 * @override
		 */
		_onAfterReadyTinyMCE4: function () {
			if (this._oEditor && poRichTextEditor.prototype._onAfterReadyTinyMCE4) {
				poRichTextEditor.prototype._onAfterReadyTinyMCE4.apply(this, arguments);
			}
		},

		/**
		 * set editable
		 * @public
		 * @override
		 * @param {boolean} pbEditable editable
		 */
		setEditable: function (pbEditable) {
			if (sap.ui.richtexteditor.RichTextEditor.prototype.setEditable) {
				sap.ui.richtexteditor.RichTextEditor.prototype.setEditable.apply(this, arguments);
			}

			if (this.getGrowing()) {
				this._sEditableInterval = setInterval(function () {
					if (!this._bInitializationPending) {
						if (this.getGrowing()) {
							this._adjustHeight();
						}
						clearInterval(this._sEditableInterval);
					}
				}.bind(this), 100);

				this.attachChange(this._fnChangeTextArea, this);
				this._bEventChangeDone = true;
			} else {
				this.detachChange(this._fnChangeTextArea, this);
			}

			return this;
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function () {
			if (sap.ui.richtexteditor.RichTextEditor.prototype.onAfterRendering) {
				sap.ui.richtexteditor.RichTextEditor.prototype.onAfterRendering.apply(this, arguments);
			}

			if (this.getGrowing()) {
				if (!this._bEventChangeDone) {
					this.setGrowing(true);
				}
				this._adjustHeight();
			}
		},

		/**
		 * growing the textarea, by content. also check minheight.
		 * @private
		 */
		_adjustHeight: function () {
			var oSource = this;
			var oJQuerySource = $('#' + oSource.getId());
			var oIFrame = oJQuerySource.find("iframe");
			if (oIFrame.length > 0) {
				if (this._sInterval) {
					// delete the interval
					clearInterval(this._sInterval);
					this._sInterval = undefined;
				}

				// calculate height
				var iHeight = 0;
				var aRow = oIFrame.contents().find("body").find("p");
				if (aRow.length > 0) {
					var oLastRow = aRow.get(aRow.length - 1);
					iHeight += oLastRow.offsetTop + oLastRow.offsetHeight + 10;
				}
				var iMinHeight = 0;
				try {
					iMinHeight = parseInt(this.getMinHeight(), 10);
				} catch (oEx) {
					jQuery.sap.log.error(oEx);
				}
				iHeight = (iHeight < iMinHeight) ? iMinHeight : iHeight;

				// set frame height, do it 3 times because standard do also rendering on height
				if (this._sHeightInterval) {
					clearInterval(this._sHeightInterval);
				}

				oIFrame.height(iHeight);
				this._sHeightInterval = setInterval(function () {
					if (this.do3Times < 3) {
						oIFrame.height(iHeight);
						this.do3Times++;
					} else {
						clearInterval(this.control._sHeightInterval);
					}
				}.bind({
					control: this,
					do3Times: 0
				}), 200);

			} else if (!this._sInterval) {
				// recursive call, all rendered so adjust height, and delete the interval
				this._sInterval = setInterval(this._adjustHeight.bind(this), 100);
			}
		}

	});
});