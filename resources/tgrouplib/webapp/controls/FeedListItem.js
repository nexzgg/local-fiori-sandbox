/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/m/FeedListItem"
], function(poText) {
	"use strict";
	return poText.extend("com.tgroup.lib.controls.FeedListItem", {

		metadata: {
			properties: {
				showActions: {
					type: "boolean",
					defaultValue: true
				}
			}
		},

		renderer: function(poRm, poControl) {
			sap.m.FeedListItemRenderer.render(poRm, poControl);
		},

		onAfterRendering: function() {
			if (sap.m.FeedListItem.prototype.onAfterRendering) {
				sap.m.FeedListItem.prototype.onAfterRendering.apply(this, arguments);
			}

			if (this.getShowActions()) {
				this.getAggregation("_actionButton").setVisible(true);
			} else {
				this.getAggregation("_actionButton").setVisible(false);
			}
		}

	});
});