/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/m/Toolbar",
	"sap/m/ToolbarSpacer",
	"sap/m/Text",
	"sap/m/Button"
], function (poToolbar, ToolbarSpacer, Text, Button) {
	"use strict";
	return poToolbar.extend("com.tgroup.lib.controls.Toolbar", {

		_done: false,
		_oText: undefined,
		_oFilter: undefined,
		_oToggleButton: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				text: {
					type: "string",
					defaultValue: ""
				},
				toggleControlVisible: {
					type: "boolean",
					defaultValue: true
				},
				visible: {
					type: "boolean",
					defaultValue: false
				},
				filter: {
					type: "string",
					defaultValue: ""
				},
				toggleActive: {
					type: "boolean",
					defaultValue: true
				}
			},
			associations: {
				toggleControl: {
					type: "sap.ui.core.Control",
					multiple: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function (poRm, poControl) {
			if (!poControl._done) {
				poControl._done = true;
				poControl._oToggleButton = new Button({
					type: sap.m.ButtonType.Transparent,
					icon: "sap-icon://undo"
				});
				poControl._oToggleButton.attachPress(function (poEvent) {
					this.setToggleControlVisible(!this.getToggleControlVisible());
				}.bind(poControl));
				poControl.attachPress(function (poEvent) {
					this.setToggleControlVisible(!this.getToggleControlVisible());
				}.bind(poControl));

				poControl.addContent(poControl._oText);
				poControl.addContent(poControl._oFilter);
				poControl.addContent(new ToolbarSpacer());
				poControl.addContent(poControl._oToggleButton);
			}

			sap.m.ToolbarRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality for setting the filter string of the toolbar
		 * @override
		 * @public
		 * @param {string} psFilter filter
		 * */
		setFilter: function (psFilter) {
			this.setProperty("filter", psFilter, true);

			if (!this._oFilter) {
				this._oFilter = new Text();
			}
			this._oFilter.setText(psFilter);
		},

		/**
		 * Functionality for setting the toggle control visible boolean of the toolbar
		 * @override
		 * @public
		 * @param {boolean} pbToggleControlVisible toggle control visible
		 * */
		setToggleControlVisible: function (pbToggleControlVisible) {
			this.setProperty("toggleControlVisible", pbToggleControlVisible, true);
			this._toggleControl();
		},

		/**
		 * Functionality for setting the toggle control id string of the toolbar
		 * @override
		 * @public
		 * @param {string} psToggleControl toggle control id
		 * */
		setToggleControl: function (psToggleControl) {
			this.setAssociation("toggleControl", psToggleControl, true);
			this._toggleControl();
		},

		/**
		 * Functionality for setting the text string of the toolbar
		 * @override
		 * @public
		 * @param {string} psText text
		 * */
		setText: function (psText) {
			this.setProperty("text", psText, true);
			if (!this._oText) {
				this._oText = new Text();
			}
			this._oText.setText(psText);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function () {
			if (sap.m.Toolbar.prototype.onAfterRendering) {
				sap.m.Toolbar.prototype.onAfterRendering.apply(this, arguments);
			}

			// if in the page set the subheader visible
			var oParent = this.getParent();
			if (oParent.getMetadata().getName() === "sap.m.Page") {
				oParent.setShowSubHeader(this.getVisible());
			}
		},

		/**
		 * Toggle control
		 * @function
		 * @public
		 * */
		_toggleControl: function () {
			if (this.getToggleControl() && this.getToggleActive()) {
				var oToggleControl = sap.ui.getCore().byId(this.getToggleControl());
				if (oToggleControl) {
					var bToggleControlVisible = !!this.getToggleControlVisible();
					oToggleControl.setVisible(bToggleControlVisible);
					this.setVisible(!bToggleControlVisible);
					// if in the page set the subheader visible
					var oParent = this.getParent();
					if (oParent.getMetadata().getName() === "sap.m.Page") {
						oParent.setShowSubHeader(!bToggleControlVisible);
					}
				}
			}
		}

	});
});