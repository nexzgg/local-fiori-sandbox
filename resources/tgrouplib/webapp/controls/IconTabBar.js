/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"sap/m/IconTabBar",
	"sap/ui/commons/RichTooltip"
], function(poHelper, poFormatter, poIconTabBar, poRichTooltip) {
	"use strict";
	return poIconTabBar.extend("com.tgroup.lib.controls.IconTabBar", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,
		_sCurrentTabGroup: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				tabGroup: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for setting the tabbar for the bar
		 * @public
		 * @param {string} psTabBar The name of the tab bar
		 * */
		setTabGroup: function(psTabGroup) {
			if (psTabGroup && typeof psTabGroup !== "undefined") {
				this.setProperty("tabGroup", psTabGroup, true);
				if (this._sCurrentTabGroup && this._sCurrentTabGroup !== psTabGroup) {
					this.destroyItems();
					this._done = false;
					this._renderBar(this);
				}
				this._sCurrentTabGroup = psTabGroup;
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			if (!oControl._done) {
				oControl._renderBar(oControl);
			}
			sap.m.IconTabBarRenderer.render(oRM, oControl);
		},

		_renderBar: function(poControl) {
			var sTabGroup = poControl.getTabGroup();
			if (sTabGroup && typeof sTabGroup !== "undefined") {
				var aTabs = sap.ui.getCore().getModel("layout").getProperty("/Tabs/" + sTabGroup);
				if (aTabs && typeof aTabs !== "undefined" && aTabs.length !== 0) {
					//Build up the new tabs and at them
					for (var i = 0; i <= aTabs.length - 1; i++) {
						var oCurrentTab = aTabs[i];
						var oIconTabFilter = new sap.m.IconTabFilter({
							text: oCurrentTab.Text,
							icon: "sap-icon://" + oCurrentTab.Icon,
							key: oCurrentTab.TabName,
							content: [
								new sap.ui.core.mvc.XMLView({
									viewName: oCurrentTab.ViewName
								})
							]
						});
						// tooltip
						if (oCurrentTab.Tooltip && oCurrentTab.Tooltip !== "") {
							oIconTabFilter.setTooltip(new poRichTooltip({
								text: oCurrentTab.Tooltip
							}));
						}
						poControl.addItem(oIconTabFilter);
					}
				}
			}
			poControl._done = true;
		},

		/**
		 * get tab filter by key.
		 * @public
		 * @param {string} psKey key
		 * @return {object} oTabFilter tab filter
		 */
		getTabFilter: function(psKey) {
			var oTabFilter;

			var aItems = this.getItems();
			for (var i = 0; i < aItems.length; i++) {
				var oTab = aItems[i];
				if (psKey === oTab.getKey()) {
					oTabFilter = oTab;
					break;
				}
			}

			return oTabFilter;
		},

		/**
		 * get the view, by viewname.
		 * @public
		 * @param {string} psViewName view name
		 * @return {object} oView view from tab
		 */
		getView: function(psViewName) {
			var oView;

			var aItems = this.getItems();
			for (var i = 0; i < aItems.length; i++) {
				var oTab = aItems[i];
				if (oTab.getContent && oTab.getContent() && oTab.getContent().length > 0) {
					var oTempView = oTab.getContent()[0];
					if (oTempView.getViewName && oTempView.getViewName() === psViewName) {
						oView = oTempView;
						break;
					}
				}
			}

			return oView;
		}
	});
});