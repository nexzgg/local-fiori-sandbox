/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"com/tgroup/lib/message/model/models"
], function(poJSONModel, poDevice, poMessageModel) {
	"use strict";

	return {
		/**
		 * read the f4 searchhelp fields and attributes.
		 * @public
		 * @param {String} psAppName app name
		 * @param {String} psFieldname field name
		 * @param {function} pfnSuccess The success function
		 * @param {function} pfnError The error function
		 */
		readF4SearchHelp: function(psAppName, psFieldname, pfnSuccess, pfnError) {
			// clear the messages
			poMessageModel.clearMessageModel();

			var aFilters = [];
			var oFilter = new sap.ui.model.Filter(
				"Function",
				sap.ui.model.FilterOperator.EQ,
				psAppName
			);
			aFilters.push(oFilter);

			oFilter = new sap.ui.model.Filter(
				"Fieldname",
				sap.ui.model.FilterOperator.EQ,
				psFieldname
			);
			aFilters.push(oFilter);

			var oDataProvider = sap.ui.getCore().getModel("dataprovider");
			// Building up the map of parameters used for our gateway read
			var mParameters = {
				success: pfnSuccess,
				error: pfnError,
				filters: aFilters,
				urlParameters: {
					$expand: "F4LowSet"
				}
			};
			// starting the gateway read
			oDataProvider.read("/F4Set", mParameters);
		},
		
		/**
		 * Method for reading tabs
		 * @public
		 * @param {string} psApplication The application name
		 * @param {string} psTabGroup The tab group
		 * @param {function} pfnSuccess The success function
		 * @param {function} pfnError The error function
		 * */
		readTabs: function(psApplication, psTabGroup, pfnSuccess, pfnError) {
			// clear the messages
			poMessageModel.clearMessageModel();

			var aFilters = [];
			var oFilter = new sap.ui.model.Filter(
				"Application",
				sap.ui.model.FilterOperator.EQ,
				psApplication
			);
			aFilters.push(oFilter);

			oFilter = new sap.ui.model.Filter(
				"TabGroup",
				sap.ui.model.FilterOperator.EQ,
				psTabGroup
			);
			aFilters.push(oFilter);

			var oDataProvider = sap.ui.getCore().getModel("dataprovider");
			// Building up the map of parameters used for our gateway read
			var mParameters = {
				success: pfnSuccess,
				error: pfnError,
				filters: aFilters
			};
			// starting the gateway read
			oDataProvider.read("/TabSet", mParameters);
		},
		
		/**
		 * read data for combobox or domain.
		 * @public
		 * @param {object} poModel model which read the data
		 * @param {string} psEntitySet entityset which we have to read
		 * @param {function} pfnSuccess success function
		 * @param {array} paFilters model filters
		 */
		readData: function(poModel, psEntitySet, pfnSuccess, paFilters) {
			// check all data existing
			if (poModel) {
					var mParameters = {
						success: pfnSuccess,
						filters: paFilters
					};
					poModel.read("/" + psEntitySet, mParameters);
			}
		}
	};

});