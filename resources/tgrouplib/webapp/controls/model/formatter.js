/** @module com/tgroup/lib/controls */
sap.ui.define([], function() {
	"use strict";

	return {

		/**
		 * Method for getting the width in pixel
		 * @public
		 * @param {integer} piWidth The integer value for the width in pixel
		 * @return {string} sWidth The width for the control
		 * */
		getWidth: function(piWidth) {
			var sWidth;
			if (piWidth && typeof piWidth !== "undefined") {
				sWidth = piWidth.toString() + "px";
			}
			return (sWidth);
		},

		/**
		 * Method for setting a control to visible depending on backend property
		 * @public
		 * @param {string} psFieldMode The mode of the field from the backend
		 * @return {boolean} bVisible The state showing if visible or not
		 * */
		getVisible: function(psFieldMode) {
			var bVisible = true;
			if (psFieldMode && typeof psFieldMode !== "undefined") {
				if (psFieldMode === "NS") {
					bVisible = false;
				}
			}
			return (bVisible);
		},

		/**
		 * Method for setting a control to delete leading zeroes
		 * @public
		 * @param {string} psNoLeadingZeroes flag no leading zeroes
		 * @return {boolean} bNoLeadingZeroes The state showing leading zeroes
		 * */
		getNoLeadingZeroes: function(psNoLeadingZeroes) {
			var bNoLeadingZeroes = false;
			if (psNoLeadingZeroes && psNoLeadingZeroes === "X") {
				bNoLeadingZeroes = true;
			}
			return (bNoLeadingZeroes);
		},

		/**
		 * Method for getting the enabled status of a specific control
		 * @public
		 * @param {string} psFieldMode The enabled status from the backend
		 * @return {boolean} bEnabled The enabled status for the control itself
		 * */
		getEnabled: function(psFieldMode) {
			var bEnabled = true;
			if (psFieldMode && typeof psFieldMode !== "undefined") {
				if (psFieldMode !== "") {
					if (psFieldMode === "OS") {
						bEnabled = false;
					}
				}
			}
			return (bEnabled);
		},

		/**
		 * Method for getting the alignment depending on backend properties
		 * @public
		 * @param {string} psAlign The alignment from the backend
		 * @return {string} sAlign The alignment for the control
		 * */
		getTextAlign: function(psAlign) {
			var sAlign = "Left";
			if (psAlign && typeof psAlign !== "undefined") {
				switch (psAlign) {
					case "L":
						sAlign = "Left";
						break;
					case "R":
						sAlign = "Right";
						break;
					case "C":
						sAlign = "Center";
						break;
					default:
						sAlign = "left";
				}
			}
			return (sAlign);
		},

		/**
		 * Method for getting the editable status of control
		 * @public
		 * @param {string} psFieldMode The editable state from the backend
		 * @return {boolean} bEditable The editable state for the control itself
		 * */
		getEditable: function(psFieldMode) {
			var bEditable = true;
			if (psFieldMode && typeof psFieldMode !== "undefined") {
				if (psFieldMode !== "") {
					if (psFieldMode === "NE") {
						bEditable = false;
					}
				}
			}
			return (bEditable);
		},

		/**
		 * Method for getting the required status of a field from the backend
		 * @public
		 * @param {string} psFieldType The field type from the backend
		 * @return {boolean} bRequired The field type for the control
		 * */
		getRequired: function(psFieldType) {
			var bRequired = false;
			switch (psFieldType) {
				case "KF":
					bRequired = false;
					break;
				case "MF":
					bRequired = true;
					break;
				default:
					bRequired = false;
			}
			return (bRequired);
		},

		/**
		 * Method used for getting the right type for value checking (validation)
		 * @public
		 * @param {string} psType The type of the field from the backend
		 * @return {object} oBindingType The type for the control
		 * */
		getBindingType: function(psType) {
			var oBindingType = new sap.ui.model.odata.type.String();
			if (psType && typeof psType !== "undefined") {
				switch (psType) {
					case "Edm.Boolean":
						oBindingType = new sap.ui.model.odata.type.Boolean();
						break;
					case "Edm.String":
						oBindingType = new sap.ui.model.odata.type.String();
						break;
					case "Edm.Decimal":
						oBindingType = new sap.ui.model.odata.type.Decimal();
						break;
					case "Edm.Double":
					case "Edm.Float":
						oBindingType = new sap.ui.model.type.Float();
						break;
					case "Edm.Int16":
					case "Edm.Int32":
					case "Edm.Int64":
					case "Edm.Byte":
						oBindingType = new sap.ui.model.type.Integer();
						break;
					case "Edm.DateTime":
						oBindingType = new sap.ui.model.type.Date();
						break;
					case "Edm.Time":
						oBindingType = new sap.ui.model.odata.type.Time();
						break;
					default:
						oBindingType = new sap.ui.model.odata.type.String();
				}
			}
			return (oBindingType);
		},

		/**
		 * Method for getting the maxLength of a specific control
		 * @public
		 * @param {string} psOutputLength The length from the backend
		 * @return {integer} iMaxLength The length for the control
		 * */
		getMaxLength: function(psOutputLength) {
			var iMaxLength = 0;
			if (psOutputLength && typeof psOutputLength !== "undefined") {
				if (psOutputLength !== "") {
					iMaxLength = parseInt(psOutputLength, 10);
				}
			}
			return (iMaxLength);
		},

		/**
		 * Method for getting and setting the type of an input
		 * @public
		 * @param {string} psType The type of the field
		 * @return {string} sInputType The type for the control
		 * */
		getType: function(psType) {
			var sInputType = "Text";
			if (psType && typeof psType !== "undefined") {
				switch (psType) {
					case "Edm.Boolean":
					case "Edm.String":
						sInputType = "Text";
						break;
					case "Edm.Decimal":
					case "Edm.Double":
					case "Edm.Float":
					case "Edm.Int16":
					case "Edm.Int32":
					case "Edm.Int64":
					case "Edm.Byte":
						sInputType = "Number";
						break;
					default:
						sInputType = "Text";
				}
			}
			return (sInputType);
		},

		/**
		 * get the abap flag from the boolean.
		 * @public
		 * @param {boolean} pbBool boolean
		 * @return {string} sFlag X or ""
		 */
		booleanToFlag: function(pbBool) {
			var sFlag = "";
			if (pbBool) {
				sFlag = "X";
			}
			return sFlag;
		},

		/**
		 * Functionality for getting a tooltip
		 * @public
		 * @param {string} psTooltip The string containing a JSON-Object
		 * @return {string } sTooltip The tooltip which should be displayed
		 * */
		getTooltip: function(psTooltip) {
			var sTooltip = "";
			if (psTooltip && typeof psTooltip !== "undefined") {
				if (psTooltip !== "") {
					//Convert it
					var aLines = JSON.parse(psTooltip);
					if (aLines.length !== 0) {
						for (var i = 0; i <= aLines.length - 1; i++) {
							sTooltip = sTooltip + aLines[i].TDLINE + "\n";
						}
					}
				}
			}
			return (sTooltip);
		}
	};
});