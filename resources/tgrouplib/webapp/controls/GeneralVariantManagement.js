sap.ui.define([
	"sap/ui/comp/variants/VariantManagement"
], function (poVariantManagement) {
	"use strict";

	/**
	 * @class GeneralVariantManagement
	 * This control handles a general variation management. The data of the variation is stored on the backend as a json string.
	 * The selectHandler() and saveHandler() can be used to get or set the json string data of an variation. In order to use
	 * this control on multiple different places, the variantname property is used to indentify, save and read the right data.
	 * 
	 * @param {function} [properties.selectHandler] Callback, that is called when a variant is selected. The data is passed in as a string.
	 * @param {function} [properties.saveHandler]   Callback, that is called when a variant is saved. The data, that has to be saved, has to be returned as a string.
	 * @param {string}   [properties.variantname]   Is used to indentify, save and read the right data
	 * 
	 * @alias com.tgroup.lib.controls.GeneralVariantManagement
	 * @author Maximilian Hinkel
	 */
	var oVariantManagement = poVariantManagement.extend("com.tgroup.lib.controls.GeneralVariantManagement", {
		_renderingDone: false,

		metadata: {
			properties: {
				variantname: {
					type: "string",
					default: "-"
				},
				selectHandler: {
					type: "function"
				},
				saveHandler: {
					type: "function"
				}
			}
		},

		init: function () {
			if (poVariantManagement.prototype.init) {
				poVariantManagement.prototype.init.apply(this, arguments);
			}
		},

		renderer: function (oRM, oControl) {
			sap.ui.comp.variants.VariantManagementRenderer.render(oRM, oControl);
		},

		onAfterRendering: function () {
			if (poVariantManagement.prototype.onAfterRendering) {
				poVariantManagement.prototype.onAfterRendering.apply(this, arguments);
			}

			if (!this._renderingDone) {
				this._buildVariantManagement();
				this._renderingDone = true;
			}
		},

		/* Building and rendering the variant management controls and items.
		 * @private
		 * 
		 * @author Maximilian Hinkel
		 */
		_buildVariantManagement: function () {
			var oVariantItem = new sap.ui.comp.variants.VariantItem({
				text: "{dataprovider>Variantitemname}",
				key: "{dataprovider>Variantitemid}",
				global: "{dataprovider>Ispublic}",
				author: "{dataprovider>Author}",
				labelReadOnly: true
			});

			this.bindAggregation(
				"variantItems", {
					path: "dataprovider>/VariantManagementSet",
					filters: [new sap.ui.model.Filter({
						path: "Variantname",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: this.getVariantname()
					})],
					template: oVariantItem
				}
			);

			this.setModel("dataprovider");

			this.bindElement({
				path: "/VariantManagementDefaultSet('" + this.getVariantname() + "')",
				model: "dataprovider"
			});

			this.bindProperty("defaultVariantKey", "dataprovider>Variantitemid");
			this.bindProperty("initialSelectionKey", "dataprovider>Variantitemid");

			this.attachSelect(this._onSelect);
			this.attachSave(this._onSave);
			this.attachManage(this._onManage);
			this.setShowShare(true);

		},

		/* General handler for selecting an variation item.
		 * @private
		 *
		 * @author Maximilian Hinkel
		 */
		_onSelect: function () {
			var oSelectedItem = this._getSelectedItem();
			if (oSelectedItem) {
				var oVariationItem = oSelectedItem.getBindingContext("dataprovider").getObject();
				var sValue = oVariationItem.Variantvalue;
			}

			this.getSelectHandler()(sValue ? sValue : null);
		},

		/* General handler for saving an variation item.
		 * @private
		 *
		 * @param poEvent Event object with additional informations for saving a variant.
		 *
		 * @author Maximilian Hinkel
		 */
		_onSave: function (poEvent) {
			var oParameters = poEvent.getParameters();
			var sValue = this.getSaveHandler()();
			var oModel = sap.ui.getCore().getModel("dataprovider");
			var oVariantData = {
				Variantname: this.getVariantname(),
				Variantitemname: oParameters.name,
				Variantvalue: sValue,
				Ispublic: oParameters.global,
				Isdefault: oParameters.def
			};

			oModel.create("/VariantManagementSet", oVariantData);
		},

		/* General handler for managing an variation item.
		 * @private
		 *
		 * @param poEvent Event object with additional informations for managing a variant.
		 *
		 * @author Maximilian Hinkel
		 */
		_onManage: function (poEvent) {
			var oModel = sap.ui.getCore().getModel("dataprovider");
			var aDeleted = poEvent.getParameters().deleted;

			for (var i = 0; i < aDeleted.length; i++) {
				var sPath = oModel.createKey("/VariantManagementSet", {
					Variantname: this.getVariantname(),
					Variantitemid: aDeleted[i]
				});
				oModel.remove(sPath);
			}
		}

	});

	return oVariantManagement;
});