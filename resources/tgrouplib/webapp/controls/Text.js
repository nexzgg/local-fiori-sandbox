/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"com/tgroup/lib/general/model/formatter",
	"sap/m/Text"
], function(poHelper, poFormatter, poGeneralFormatter, poText) {
	"use strict";
	return poText.extend("com.tgroup.lib.controls.Text", {

		_done: false,
		_bindingTypeDone: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.TextRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality for setting the text
		 * @public
		 * @override
		 * @param {object} poText text
		 **/
		setText: function(poText) {
			if (sap.m.Text.prototype.setText) {
				sap.m.Text.prototype.setText.apply(this, arguments);
			}
			this._setBindingType();
		},

		/**
		 * set the binding type.
		 */
		_setBindingType: function() {
			var sLayout = this.getLayoutName();
			var sField = this.getField();
			var oField = poHelper.getField(sLayout, sField);

			// Checking if we do have a falid name for the control itself
			if (!this._bindingTypeDone && sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField &&
				typeof oField !== "undefined") {
				var oBindingInfo = this.getBindingInfo("text");
				if (oBindingInfo) {
					var oBinding = oBindingInfo.binding;
					if (oBinding && !oBinding.getType()) {
						this._bindingTypeDone = true;
						// change binding type from input type
						var oBind = {
							path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() : oBinding.getPath()
						};
						if (poFormatter.getNoLeadingZeroes(oField.NoLeadingZeroes)) {
							oBind.formatter = function(psValue) {
								return poGeneralFormatter.deleteLeadingZeroes(psValue);
							};
						} else if (oField.Decimals > 0) {
							oBind.type = new sap.ui.model.type.Float({
								maxFractionDigits: oField.Decimals
							});
						} else if (poHelper._isInputTypeComboBox(oField.InputType) || oField.ComboBox) {
							oBind.formatter = function(psValue) {
								var sValue = psValue + "";
								if (sValue && sValue !== "") {
									// combobox text handling
									poHelper.getTextFromComboBox(oField, function(psText) {
										this.setText(psText);
									}.bind(this), psValue);
								}
								return psValue;
							}.bind(this);
						} else {
							var oBindingType;
							if (oField.InputType) {
								oBindingType = poHelper.getBindingTypeFromInputType(oField.InputType);
							} else if (oField.Type) {
								oBindingType = poFormatter.getBindingType(oField.Type);
							}
							oBind.type = oBindingType;
						}
						this.bindProperty("text", oBind);
					}
				}
			}
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.Text.prototype.onAfterRendering) {
				sap.m.Text.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				this._done = true;

				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Text
					// - TextAlign
					// - Width
					// - Required
					// - Visible

					// check if no text bound only show the field text
					var oBindingInfo = this.getBindingInfo("text");
					if (!oBindingInfo) {
						// Text
						this.setText(oField.Text);

						// Required
						if (this.setRequired) {
							this.setRequired(poFormatter.getRequired(oField.FieldType));
						}
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

				}
			}
		}

	});
});