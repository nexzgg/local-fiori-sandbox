/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/ui/core/Control",
	"com/tgroup/lib/controls/lib/jsonpath/jsonpath",
	"com/tgroup/lib/controls/util/helper"
], function(contrl, jsonPath, poControlHelper) {
	"use strict";
	return contrl.extend("com.tgroup.lib.controls.DynamicAttribute", {

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				attributeKey: {
					type: "string"
				},
				text: {
					type: "string"
				},
				context: {
					type: "object"
				}
			}
		},

		_oControl: undefined,
		_sControlType: undefined,
		_oPopover: undefined,
		_aNavItem: undefined,
		_bDone: false,

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			poControl._getNavItems();
			poControl._bDone = false;

			poRm.write("<div");

			//next, render the control information, this handles your sId (you must do this for your control to be properly tracked by ui5).
			poRm.writeControlData(poControl);
			poRm.write(">");

			var oNewControl;
			if (poControl._aNavItem.length !== 0) {
				oNewControl = new sap.m.Link({
					text: poControl.getText()
				});
				poControl._sControlType = 'LINK';
			} else {
				oNewControl = new sap.m.Text({
					text: poControl.getText()
				});
				poControl._sControlType = 'TEXT';
			}
			poRm.renderControl(oNewControl);
			poControl._oControl = oNewControl;

			//and obviously, close off our div
			poRm.write("</div>");

		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {

			if (!this._bDone && this._sControlType === "LINK") {
				//set properties
				this._oControl.attachPress(this._openPopover.bind({
					this: this,
					link: this._oControl
				}));
				
					this._buildPopover(this);

				//build popover
				this._buildPopover(this);
				this._bDone = true;
			}

		},

		/**
		 * Functionality to buildPopover onAfterRendering
		 * @private
		 * */
		_buildPopover: function() {
			this._oPopover = poControlHelper.buildNavigationPopover(this._aNavItem, this.getContext(), this.getText());
		},

		/**
		 * Functionality to open Popover
		 * @private
		 * */
		_openPopover: function() {
			this.this._oPopover.openBy(this.link);
		},

		/**
		 * Functionality to get navigation items from model
		 * @private
		 * */
		_getNavItems: function() {
			//get nav items
			this._aNavItem = poControlHelper.getNavItems(this.getAttributeKey());
		}

	});
});