/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"sap/m/DatePicker",
	"sap/ui/commons/RichTooltip"
], function(poHelper, poFormatter, poDatePicker) {
	"use strict";
	return poDatePicker.extend("com.tgroup.lib.controls.DatePicker", {
		/**
		 * Variables used for the complete control
		 * */
		_done: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				mandatory: {
					type: "boolean",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.DatePickerRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.DatePicker.prototype.onAfterRendering) {
				sap.m.DatePicker.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Width
					// - Enabled
					// - Editable
					// - TextAlign
					// - Required
					// - Visible
					// - Tooltip

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						//For 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for value live change
						this.attachChange(function(poEvent) {
							var sValue = poEvent.getParameter("value");
							if (sValue === "") {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					//this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
					if (oField.Tooltip && typeof oField.Tooltip !== "undefined" && oField.Tooltip !== "") {
						this.setTooltip(new sap.ui.commons.RichTooltip({
							text: poFormatter.getTooltip(oField.Tooltip),
							collision: "fit"
						}));
					}
					
					// Default Value
					if (oField.DefaultValue && typeof oField.DefaultValue !== "undefined") {
						if (oField.DefaultValue === "CUR_DATE") {
							this.setDateValue(new Date());
						}
					}
					
				}
				this._done = true;
			}
		}
	});
});