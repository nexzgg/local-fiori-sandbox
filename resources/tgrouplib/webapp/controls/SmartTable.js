/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/ui/comp/smarttable/SmartTable"
], function(poSmartTable) {
	"use strict";
	return poSmartTable.extend("com.tgroup.lib.controls.SmartTable", {

		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				modelName: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRM, poControl) {
			sap.ui.comp.smarttable.SmartTableRenderer.render(poRM, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.ui.comp.smarttable.SmartTable.prototype.onAfterRendering) {
				sap.ui.comp.smarttable.SmartTable.prototype.onAfterRendering.apply(this, arguments);
			}

			if (!this._done) {
				// set model
				if (this.getModelName() && this.getModelName() !== "") {
					var oModel = sap.ui.getCore().getModel(this.getModelName());
					if (oModel) {
						this.setModel(oModel);
					}
				}

				// set layout visible columns
				if (this.getLayoutName() && this.getLayoutName() !== "") {
					var oLayouts = sap.ui.getCore().getModel("layout");
					if (oLayouts) {
						var oLayout = oLayouts.getProperty("/" + this.getLayoutName());
						// set column of layout to visible
						if (oLayout && oLayout.Fields) {
							var oElements = oLayout.Fields;
							var sVisibleFields = "";
							for (var property in oElements) {
								if (oElements.hasOwnProperty(property)) {
									sVisibleFields += property + ",";
								}
							}
							if (sVisibleFields !== "") {
								sVisibleFields = sVisibleFields.slice(0, -1);
								this.setInitiallyVisibleFields(sVisibleFields);
							}
						}
					}

				}
				this._done = true;
			}
		}

	});
});