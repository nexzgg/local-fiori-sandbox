/** @module com/tgroup/lib/controls */
function formatKommaPunktOutput(num, deci) {
	var parts;
	if (deci)
		parts = num.toFixed(deci).split(".");
	else
		parts = num.toString().split(".");

	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	return parts.join(",");
}

/************************************** HTML II 5 Tacho **********************************************/
var CAT;
if (typeof CAT !== "object") {
	CAT = {};
}

CAT.drawingTacho2 = function(id, _params) {
	var params = (typeof _params === "object") ? _params : {};

	var width = (params.width) ? parseFloat(params.width) : 300;
	var minValue = (params.low) ? parseFloat(params.low) : 0;
	var maxValue = (params.high) ? parseFloat(params.high) : 0;
	var normValue = (params.normal) ? parseFloat(params.normal) : 0;
	var tolMin = (params.tolMin) ? parseFloat(params.tolMin) : 0;
	var tolMax = (params.tolMax) ? parseFloat(params.tolMax) : 0;
	var tollerance = (params.toleranz) ? parseFloat(params.toleranz) : 0.3333;
	var curValue = (params.value) ? parseFloat(params.value) : 0;
	var unit = (params.unit) ? params.unit : "";
	var green = (params.green) ? params.green : "#007833";
	var red = (params.red) ? params.red : "#bb0000";
	var background = (params.background) ? params.background : "#BEBEBE";
	var needle = (params.needle) ? params.needle : "#27363d";

	var canvasTacho = $('#' + id).find("canvas")[0];
	if (!canvasTacho) {
		return false;
	}

	var height = width / 3 * 2.2;

	if (canvasTacho && canvasTacho.getContext) {
		canvasTacho.width = width;
		canvasTacho.height = height;

		var ctx = canvasTacho.getContext("2d");
		if (ctx) {

			// scale from standard width
			var iScale = width / 300;
			ctx.scale(iScale, iScale);

			var grad;
			var normGrad;
			var greenStart = 0,
				greenEnd = 0;

			// StHe: Checking the maximal and minimum values
			if (tolMin === 0) {
				tolMin = normValue - (maxValue - minValue) * tollerance / 2;
			}
			if (tolMax === 0) {
				tolMax = normValue + (maxValue - minValue) * tollerance / 2;
			}

			if (tolMin != 0) {
				greenStart = (((tolMin - minValue) / (maxValue - minValue) + 1) * Math.PI);
			}
			if (tolMax != 0) {
				greenEnd = (((tolMax - minValue) / (maxValue - minValue) + 1) * Math.PI);
			}

			grad = ((curValue - minValue) / (maxValue - minValue) * 180);
			normGrad = ((normValue - minValue) / (maxValue - minValue) * 180);

			if (grad > 180)
				grad = 180;

			CAT.drawTacho2(ctx, greenStart, greenEnd, normValue, normGrad, green, red, background);
			CAT.drawTachoLabel2(ctx, minValue, maxValue, curValue, unit);
			CAT.drawNeedle2(ctx, grad, needle);

			if (!CAT.storeTachos) CAT.storeTachos = [];

			var storeTacho = {
				"id": id,
				"params": params
			};
			var exists = false;
			for (var i = 0; i < CAT.storeTachos.length; i++) {
				if (CAT.storeTachos[i].id == id) {
					CAT.storeTachos[i].params = params;
					exists = true;
					break;
				}

			}
			if (!exists)
				CAT.storeTachos.push(storeTacho);
		}
	}
};

CAT.drawTacho2 = function(context, _greenStart, _greenEnd, normValue, normGrad, green, red, background) {
	var heighPoint = 112;
	var widthMiddle = 100;
	var radius = 99;
	var top = 12;

	var redColor = red;

	var greenColor = green;

	var greenStart = (typeof _greenStart == 'number') ? _greenStart : 0;
	var greenEnd = (typeof _greenEnd == 'number') ? _greenEnd : 0;

	if (context) {
		var ctx = context;

		ctx.beginPath();
		ctx.fillStyle = background;
		ctx.arc(widthMiddle, heighPoint, radius, 1 * Math.PI, 2 * Math.PI);
		ctx.strokeStyle = '#d2d2d2';
		ctx.stroke();
		ctx.fill();

		ctx.globalCompositeOperation = "source-over";

		if (greenStart != 0 && greenEnd != 0) {
			ctx.beginPath();
			ctx.fillStyle = greenColor;
			ctx.moveTo(widthMiddle, heighPoint);
			ctx.arc(widthMiddle, heighPoint, radius, greenStart, greenEnd);
			ctx.closePath();
			ctx.fill();

			if (greenEnd < 2 * Math.PI) {
				ctx.beginPath();
				ctx.fillStyle = redColor;
				ctx.moveTo(widthMiddle, heighPoint);
				ctx.arc(widthMiddle, heighPoint, radius, greenEnd, 2 * Math.PI);
				ctx.closePath();
				ctx.fill();
			}

			ctx.beginPath();

			/* Punkt 1*/

			var xPt = 100 - Math.cos(normGrad * (Math.PI / 180)) * (heighPoint / 2 - 2);
			var yPt = 100 - Math.sin(normGrad * (Math.PI / 180)) * (heighPoint / 2 - 2) + top;

			ctx.moveTo(xPt, yPt);
			//ctx.moveTo(widthMiddle, 12);

			xPt = 100 - Math.cos(normGrad * (Math.PI / 180)) * 100;
			yPt = 100 - Math.sin(normGrad * (Math.PI / 180)) * 100 + top;

			ctx.lineTo(xPt, yPt);
			//ctx.lineTo(widthMiddle, heighPoint/2);
			ctx.globalCompositeOperation = "destination-out";
			ctx.strokeStyle = "#d2d2d2";
			ctx.lineWidth = 4;
			ctx.stroke();
		}

		ctx.globalCompositeOperation = "source-over";
		ctx.beginPath();
		ctx.fillStyle = '#f2f2f2';
		ctx.arc(widthMiddle, heighPoint, heighPoint / 2, 1 * Math.PI, 3 * Math.PI);
		ctx.strokeStyle = '#d2d2d2';
		ctx.lineWidth = 2;
		ctx.stroke();

		ctx.globalCompositeOperation = "destination-out";
		ctx.fill();

		ctx.fillRect(0, 112, 200, 150);
		ctx.fill();
	}
};

CAT.drawNeedle2 = function(context, grad, needle) {
	var needleColor = needle;
	var heighPoint = 100;
	var top = 12;

	context.globalCompositeOperation = "source-over";

	context.lineWidth = 2;
	context.strokeStyle = needleColor;
	context.fillStyle = needleColor;

	/* Pfad Los*/
	context.beginPath();

	/* Punkt 1*/
	var xPt = heighPoint - Math.cos(grad * (Math.PI / 180)) * 60;
	var yPt = heighPoint - Math.sin(grad * (Math.PI / 180)) * 60 + top;

	context.moveTo(xPt, yPt);

	/* Links unten Dreieck */
	xPt = heighPoint - Math.cos((grad + 10) * (Math.PI / 180)) * 35;
	yPt = heighPoint - Math.sin((grad + 10) * (Math.PI / 180)) * 35 + top;

	context.lineTo(xPt, yPt);

	/* Rechts unten Dreieck */
	xPt = heighPoint - Math.cos((grad - 10) * (Math.PI / 180)) * 35;
	yPt = heighPoint - Math.sin((grad - 10) * (Math.PI / 180)) * 35 + top;

	context.lineTo(xPt, yPt);

	xPt = heighPoint - Math.cos(grad * (Math.PI / 180)) * 60;
	yPt = heighPoint - Math.sin(grad * (Math.PI / 180)) * 60 + top;

	context.lineTo(xPt, yPt);

	xPt = heighPoint - Math.cos(grad * (Math.PI / 180)) * 99;
	yPt = heighPoint - Math.sin(grad * (Math.PI / 180)) * 99 + top;

	context.lineTo(xPt, yPt);
	context.stroke();
	context.fill();
};

CAT.drawTachoLabel2 = function(context, min, max, curValue, unit) {
	var top = 122;
	context.globalCompositeOperation = "source-over";
	context.font = '600 12px Arial';
	context.fillStyle = '#000000';
	context.textAlign = 'right';
	var minStr = formatKommaPunktOutput(min);
	context.fillText(minStr, 48, top);

	context.fillStyle = '#000000';
	context.textAlign = 'right';
	var maxStr = formatKommaPunktOutput(max);
	context.fillText(maxStr, 198, top);

	context.fillStyle = '#000000';
	context.textAlign = 'center';
	var middle = parseFloat(min + (max - min) / 2);
	middle = formatKommaPunktOutput(middle);
	context.fillText(middle, 100, 12);

	context.fillStyle = '#000000';
	context.textAlign = 'center';
	context.font = '900 20px Arial';
	curValue = formatKommaPunktOutput(curValue);
	context.fillText(curValue + " " + unit, 100, 110);
};

CAT.redrawTacho = function(id, _params) {
	var params = (typeof _params == "object") ? _params : false;

	if (!CAT.storeTachos) return;

	var curTacho = false;
	for (var i = 0; i < CAT.storeTachos.length; i++) {
		if (CAT.storeTachos[i].id == id) {
			curTacho = CAT.storeTachos[i];
			break;
		}
	}

	if (curTacho != false) {

		if (params) {
			var newParams = {};
			newParams.width = (params.width) ? parseFloat(params.width) : curTacho.params.width;
			newParams.low = (params.low) ? parseFloat(params.low) : curTacho.params.low;
			newParams.high = (params.high) ? parseFloat(params.high) : curTacho.params.high;
			newParams.normal = (params.normal) ? parseFloat(params.normal) : curTacho.params.normal;
			newParams.toleranz = (params.toleranz) ? parseFloat(params.toleranz) : curTacho.params.toleranz;
			newParams.value = (params.value) ? parseFloat(params.value) : curTacho.params.value;

		} else {
			newParams = curTacho.params;
		}

		CAT.drawingTacho2(id, newParams);
	}
};