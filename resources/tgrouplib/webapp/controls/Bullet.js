/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/ui/core/Control",
	"com/tgroup/lib/controls/lib/d3/d3",
	"com/tgroup/lib/controls/lib/bulleT/bulleT"
], function(poControl) {
	"use strict";

	return poControl.extend("com.tgroup.lib.controls.Bullet", {

		_done: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				id: {
					type: "string",
					defaultValue: "0"
				},
				referenceValue: {
					type: "string",
					defaultValue: "0"
				},
				actualValue: {
					type: "string",
					defaultValue: "0"
				},
				minValue: {
					type: "string",
					defaultValue: "0"
				},
				maxValue: {
					type: "string",
					defaultValue: "0"
				},
				lowerValue1: {
					type: "string",
					defaultValue: "0"
				},
				lowerValue2: {
					type: "string",
					defaultValue: "0"
				},
				upperValue1: {
					type: "string",
					defaultValue: "0"
				},
				upperValue2: {
					type: "string",
					defaultValue: "0"
				},
				optimalValue1: {
					type: "string",
					defaultValue: "0"
				},
				optimalValue2: {
					type: "string",
					defaultValue: "0"
				},
				width: {
					type: "int",
					defaultValue: 300
				},
				height: {
					type: "int",
					defaultValue: 45
				},
				direction: {
					type: "string",
					defaultValue: "horizontal"
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			oRM.write("<html:div ");
			oRM.writeControlData(oControl);
			oRM.writeClasses();
			oRM.write("></html:div>");
		},

		/**
		 * on after rendering.
		 * Initialisation of the gauge
		 */
		onAfterRendering: function() {
			//Checking for the needed information

			//Ranges
			var iMinValue;
			if (this.getMinValue() && typeof this.getMinValue() !== "undefined") {
				if (this.getMinValue() !== "") {
					iMinValue = parseInt(this.getMinValue(), 10);
				}
			}
			var iMaxValue;
			if (this.getMaxValue() && typeof this.getMaxValue() !== "undefined" && this.getMaxValue() !== "0") {
				if (this.getMaxValue() !== "") {
					iMaxValue = parseInt(this.getMaxValue(), 10);
				}
			}

			//Checking the current value
			var nCurrentValue = parseFloat(this.getActualValue());
			if (nCurrentValue >= iMaxValue) {
				nCurrentValue = iMaxValue;
			}

			var oDataObject = [{
				"terjedelem": [iMinValue, iMaxValue],
				"measures": [parseFloat(this.getLowerValue1()), parseFloat(this.getLowerValue2()), parseFloat(this.getOptimalValue1()),
					parseFloat(this.getOptimalValue2()), parseFloat(this.getUpperValue1()),
					parseFloat(this.getUpperValue2())
				],
				"ranges": [parseFloat(this.getLowerValue1()), parseFloat(this.getLowerValue2()), parseFloat(this.getReferenceValue()),
					parseFloat(this.getUpperValue1()), parseFloat(this.getUpperValue2())
				],
				"markers": [nCurrentValue, nCurrentValue]
			}];

			// Checking the height and width of the control
			var sWidth = this.getWidth();
			var sHeight = this.getHeight();
			var iWidth, iHeight, iPosition;
			if (sWidth !== parseInt(sWidth, 10)) {
				if (sWidth.indexOf("px") !== -1) {
					//The width contains a string value
					iPosition = sWidth.indexOf("px");
					iWidth = parseInt(sWidth.substring(0, iPosition), 10);
				} else {
					iWidth = parseInt(sWidth, 10);
				}
			} else {
				iWidth = parseInt(sWidth, 10);
			}

			if (sHeight !== parseInt(sHeight, 10)) {
				if (sHeight.indexOf("px") !== -1) {
					//The height contains a string value
					iPosition = sHeight.indexOf("px");
					iHeight = parseInt(sHeight.substring(0, iPosition), 10);
				} else {
					iHeight = parseInt(sHeight, 10);
				}
			} else {
				iHeight = parseInt(sHeight, 10);
			}

			var oMargin = {
				top: 5,
				right: 5,
				bottom: 20,
				left: 60
			};
			var iChartWidth = iWidth - oMargin.left - oMargin.right;
			var iChartHeight = iHeight - oMargin.top - oMargin.bottom;

			var oChart = d3.bulleT();
			oChart.width(iChartWidth);
			oChart.height(iChartHeight);

			function bulleT(poData, psContainerId, psDirection) {
				var bVertical;
				//Checking the direction
				if (psDirection === "vertical") {
					bVertical = true;
				} else {
					bVertical = false;
				}

				var svg = d3.select(psContainerId).selectAll("svg")
					.data(poData)
					.enter().append("svg")
					.attr("class", "bulleT")
					.attr("width", iWidth)
					.attr("height", iHeight)
					.append("g")
					.attr("transform", function() {
						if (psDirection === "vertical") {
							return "rotate(-90)translate(" + -(iHeight - oMargin.left) + ",10)";
						} else {
							return "translate(" + oMargin.left + "," + oMargin.top + ")";
						}
					})
					.call(oChart.vertical(bVertical));

				var title = svg.append("g")
					.style("text-anchor", function() {
						if (psDirection === "vertical") {
							return "middle";
						} else {
							return "end";
						}
					})
					.attr("transform", function() {
						if (psDirection === "vertical") {
							return "rotate(90)translate(" + iWidth / 4 + ",10)";
						} else {
							return "translate(0," + iHeight / 3 + ")";
						}
					});

				title.append("text")
					.attr("class", "title")
					.text(function(d) {
						return d.title;
					});

				title.append("text")
					.attr("dy", "1.2em")
					.text(function(d) {
						return d.dimension;
					});

				title.append("text")
					.attr("class", function(d) {
						switch (true) {
							case ((d.markers[1] < 30) || (d.markers[1]) > 70):
								return "subtitle s04";
							case ((d.markers[1] >= 30) && (d.markers[1] < 40)):
								return "subtitle s13";
							case ((d.markers[1] >= 40) && (d.markers[1] <= 60)):
								return "subtitle s2";
							case ((d.markers[1] > 60) && (d.markers[1] <= 70)):
								return "subtitle s13";
						}
					})
					.attr("dy", "2.4em")
					.text(function(d) {
						return d.subtitle;
					});
			}
			bulleT(oDataObject, "#" + this.getId(), this.getDirection());
		}
	});
});