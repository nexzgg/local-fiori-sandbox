/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/general/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"sap/ui/comp/smartfilterbar/SmartFilterBar"
], function(poHelper, poFormatter, poSmartFilterBar) {
	"use strict";
	return poSmartFilterBar.extend("com.tgroup.lib.controls.SmartFilterBar", {

		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				modelName: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} oControl The control itself
		 * */
		renderer: function(poRm, oControl) {
			sap.ui.comp.smartfilterbar.SmartFilterBarRenderer.render(poRm, oControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.ui.comp.smartfilterbar.SmartFilterBar.prototype.onAfterRendering) {
				sap.ui.comp.smartfilterbar.SmartFilterBar.prototype.onAfterRendering.apply(this, arguments);
			}

			if (!this._done) {
				// set model
				if (this.getModelName() && this.getModelName() !== "") {
					this.setModel(sap.ui.getCore().getModel(this.getModelName()));
				}

				this.attachInitialise(function() {
					// set layout visible filters
					if (this.getLayoutName() && this.getLayoutName() !== "") {
						var oLayouts = sap.ui.getCore().getModel("layout");
						if (oLayouts) {
							var oLayout = oLayouts.getProperty("/" + this.getLayoutName());
							// set column of layout to visible
							if (oLayout && oLayout.Fields) {
								var oElements = oLayout.Fields;
								for (var property in oElements) {
									if (oElements.hasOwnProperty(property)) {
										this.addFieldToAdvancedArea(property);
										
										// required
										var bRequired = poFormatter.getRequired(oElements[property].FieldType);
										if (bRequired) {
											var oFilterGroupItem = poHelper.getObject(this.getFilterGroupItems(), { name: property });
											if (oFilterGroupItem) {
												oFilterGroupItem.setMandatory(true);
											}
										}
									}
								}
								
							}
						}
					}
				});

				this._done = true;
			}

		}
	});
});