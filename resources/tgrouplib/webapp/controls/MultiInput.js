/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"sap/m/MultiInput",
	"sap/m/Token",
	"sap/ui/commons/RichTooltip"
], function(poHelper, poFormatter, poMulti, poToken) {
	"use strict";
	return poMulti.extend("com.tgroup.lib.controls.MultiInput", {

		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				enabled: {
					type: "boolean",
					defaultValue: true
				},
				title: {
					type: "string",
					defaultValue: ""
				},
				key: {
					type: "string",
					defaultValue: ""
				},
				descriptionKey: {
					type: "string",
					defaultValue: ""
				},
				supportMultiselect: {
					type: "boolean",
					defaultValue: true
				},
				supportRanges: {
					type: "boolean",
					defaultValue: true
				},
				filterBarExpanded: {
					type: "boolean",
					defaultValue: true
				},
				mandatory: {
					type: "boolean",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				},
				hasSuggestionItems: {
					type: "boolean",
					defaultValue: false
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				},
				suggestionKey: {
					type: "string[]",
					defaultValue: []
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			sap.m.MultiInputRenderer.render(oRM, oControl);
		},

		onAfterRendering: function() {
			if (sap.m.MultiInput.prototype.onAfterRendering) {
				sap.m.MultiInput.prototype.onAfterRendering.apply(this, arguments);
			}

			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);
				var bHasShlp = false;

				// add validator. take value and make a Token
				this.addValidator(function(poData) {

					var oToken = new poToken({
						key: (poData.suggestionObject) ? poData.suggestionObject.getKey() : poData.text,
						text: poData.text
					});

					poData.asyncCallback(oToken);

					return sap.m.MultiInput.WaitForAsyncValidation;
				});

				// Checking if we do have a falid name for the control itself
				if (sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField && typeof oField !==
					"undefined") {

					// Time to set the specific properties for the control (depending on the aggregation of course)
					// Important fields:
					// - Type
					// - MaxLength
					// - Width
					// - Enabled
					// - Editable
					// - TextAlign
					// - Required
					// - Visible
					// - Text
					// - Tooltip

					// Type (only if we have input / multiInput)
					this.setType(poFormatter.getType(oField.Type));

					// MaxLength (only if we have input)
					this.setMaxLength(poFormatter.getMaxLength(oField.OutputLength));

					// has search help. multi input attach value help request
					if (oField.HasShlp === "X") {
						bHasShlp = true;
						this._addSearchHelp(this, oField);
					}
					this.setShowValueHelp(bHasShlp);

					// Width
					if (!this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						// For version 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for value live change
						this.attachLiveChange(function(poEvent) {
							var sValue = poEvent.getParameter("value");
							if (sValue === "") {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					//this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
					if (oField.Tooltip && typeof oField.Tooltip !== "undefined" && oField.Tooltip !== "") {
						this.setTooltip(new sap.ui.commons.RichTooltip({
							text: poFormatter.getTooltip(oField.Tooltip),
							collision: "fit"
						}));
					}

					// Checking for suggestion items
					if (oField.ComboBox && typeof oField.ComboBox !== "undefined" && !bHasShlp) {
						this.setHasSuggestionItems(true);
						this.setShowSuggestion(true);

						var sModelPath = oField.ComboBox.ModelName;
						if (sModelPath) {
							var oModel = sap.ui.getCore().getModel(sModelPath);
							if (!oModel) {
								var mSetting = {
									disableHeadRequestForToken: true
								};
								// model not exist, create model with service
								if (oField.ComboBox.Service && oField.ComboBox.Service !== "") {
									oModel = new sap.ui.model.odata.v2.ODataModel(oField.ComboBox.Service, mSetting);
									sap.ui.getCore().setModel(oModel, sModelPath);
								}
							}
						}

						this.setModel(oModel, sModelPath);

						var oBindingInfo = this.getBindingInfo("tokens");
						if (oBindingInfo && typeof oBindingInfo !== "undefined") {
							var oBinding = oBindingInfo.binding;
						}

						if (oBinding && typeof oBinding !== "undefined") {
							this.bindProperty("suggestionKey", {
								path: this.getBindingModel() + ">" + oBinding.getPath() + "Key"
							});
						}

						this.bindAggregation("suggestionItems", oField.ComboBox.ModelName + ">/" + oField.ComboBox.EntitySet, new sap.ui.core.Item({
							text: "{" + oField.ComboBox.ModelName + ">" + oField.ComboBox.TextField + "}",
							key: "{" + oField.ComboBox.ModelName + ">" + oField.ComboBox.KeyField + "}"
						}));
						this.setFilterFunction(function(psTerm, oItem) {
							var sText = oItem.getText().replace(/[^a-zA-Z0-9-]/gi, "");
							var sTerm = psTerm.replace(/[^a-zA-Z0-9-]/gi, "");
							return (sText.match(new RegExp(sTerm, "i")));
						});
						this.attachSuggest(function(poEvent) {
							var sTerm = poEvent.getParameter("suggestValue");
							var aFilters = [];
							if (sTerm) {
								aFilters.push(new sap.ui.model.Filter(oField.ComboBox.TextField, sap.ui.model.FilterOperator.Contains, sTerm));
							}
							poEvent.getSource().getBinding("suggestionItems").filter(aFilters);
						});
						this.attachTokenChange(function(poEvent) {
							var oSelectedItem = poEvent.getParameter("token");
							var sType = poEvent.getParameter("type");
							if (oSelectedItem) {
								var sKey = oSelectedItem.getKey();
								if (sKey && typeof sKey !== "undefined") {
									if (sKey !== "") {
										if (sType === "added") {
											this.setSuggestionKey(this.getSuggestionKey().concat([sKey]));
										} else if (sType === "removed") {
											var aKey = this.getSuggestionKey();
											for (var i = 0; i < aKey.length; i++) {
												var sSuggestionKey = aKey[i];
												if (sSuggestionKey === sKey) {
													aKey.splice(i, 1);
													this.setSuggestionKey(aKey);
													break;
												}
											}
										} else if (sType === "removedAll") {
											this.setSuggestionKey([]);
										}
									}
								}
							}
						}.bind(this));
					}
				}

				// has no search help, add ranges value help dialog
				if (!bHasShlp) {
					this._addRangesSearchHelp(this);
				}

				this._done = true;
			}
		},

		/**
		 * add ranges search help. 
		 * @private
		 * @param {object} poMultiInput input control
		 */
		_addRangesSearchHelp: function(poMultiInput) {
			this._addCustomData(poMultiInput);
			poMultiInput.attachValueHelpRequest(function() {
				poHelper.onRangeValueHelpRequest(this);
			});
		},

		/**
		 * add search help event. value help dialog
		 * @private
		 * @param {object} poMultiInput input control
		 * @param {object} poField field object
		 */
		_addSearchHelp: function(poMultiInput, poField) {
			this._addCustomData(poMultiInput, poField);
			poMultiInput.attachValueHelpRequest(function() {
				poHelper.onValueHelpRequest(this, "F4");
			});
		},

		_addCustomData: function(poMultiInput, poField) {
			// set custom data with search help infos
			poMultiInput.addCustomData(new sap.ui.core.CustomData({
				key: "appName",
				value: poHelper.getAppName()
			}));
			// do boolean as string
			poMultiInput.addCustomData(new sap.ui.core.CustomData({
				key: "filterBarExpanded",
				value: "" + this.getFilterBarExpanded() + ""
			}));
			poMultiInput.addCustomData(new sap.ui.core.CustomData({
				key: "supportMultiselect",
				value: "" + this.getSupportMultiselect() + ""
			}));
			poMultiInput.addCustomData(new sap.ui.core.CustomData({
				key: "supportRanges",
				value: "" + this.getSupportRanges() + ""
			}));

			// field is not initial
			if (poField) {
				poMultiInput.addCustomData(new sap.ui.core.CustomData({
					key: "fieldname",
					value: poField.ElementName
				}));

				poMultiInput.addCustomData(new sap.ui.core.CustomData({
					key: "title",
					value: (poMultiInput.getTitle() !== "") ? poMultiInput.getTitle() : poField.Text
				}));

				poMultiInput.addCustomData(new sap.ui.core.CustomData({
					key: "descriptionKey",
					value: (poMultiInput.getDescriptionKey() !== "") ? poMultiInput.getDescriptionKey() : poField.ElementName
				}));

				poMultiInput.addCustomData(new sap.ui.core.CustomData({
					key: "key",
					value: (poMultiInput.getKey() !== "") ? poMultiInput.getKey() : poField.ElementName
				}));
			}
		}
	});
});