/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"com/tgroup/lib/general/model/formatter",
	"sap/m/Label"
], function(poHelper, poFormatter, poGeneralFormatter, poLabel) {
	"use strict";
	return poLabel.extend("com.tgroup.lib.controls.Label", {

		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.LabelRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.Label.prototype.onAfterRendering) {
				sap.m.Label.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				this._done = true;

				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Text
					// - TextAlign
					// - Width
					// - Required
					// - Visible

					// check if no text bound only show the field text
					var oBindingInfo = this.getBindingInfo("text");
					if (oBindingInfo) {
						var oBinding = oBindingInfo.binding;
						if (oBinding && !oBinding.getType()) {
							// change binding type from input type
							var oBind = {
								path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() : oBinding.getPath()
							};
							if (poFormatter.getNoLeadingZeroes(oField.NoLeadingZeroes)) {
								oBind.formatter = function(psValue) {
									return poGeneralFormatter.deleteLeadingZeroes(psValue);
								};
							} else if (oField.Decimals > 0) {
								oBind.type = new sap.ui.model.type.Float({
									maxFractionDigits: oField.Decimals
								});
							} else {
								var oBindingType;
								if (oField.InputType) {
									oBindingType = poHelper.getBindingTypeFromInputType(oField.InputType);
								} else if (oField.Type) {
									oBindingType = poFormatter.getBindingType(oField.Type);
								}
								oBind.type = oBindingType;
							}
							this.bindProperty("text", oBind);
						}
					} else {
						// Text
						this.setText(oField.Text);

						// Required
						if (this.setRequired) {
							this.setRequired(poFormatter.getRequired(oField.FieldType));
						}
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

				}
			}
		}

	});
});