/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/Input"
], function(poHelper, poFormatter, poInput) {
	"use strict";
	return poInput.extend("com.tgroup.lib.controls.v1.Input", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				mandatory: {
					type: "boolean",
					defaultValue: false
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				},
				hasSuggestionItems: {
					type: "boolean",
					defaultValue: false
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				},
				key: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			sap.m.InputRenderer.render(oRM, oControl);
		},

		onAfterRendering: function() {
			if (sap.m.Input.prototype.onAfterRendering) {
				sap.m.Input.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				// Checking if we do have a falid name for the control itself
				if (sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField && typeof oField !==
					"undefined") {

					// Time to set the specific properties for the control (depending on the aggregation of course)
					// Important fields:
					// - Binding
					// - Type
					// - MaxLength
					// - ValueHelp
					// - Width
					// - Enabled
					// - Editable
					// - TextAlign
					// - Required
					// - Visible
					// - Tooltip

					// Binding
					var oBindingInfo = this.getBindingInfo("value");
					if (oBindingInfo && typeof oBindingInfo !== "undefined") {
						var oBinding = oBindingInfo.binding;
						if (oBinding && typeof oBinding !== "undefined") {
							// Setting the live-update functionality for an input
							this.setValueLiveUpdate(true);
							this.bindProperty("value", {
								path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() : oBinding.getPath(),
								type: poFormatter.getBindingType(oField.Type)
							});
						}
					}

					// Type
					this.setType(poFormatter.getType(oField.Type));

					// MaxLength
					this.setMaxLength(poFormatter.getMaxLength(oField.OutputLength));

					// has search help. multi input attach value help request
					if (oField.HasShlp === "X") {
						this.setShowValueHelp(true);
						this._addSearchHelp(this, oField);
					}

					// Width
					if (!this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						// For version 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for value live change
						this.attachLiveChange(function(poEvent) {
							var sValue = poEvent.getParameter("value");
							if (sValue === "") {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					this.setTooltip(poFormatter.getTooltip(oField.Tooltip));

					// Checking for suggestion items
					if (oField.ComboBox && typeof oField.ComboBox !== "undefined") {
						this.setHasSuggestionItems(true);
						this.setShowSuggestion(true);

						var sModelPath = oField.ComboBox.ModelName;
						if (sModelPath) {
							var oModel = sap.ui.getCore().getModel(sModelPath);
							if (!oModel) {
								var mSetting = {
									disableHeadRequestForToken: true
								};
								// model not exist, create model with service
								if (oField.ComboBox.Service && oField.ComboBox.Service !== "") {
									oModel = new sap.ui.model.odata.v2.ODataModel(oField.ComboBox.Service, mSetting);
									sap.ui.getCore().setModel(oModel, sModelPath);
								}
							}
						}
						
						this.setModel(oModel, sModelPath);

						if (oBinding && typeof oBinding !== "undefined") {
							this.bindProperty("key", {
								path: this.getBindingModel() + ">" + oBinding.getPath() + "Key",
								type: poFormatter.getBindingType(oField.Type)
							});
						}
						this.bindAggregation("suggestionItems", oField.ComboBox.ModelName + ">/" + oField.ComboBox.EntitySet, new sap.ui.core.Item({
							text: "{" + oField.ComboBox.ModelName + ">" + oField.ComboBox.TextField + "}",
							key: "{" + oField.ComboBox.ModelName + ">" + oField.ComboBox.KeyField + "}"
						}));
						this.setFilterFunction(function(psTerm, oItem) {
							var sText = oItem.getText().replace(/[^a-zA-Z0-9-]/gi, "");
							var sTerm = psTerm.replace(/[^a-zA-Z0-9-]/gi, "");
							return (sText.match(new RegExp(sTerm, "i")));
						});
						this.attachSuggest(function(poEvent) {
							var sTerm = poEvent.getParameter("suggestValue");
							var aFilters = [];
							if (sTerm) {
								aFilters.push(new sap.ui.model.Filter(oField.ComboBox.TextField, sap.ui.model.FilterOperator.Contains, sTerm));
							}
							poEvent.getSource().getBinding("suggestionItems").filter(aFilters);
						});
						this.attachSuggestionItemSelected(function(poEvent) {
							var oSelectedItem = poEvent.getParameter("selectedItem");
							if (oSelectedItem) {
								var sKey = oSelectedItem.getKey();
								if (sKey && typeof sKey !== "undefined") {
									if (sKey !== "") {
										this.setKey(sKey);
									}
								}
							} else {
								this.setKey("");
							}
						}.bind(this));
						this.attachLiveChange(function(poEvent) {
							//If the value is complete empty we do have to delete the setted key
							if (poEvent.getSource().getValue() === "") {
								//The key needs always to be setted
								this.setKey("");
							}
						}.bind(this));
					}
				}
				this._done = true;
			}
		},

		/**
		 * add search help event. value help dialog
		 * @private
		 * @param {object} poDisplayInput input control
		 * @param {object} poField field object
		 */
		_addSearchHelp: function(poDisplayInput, poField) {
			// set custom data with search help infos

			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "appName",
				value: poHelper.getAppName()
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "fieldname",
				value: poField.ElementName
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "title",
				value: poField.Text
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "key",
				value: poField.ElementName
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "descriptionKey",
				value: poField.ElementName
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "filterBarExpanded",
				value: "true"
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "supportMultiselect",
				value: "false"
			}));

			poDisplayInput.attachValueHelpRequest(function() {
				poHelper.onValueHelpRequest(this, "F4");
			});
		}
	});
});