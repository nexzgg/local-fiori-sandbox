/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/DatePicker"
], function(poHelper, poFormatter, poDatePicker) {
	"use strict";
	return poDatePicker.extend("com.tgroup.lib.controls.v1.DatePicker", {
		/**
		 * Variables used for the complete control
		 * */
		_done: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				mandatory: {
					type: "boolean",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.DatePickerRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.DatePicker.prototype.onAfterRendering) {
				sap.m.DatePicker.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Width
					// - Enabled
					// - Editable
					// - TextAlign
					// - Required
					// - Visible
					// - Tooltip

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						//For 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for value live change
						this.attachChange(function(poEvent) {
							var sValue = poEvent.getParameter("value");
							if (sValue === "") {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
				}
				this._done = true;
			}
		}
	});
});