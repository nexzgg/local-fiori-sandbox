/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"sap/ui/layout/form/SimpleForm"
], function(poHelper, poSimpleForm) {
	"use strict";
	return poSimpleForm.extend("com.tgroup.lib.controls.v1.DynamicOutput", {
		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				value: {
					type: "object",
					defaultValue: ""
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		_done: false,

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} oControl The control itself
		 * */
		renderer: function(poRm, oControl) {
			oControl.setEditable(true);
			oControl.setLayout(sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout);
			oControl.setLabelSpanL(3);
			oControl.setLabelSpanM(3);
			oControl.setEmptySpanM(4);
			oControl.setColumnsL(1);
			oControl.setColumnsM(1);
			sap.ui.layout.form.SimpleFormRenderer.render(poRm, oControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (!this._done) {
				if (sap.ui.layout.form.SimpleForm.prototype.onAfterRendering) {
					sap.ui.layout.form.SimpleForm.prototype.onAfterRendering.apply(this, arguments);
				}
				var sLayoutName = this.getLayoutName();
				if (sLayoutName && sLayoutName !== "") {
					var aObjects = poHelper.getDynamicObjectFromLayout(sLayoutName);
					var mSetting = {
						form: this
					};
					if (this.getValue() !== "" && typeof this.getValue() === "object") {
						mSetting.value = this.getValue();
						mSetting.bindingPath = (this.getBinding("value")) ? this.getBinding("value").getPath() : "";
						mSetting.bindingModel = this.getBindingModel();
					} else if (this.getBindingContext() || this.getBindingContext(this.getBindingModel())) {
						mSetting.value = {};
						mSetting.bindingModel = this.getBindingModel();
					}
					poHelper.createDynamicInput(aObjects, sLayoutName, true, false, mSetting);
				}

				this._done = true;
			}
		}
	});
});