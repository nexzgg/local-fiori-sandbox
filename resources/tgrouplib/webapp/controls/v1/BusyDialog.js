/** @module com/tgroup/lib/v1/controls */
sap.ui.define([

], function() {
	"use strict";

	return {
		_oDialog: sap.ui.xmlfragment("com.tgroup.lib.controls.v1.view.fragments.BusyDialog", this),

		/**
		 * Method for open the BusyDialog
		 * @public
		 */
		openBusyDialog: function() {
			if (this._oDialog && !this._oDialog !== undefined) {
				//jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
				this._oDialog.open();
			}
		},

		/**
		 * Method for closing the BusyDialog
		 * @public
		 */
		closeBusyDialog: function() {
			if (this._oDialog && !this._oDialog !== undefined) {
				this._oDialog.close();
			}
		}
	};

});