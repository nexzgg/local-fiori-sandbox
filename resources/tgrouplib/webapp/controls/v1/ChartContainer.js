/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"sap/suite/ui/commons/ChartContainer",
	"com/tgroup/lib/controls/v1/util/helper"
], function(poChartContainer, poHelper) {
	"use strict";
	return poChartContainer.extend("com.tgroup.lib.controls.v1.ChartContainer", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				diagram: {
					type: "string",
					defaultValue: ""
				},
				dimensions: {
					type: "object"
				},
				measures: {
					type: "object"
				},
				entitySet: {
					type: "string",
					defaultValue: ""
				},
				model: {
					type: "string",
					defaultValue: ""
				},
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				filters: {
					type: "object"
				},
				chartProperties: {
					type: "object"
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.suite.ui.commons.ChartContainerRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (!this._done) {
				if (sap.suite.ui.commons.ChartContainer.prototype.onAfterRendering) {
					sap.suite.ui.commons.ChartContainer.prototype.onAfterRendering.apply(this, arguments);
				}
				this._done = true;
				var oDimensions = [];
				oDimensions = this.getDimensions();
				var oMeasures = [];
				oMeasures = this.getMeasures();
				var sEntitySet = this.getEntitySet();
				var sModel = this.getModel();
				var sLayoutName = this.getLayoutName();
				var sDiagram = this.getDiagram();
				var oChartProperties = {};
				oChartProperties = this.getChartProperties();
				var aFilters = [];
				aFilters = this.getFilters();

				// All parameters need to be filled! Otherwise we can't create the diagram
				// First we do have to check if we surely have dimensions and measures
				if (oDimensions && typeof oDimensions !== "undefined") {
					if (oMeasures && typeof oMeasures !== "undefined") {
						if (sEntitySet !== "" && sModel !== "" && sLayoutName !== "" && sDiagram !== "") {
							var oVizFrame = poHelper.createVizFrame(oDimensions, oMeasures, sLayoutName, sDiagram, oChartProperties, sModel, sEntitySet,
								aFilters);
							var oChartContainerContent = new sap.suite.ui.commons.ChartContainerContent();
							oChartContainerContent.setContent(oVizFrame);
							this.addContent(oChartContainerContent);
						}
					}
				}
			}
		}
	});
});