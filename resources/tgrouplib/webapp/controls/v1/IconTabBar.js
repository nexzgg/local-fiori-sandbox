/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/IconTabBar"
], function(poHelper, poFormatter, poIconTabBar) {
	"use strict";
	return poIconTabBar.extend("com.tgroup.lib.controls.v1.IconTabBar", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				tabGroup: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			if (!oControl._done) {
				var sTabGroup = oControl.getTabGroup();
				if (sTabGroup && typeof sTabGroup !== "undefined") {
					var aTabs = sap.ui.getCore().getModel("layout").getProperty("/Tabs/" + sTabGroup);
					if (aTabs && typeof aTabs !== "undefined" && aTabs.length !== 0) {
						//Build up the new tabs and at them
						for (var i = 0; i <= aTabs.length - 1; i++) {
							var oCurrentTab = aTabs[i];
							var oIconTabFilter = new sap.m.IconTabFilter({
								text: oCurrentTab.Text,
								icon: "sap-icon://" + oCurrentTab.Icon,
								key: oCurrentTab.TabName,
								content: [
									new sap.ui.core.mvc.XMLView(oCurrentTab.ViewName, {
										viewName: oCurrentTab.ViewName
									})
								]
							});
							oControl.addItem(oIconTabFilter);
						}
					}
				}
				oControl._done = true;
			}
			sap.m.IconTabBarRenderer.render(oRM, oControl);
		}
	});
});