/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/TextArea"
], function(poHelper, poFormatter, poTextArea) {
	"use strict";
	return poTextArea.extend("com.tgroup.lib.controls.v1.TextArea", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,
		_rows: 2,
		_cols: 20,
		_growing: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				},
				mandatory: {
					type: "boolean",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			sap.m.TextAreaRenderer.render(oRM, oControl);
		},

		onAfterRendering: function() {
			if (sap.m.TextArea.prototype.onAfterRendering) {
				sap.m.TextArea.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				// Checking if we do have a falid name for the control itself
				if (sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField && typeof oField !==
					"undefined") {

					// Time to set the specific properties for the control (depending on the aggregation of course)
					// Important fields:
					// - Binding
					// - MaxLength
					// - Width
					// - Enabled
					// - Editable
					// - TextAlign
					// - Required
					// - Visible
					// - Tooltip

					// Binding
					var oBindingInfo = this.getBindingInfo("value");
					if (oBindingInfo && typeof oBindingInfo !== "undefined") {
						var oBinding = oBindingInfo.binding;
						if (oBinding && typeof oBinding !== "undefined") {
							if (this.setValueLiveUpdate) {
								this.setValueLiveUpdate(true);
							}
							// Setting the live-update functionality for an input
							this.bindProperty("value", {
								path: this.getBindingModel() + ">" + oBinding.getPath(),
								type: poFormatter.getBindingType(oField.Type)
							});
						}
					}

					// MaxLength
					this.setMaxLength(poFormatter.getMaxLength(oField.OutputLength));

					// Width
					if (!this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						// For version 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for value live change
						this.attachLiveChange(function(poEvent) {
							var sValue = poEvent.getParameter("value");
							if (sValue === "") {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
				}
				this._rows = this.getRows();
				this._cols = this.getCols();
				if (this.getGrowing && typeof this.getGrowing === "function") {
					this._growing = this.getGrowing();
				}
				this._done = true;
			}
			if (this.getEnabled()) {
				this.setRows(this._rows);
				this.setCols(this._cols);
				if (this.setGrowing && typeof this.setGrowing === "function") {
					this.setGrowing(this._growing);
				}
			} else {
				this._rows = this.getRows();
				this._cols = this.getCols();
				if (this.setGrowing && typeof this.setGrowing === "function") {
					this._growing = this.getGrowing();
					this.setGrowing(true);
				} else {
					var sValue = this.getValue();
					if (sValue && sValue !== "") {
						var oMatch = sValue.match(/\n/g);
						var iRow = 2;
						if (oMatch) {
							iRow += oMatch.length;
						}
						this.setRows(iRow);
					}
				}
			}
		}
	});
});