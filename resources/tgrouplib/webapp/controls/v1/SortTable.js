/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"sap/ui/table/Table"
], function(poHelper, poUiTable) {
	"use strict";
	return poUiTable.extend("com.tgroup.lib.controls.SortTable", {

		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				sortable: {
					type: "boolean",
					defaultValue: false
				},
				filterable: {
					type: "boolean",
					defaultValue: false
				},
				top: {
					type: "int",
					defaultValue: 0
				},
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				modelName: {
					type: "string",
					defaultValue: ""
				},
				entitySet: {
					type: "string",
					defaultValue: ""
				},
				message: {
					type: "boolean",
					defaultValue: false
				},
				editable: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			sap.ui.table.TableRenderer.render(oRM, oControl);
		},

		/**
		 * on after rendering. 
		 */
		onAfterRendering: function() {
			if (sap.ui.table.Table.prototype.onAfterRendering) {
				sap.ui.table.Table.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var oCore = sap.ui.getCore();

				if (this.getLayoutName() && this.getModelName() && this.getEntitySet() && this.getEntitySet !== "") {
					// binding info
					var oBindingInfo = {
						path: this.getModelName() + ">/" + this.getEntitySet()
					};

					// set top. check ui table or table
					if (this.getTop() > 0) {
						this.setVisibleRowCount(this.getTop());
					}

					// generate the table from layout
					poHelper.getTableFromLayout(this.getId(), this.getLayoutName(), this.getModelName(), oBindingInfo, this.getSortable(), this.getFilterable(),
						this, this.getEditable());

					// add message handling
					if (this.getMessage()) {
						poHelper.addShowMessageToTable(this, oCore.getModel("message"));
					}
				}
				this._done = true;
			}
		}
	});
});