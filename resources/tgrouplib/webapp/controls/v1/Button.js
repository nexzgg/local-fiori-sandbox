/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/Button"
], function(poHelper, poFormatter, poButton) {
	"use strict";
	return poButton.extend("com.tgroup.lib.controls.v1.Button", {
		/**
		 * Attributes used for the whole control
		 * */
		_done: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.ButtonRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.Button.prototype.onAfterRendering) {
				sap.m.Button.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Text
					// - Width
					// - Visible
					// - Tooltip

					// Text
					this.setText(oField.Text);

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
				}
				this._done = true;
			}
		}
	});
});