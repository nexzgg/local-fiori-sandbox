/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"sap/ui/comp/filterbar/FilterBar"
], function(poHelper, poFilterBar) {
	"use strict";
	return poFilterBar.extend("com.tgroup.lib.controls.v1.FilterBar", {
		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} oControl The control itself
		 * */
		renderer: function(poRm, oControl) {
			sap.ui.comp.filterbar.FilterBarRenderer.render(poRm, oControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.ui.comp.filterbar.FilterBar.prototype.onAfterRendering) {
				sap.ui.comp.filterbar.FilterBar.prototype.onAfterRendering.apply(this, arguments);
			}
			var aFilterGroupItems = this.getFilterGroupItems();
			if (!aFilterGroupItems || aFilterGroupItems.length === 0) {
				var sLayoutName = this.getLayoutName();
				if (sLayoutName && sLayoutName !== "") {
					poHelper.setDynamicFilterBar(sLayoutName, this);
				}
			}
		}
	});
});