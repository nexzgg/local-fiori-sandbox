/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/CheckBox"
], function(poHelper, poFormatter, poCheckBox) {
	"use strict";
	return poCheckBox.extend("com.tgroup.lib.controls.v1.CheckBox", {
		/**
		 * Attributes for the whole control
		 * */
		_done: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.CheckBoxRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.CheckBox.prototype.onAfterRendering) {
				sap.m.CheckBox.prototype.onAfterRendering.apply(this, arguments);
			}

			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Enabled
					// - Text
					// - TextAlign
					// - Width
					// - Editable
					// - Visible
					// - Tooltip

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Text
					this.setText(oField.Text);

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
				}
				this._done = true;
			}
		}
	});
});