/** @module com/tgroup/lib/v1/controls */
sap.ui.define([
	"com/tgroup/lib/controls/v1/util/helper",
	"com/tgroup/lib/controls/v1/model/formatter",
	"sap/m/MultiComboBox"
], function(poHelper, poFormatter, poComboBox) {
	"use strict";
	return poComboBox.extend("com.tgroup.lib.controls.v1.MultiComboBox", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				mandatory: {
					type: "boolean",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			sap.m.MultiComboBoxRenderer.render(oRM, oControl);
		},

		onAfterRendering: function() {
			if (sap.m.MultiComboBox.prototype.onAfterRendering) {
				sap.m.MultiComboBox.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				// Checking if we do have a falid name for the control itself
				if (sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField && typeof oField !==
					"undefined") {

					// Width
					if (!this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						// For version 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for selection Change
						this.attachSelectionFinish(function(poEvent) {
							var aItems = poEvent.getParameter("selectedItems");
							if (!aItems || (aItems && aItems.length === 0)) {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));
				
					// Tooltip
					this.setTooltip(poFormatter.getTooltip(oField.Tooltip));

					// has domain values more important, as the combobox customizing
					if (oField.HasDom && oField.Domname) {
						var oDataprovider = sap.ui.getCore().getModel("dataprovider");
						if (oDataprovider) {
							// get the filter to domname
							var oFilter = new sap.ui.model.Filter("Domname", sap.ui.model.FilterOperator.EQ, oField.Domname);
							this.setModel(oDataprovider, "dataprovider");
							this.bindItems({
								path: "dataprovider>/DomainSet",
								filters: [oFilter],
								sorter: {
									path: "dataprovider>Text"
								},
								template: new sap.ui.core.Item({
									text: "{dataprovider>Text}",
									key: "{dataprovider>Value}"
								})
							});
						}
					} else if (oField.ComboBox) {
						// fill items from combobox customizing
						if (!this.getBindingInfo("items")) {
							var sModelPath = oField.ComboBox.ModelName;
							if (sModelPath) {
								var oModel = sap.ui.getCore().getModel(sModelPath);
								if (!oModel) {
									var mSetting = {
										disableHeadRequestForToken: true
									};
									// model not exist, create model with service
									if (oField.ComboBox.Service && oField.ComboBox.Service !== "") {
										oModel = new sap.ui.model.odata.v2.ODataModel(oField.ComboBox.Service, mSetting);
										sap.ui.getCore().setModel(oModel, sModelPath);
									}
								}

								// get the filters
								var aFilter = [];
								if (oField.Filters) {
									aFilter = poHelper.convertToModelFilter(oField.Filters);
								}

								this.setModel(oModel, sModelPath);
								this.bindItems({
									path: sModelPath + ">/" + oField.ComboBox.EntitySet,
									filters: aFilter,
									sorter: {
										path: sModelPath + ">" + oField.ComboBox.TextField
									},
									template: new sap.ui.core.Item({
										text: "{" + sModelPath + ">" + oField.ComboBox.TextField + "}",
										key: "{" + sModelPath + ">" + oField.ComboBox.KeyField + "}"
									})
								});
							}
						}
					}
				}
				this._done = true;
			}
		}

	});
});