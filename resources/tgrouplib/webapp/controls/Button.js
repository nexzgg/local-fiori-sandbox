/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"sap/m/Button",
	"sap/ui/commons/RichTooltip"
], function(poHelper, poFormatter, poButton) {
	"use strict";
	return poButton.extend("com.tgroup.lib.controls.Button", {
		/**
		 * Attributes used for the whole control
		 * */
		_done: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				},
				count: {
					type: "string",
					defaultValue: null
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(poRm, poControl) {
			sap.m.ButtonRenderer.render(poRm, poControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.m.Button.prototype.onAfterRendering) {
				sap.m.Button.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);
				this._oField = oField;

				if (oField && typeof oField !== "undefined") {
					// If we do have a field let's set the attributes for the label
					// Important fields:
					// - Text
					// - Width
					// - Visible
					// - Tooltip

					// Text
					this.setText(oField.Text);

					// Width
					if (oField.Width !== 0 && !this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					//this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
					if (oField.Tooltip && typeof oField.Tooltip !== "undefined" && oField.Tooltip !== "") {
						this.setTooltip(new sap.ui.commons.RichTooltip({
							text: poFormatter.getTooltip(oField.Tooltip),
							collision: "fit"
						}));
					}
				}
				this._done = true;
			}

			if (this.getCount()) {
				this.setText(this._oField.Text + " (" + this.getCount() + ")");
			}
		}
	});
});