/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"sap/ui/layout/form/SimpleForm"
], function (poHelper, poSimpleForm) {
	"use strict";
	return poSimpleForm.extend("com.tgroup.lib.controls.DynamicOutput", {
		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				value: {
					type: "object",
					defaultValue: ""
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				},
				enabled: {
					type: "boolean",
					defaultValue: true
				},
				layout: {
					type: "string",
					defaultValue: "ResponsiveGridLayout"
				},
				context: {
					type: "object"
				}
			}
		},

		_done: false,
		_sCurrentLayout: undefined,
		_oCurrentValue: undefined,

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} oControl The control itself
		 * */
		renderer: function (poRm, oControl) {
			oControl.setEditable(true);
			oControl.setLabelSpanL(3);
			oControl.setLabelSpanM(3);
			oControl.setEmptySpanM(4);
			oControl.setColumnsL(1);
			oControl.setColumnsM(1);
			sap.ui.layout.form.SimpleFormRenderer.render(poRm, oControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function () {
			if (sap.ui.layout.form.SimpleForm.prototype.onAfterRendering) {
				sap.ui.layout.form.SimpleForm.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this.getContent() || this.getContent().length === 0) {
				this.renderForm();
			} else if (!this._done) {
				this.done = true;
				// set the defaults
				for (var j = 0; j < this.getContent().length; j++) {
					var oContent = this.getContent()[j];
					if (oContent.getMetadata().getName() === "com.tgroup.lib.controls.ComboBox") {
						var oComboField = poHelper.getField(oContent.getLayoutName(), oContent.getField());
						if (oComboField && oComboField.ComboBox && oComboField.ComboBox.DefaultValue) {
							oContent.setSelectedKey(oComboField.ComboBox.DefaultValue);
							oContent.fireChange();
						}
					}
				}
			}
		},

		/**
		 * Functionality for setting the layout for the table
		 * @public
		 * @param {string} psLayoutName The name of the layout
		 * */
		setLayoutName: function (psLayoutName) {
			if (psLayoutName && typeof psLayoutName !== "undefined") {
				this.setProperty("layoutName", psLayoutName, true);
				if (this._sCurrentLayout && this._sCurrentLayout !== psLayoutName) {
					this.destroyContent();
					this.renderForm();
				}
				this._sCurrentLayout = psLayoutName;
			}
		},

		/**
		 * Functionality for setting the context
		 * @public
		 * @param {object} poContext context
		 * */
		setContext: function (poContext) {
			if (poContext) {
				this.setProperty("context", poContext, true);
				this.destroyContent();
				this.renderForm();
			}
		},

		/**
		 * Functionality for setting the Editable for the form
		 * @public
		 * @override
		 * @param {boolean} pbEnabled enabled flag
		 * */
		setEnabled: function (pbEnabled) {
			this.setProperty("enabled", pbEnabled, true);
			if (this.getContent() && this.getContent().length > 0) {
				var aContent = this.getContent();
				for (var i = 0; i < aContent.length; i++) {
					var oItem = aContent[i];
					var oContent = poHelper.getControlFromBox(oItem, 0);
					if (oContent.setEnabled) {
						oContent.setEnabled(pbEnabled);
					}
				}
			}
		},

		/**
		 * Functionality for setting the value for the form
		 * @public
		 * @override
		 * @param {boolean} poValue enabled flag
		 * */
		setValue: function (poValue) {
			if (poValue) {
				this.setProperty("value", poValue, true);
				if (!this._oCurrentValue) {
					this.destroyContent();
					this.renderForm();
				}
				this._oCurrentValue = poValue;
			}
		},

		renderForm: function () {
			var sLayoutName = this.getLayoutName();

			if (sLayoutName && sLayoutName !== "") {
				var aObjects = poHelper.getDynamicObjectFromLayout(sLayoutName);
				var mSetting = {
					form: this,
					output: true
				};
				if (this.getValue() !== "" && typeof this.getValue() === "object") {
					mSetting.value = this.getValue();
					mSetting.bindingPath = (this.getBinding("value")) ? this.getBinding("value").getPath() : "";
					mSetting.bindingModel = this.getBindingModel();
				} else if (this.getBindingContext() || this.getBindingContext(this.getBindingModel())) {
					mSetting.value = {};
					mSetting.bindingModel = this.getBindingModel();
				}
				poHelper.createDynamicInput(aObjects, sLayoutName, true, false, mSetting, false, this.getContext());
			}
		},

		/**
		 * get control from content by fieldname
		 * @public
		 * @param {string} psField field name
		 * @return {object} oControl control
		 */
		getControl: function (psField) {
			return poHelper.getControlFromSimpleForm(this.getContent(), psField);
		}
	});
});