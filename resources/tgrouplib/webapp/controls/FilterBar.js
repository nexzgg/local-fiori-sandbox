/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"sap/ui/comp/filterbar/FilterBar"
], function(poHelper, poFilterBar) {
	"use strict";
	return poFilterBar.extend("com.tgroup.lib.controls.FilterBar", {
		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				showClearOnFB: {
					type: "boolean",
					defaultValue: true
				},
				showRestoreButton: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} oControl The control itself
		 * */
		renderer: function(poRm, oControl) {
			sap.ui.comp.filterbar.FilterBarRenderer.render(poRm, oControl);
		},

		/**
		 * Functionality to be executed after the control has been rendered
		 * @public
		 * */
		onAfterRendering: function() {
			if (sap.ui.comp.filterbar.FilterBar.prototype.onAfterRendering) {
				sap.ui.comp.filterbar.FilterBar.prototype.onAfterRendering.apply(this, arguments);
			}
			var aFilterGroupItems = this.getFilterGroupItems();
			if (!aFilterGroupItems || aFilterGroupItems.length === 0) {
				var sLayoutName = this.getLayoutName();
				if (sLayoutName && sLayoutName !== "") {
					poHelper.setDynamicFilterBar(sLayoutName, this);
				}
				// attach clear on filterbar 
				this.attachClear(function(poEvent) {
					var aSelectionSet = poEvent.getParameter("selectionSet");
					poHelper.clearFilter(aSelectionSet);
				});
			}
		},

		/**
		 * get control from selectionset by fieldname
		 * @public
		 * @param {string} psField field name
		 * @return {object} oControl control
		 */
		getControl: function(psField) {
			var oControl;
			if (psField) {
				var aSelectionSet = poHelper.getFilterBarSelectionSet(this);
				if (aSelectionSet) {
					for (var i = 0; i < aSelectionSet.length; i++) {
						var oSelectionSet = aSelectionSet[i];
						if (oSelectionSet.getField && typeof oSelectionSet.getField === "function" || oSelectionSet.data("field")) {
							if (oSelectionSet.data("field") === psField || (oSelectionSet.getField && oSelectionSet.getField() === psField)) {
								oControl = oSelectionSet;
								break;
							}
						}
					}
				}
			}
			return oControl;
		}
	});
});