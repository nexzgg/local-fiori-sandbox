/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/message/util/messageHandler",
	"com/tgroup/lib/controls/model/models",
	"com/tgroup/lib/controls/model/formatter",
	"com/tgroup/lib/controls/BusyDialog",
	"sap/ui/core/CustomData",
	"com/tgroup/lib/general/util/helper",
	"com/tgroup/lib/general/model/formatter",
	"com/tgroup/lib/controls/lib/jsonpath/jsonpath",
	"com/tgroup/lib/login/util/loginHandler"
], function (poMessageHandler, poModels, poFormatter, poBusyDialog, poCustomData, poGeneralHelper, poGeneralFormatter, jsonPath,
	LoginHandler) {
	"use strict";

	return {

		oFormatter: poFormatter,

		inputType: {
			multiInput: "MULTIINPUT",
			checkBox: "CHECKBOX",
			comboBox: "COMBOBOX",
			dateTime: "DATETIME",
			textArea: "TEXTAREA",
			time: "TIME",
			multiComboBox: "MULTICOMBO",
			label: "LABEL",
			icon: "ICON",
			rating: "RATING",
			password: "PASSWORD",
			html: "HTML"
		},

		type: {
			date: "Edm.DateTime"
		},

		/**
		 * get Bindingtype from input type.
		 * @param {string} psInputType the input type
		 * @return {object} oBindingType The type for the control
		 */
		getBindingTypeFromInputType: function (psInputType) {
			var oBindingType;
			switch (psInputType) {
			case this.inputType.checkBox:
				oBindingType = new sap.ui.model.odata.type.Boolean();
				break;
			case this.inputType.dateTime:
				oBindingType = new sap.ui.model.type.Date();
				break;
			case this.inputType.time:
				oBindingType = new sap.ui.model.odata.type.Time();
				break;
			default:
				oBindingType = new sap.ui.model.odata.type.String();
			}
			return oBindingType;
		},

		/**
		 * Functionality for getting tabs to specific application and group of tabs
		 * @public
		 * @param {string} psApplication The name of the application
		 * @param {string} psTabGroup The name of the tag group
		 * @param {function} pfnCallBack The callback function
		 * */
		getTabs: function (psApplication, psTabGroup, pfnCallBack) {
			if (psApplication && typeof psApplication !== "undefined" && psTabGroup && typeof psTabGroup !== "undefined") {
				if (psApplication !== "" && psTabGroup !== "") {
					// success function
					var fnSuccess = function (oData) {
						poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
						pfnCallBack(this._convertTabs(oData));
					}.bind(this);

					// error function
					var fnError = function () {
						poMessageHandler.showMessage(this.getView().getModel("message"));
					}.bind(this);

					poModels.readTabs(psApplication, psTabGroup, fnSuccess, fnError);
				}
			}
		},

		/**
		 * Functionality for converting the tabs into a right format
		 * @param {object} poData The data object containing our tabs
		 * @return {array} aIconTabFilter The array containig the icon tab filters
		 * */
		_convertTabs: function (poData) {
			var aIconTabFilter = [];
			if (poData.results && typeof poData.results !== "undefined") {
				if (poData.results.length !== 0) {
					for (var i = 0; i <= poData.results.length - 1; i++) {
						var oCurrentTab = poData.results[i];
						var oIconTabFilter = new sap.m.IconTabFilter({
							text: oCurrentTab.Text,
							icon: "sap-icon://" + oCurrentTab.Icon,
							key: oCurrentTab.TabName
						});
						//Checking if a view is provided
						if (oCurrentTab.ViewName && typeof oCurrentTab.ViewName !== "undefined" && oCurrentTab.ViewName !== "") {
							oIconTabFilter.addContent(new sap.ui.core.mvc.XMLView({
								viewName: oCurrentTab.ViewName
							}));
						}
						aIconTabFilter.push(oIconTabFilter);
					}
				}
			}
			return (aIconTabFilter);
		},

		/**
		 * Functionality used for getting the right field corresponding to a control layout name
		 * @public
		 * @param {string} psLayout The name of the layout
		 * @param {string} psField The name of the field
		 * @return {object} oField The found field with all settings
		 * */
		getField: function (psLayout, psField) {
			var oLayoutModel = sap.ui.getCore().getModel("layout");
			var oLayoutData = oLayoutModel.getData();
			var oField;

			if (oLayoutData && typeof oLayoutData !== "undefined") {
				if (psLayout && typeof psLayout !== "undefined") {
					if (psField && typeof psField !== "undefined") {
						// Let's check our layout data for the specific field and setting the corresponding attributes
						var sLayoutName = psLayout;
						var sFieldName = psField;
						// Looping over all layouts in the object for finding the right one
						for (var sObject in oLayoutData) {
							if (oLayoutData.hasOwnProperty(sObject)) {
								if (sObject === sLayoutName) {
									// If we do have the right layout let's check for the right field
									var oObject = oLayoutData[sObject].Fields;
									for (var sField in oObject) {
										if (oObject.hasOwnProperty(sField)) {
											if (sField === sFieldName) {
												oField = oObject[sField];
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return (oField);
		},

		/**
		 * get dynamic objects from layout.
		 * array is for example creating dynamic input or output
		 * @public
		 * @param {string} psLayout The layout containing the corresponding fields to the entity set
		 **/
		getDynamicObjectFromLayout: function (psLayout) {
			var aObjects = [];
			var oLayouts = sap.ui.getCore().getModel("layout");
			// First find the corresponding layout object
			var oLayoutObject = oLayouts.getProperty("/" + psLayout);

			// Start with building up the dynamic objects
			// Checking if we do have a layout object for finding the right label
			if (oLayoutObject && oLayoutObject !== undefined) {
				for (var sField in oLayoutObject.Fields) {
					if (oLayoutObject.Fields.hasOwnProperty(sField)) {
						var oField = oLayoutObject.Fields[sField];
						aObjects.push(oField);
					}
				}
			}

			return (aObjects);
		},

		/**
		 * method for building up the dynamic input structure
		 * @public
		 * @param {array} paObjects The objects which represent the inputs
		 * @param {string} psLayout the name of the layout for the cat inputs
		 * @param {boolean} pbEditable input fields are editable or not
		 * @param {boolean} pbEnabled input fields are enabled or not
		 * @param {map} poSetting? settings
		 * @return {control} oControl the control which holds the input structure
		 **/
		createDynamicInput: function (paObjects, psLayout, pbEditable, pbEnabled, pmSetting, pbEditMode, oContext) {
			var oForm;
			var aControls = [];

			if (paObjects.length !== 0) {
				// if form undefined, create simpleForm
				if (pmSetting && pmSetting.form) {
					oForm = pmSetting.form;
				} else {
					oForm = new sap.ui.layout.form.SimpleForm({
						maxContainerCols: 2,
						editable: true,
						layout: "ResponsiveGridLayout",
						labelSpanL: 3,
						labelSpanM: 3,
						emptySpanM: 4,
						columnsL: 1,
						columnsM: 1
					});
				}

				// check value object in the setting
				var bBindValue = pmSetting && pmSetting.value && typeof pmSetting.value === "object";
				for (var i = 0; i <= paObjects.length - 1; i++) {
					var oObject = paObjects[i];
					var bExist = true;
					var sValue = "";
					var oInput;

					// value object in set setting get value path
					if (bBindValue) {
						sValue = "";
						//if (pmSetting.value[oObject.ElementName] || pmSetting.value[oObject.ElementName] === "") {
						if (pmSetting.bindingModel && pmSetting.bindingModel !== "") {
							sValue = pmSetting.bindingModel + ">";
						}
						if (pmSetting.bindingPath && pmSetting.bindingPath !== "" && pmSetting.bindingPath !== "/") {
							sValue += pmSetting.bindingPath + "/";
						} else if (pmSetting.bindingPath === "/") {
							sValue += "/";
						}
						sValue += oObject.ElementName;
						//} else {
						//	bExist = false;
						//}
					}

					if (bExist) {
						var oLabel = new com.tgroup.lib.controls.Label({
							layoutName: psLayout,
							field: oObject.ElementName
						});
						// settings for get the custom control
						var mSetting = {
							bindingModel: (pmSetting && pmSetting.bindingModel) ? pmSetting.bindingModel : "",
							enabled: pbEnabled,
							editable: pbEditable,
							value: sValue
						};
						if (pmSetting) {
							mSetting.output = pmSetting.output;
						}

						if (pbEditMode) {
							mSetting.enabled = "{" + mSetting.bindingModel + ">/" + oObject.ElementName + "Switch}";
						}

						oInput = this.getCustomControl(psLayout, oObject, mSetting, oContext);
						if (pbEditMode) {
							this.setInitialValueToModel(oInput, oObject, pmSetting.bindingModel, oObject.ElementName);
						}

						//Storing all the elements in an array
						aControls.push(oInput);
						oForm.addContent(oLabel);

						if (pbEditMode) {
							var oFlexBox = new sap.m.FlexBox({
								alignItems: "Start",
								justifyContent: "SpaceBetween"
							});

							var oSwitch = new sap.m.Switch({
								state: "{" + pmSetting.bindingModel + ">/" + oObject.ElementName + "Switch}"
							});
							sap.ui.getCore().getModel(pmSetting.bindingModel).setProperty("/" + oObject.ElementName + "Switch", true);

							oFlexBox.addItem(oInput);

							oFlexBox.addItem(oSwitch);

							oForm.addContent(oFlexBox);
						} else {
							oForm.addContent(oInput);
						}
					}
				}
			}

			//When we do have all controls let's look for all dependencies which we have
			this._attachDependencies(aControls, paObjects);
			return (oForm);
		},

		setInitialValueToModel: function (oInput, oObject, sModel, sPath) {
			switch (oObject.InputType) {
			case this.inputType.checkBox:
				sap.ui.getCore().getModel(sModel).setProperty("/" + sPath, false);
				break;
			default:
				sap.ui.getCore().getModel(sModel).setProperty("/" + sPath, "");
			}
		},

		/**
		 * set dynamic filter bar from layout.
		 * @public
		 * @param {string} layout name
		 * @param {control} filter bar control
		 */
		setDynamicFilterBar: function (psLayout, poFilterBar) {
			var oLayout = sap.ui.getCore().getModel("layout");
			// First find the corresponding layout object
			var oLayoutObject = oLayout.getProperty("/" + psLayout);
			var aControls = [];
			var aObjects = [];

			if (oLayoutObject && oLayoutObject.Fields && poFilterBar) {
				// create filter items from layout
				var oFilterItem;
				for (var sField in oLayoutObject.Fields) {
					var oField = oLayoutObject.Fields[sField];
					var sLabel = oField.Text;
					var oControl = this.getCustomControl(psLayout, oField);
					if (oControl.setNotWidth && typeof oControl.setNotWidth === "function") {
						oControl.setNotWidth(true);
					}
					oFilterItem = new sap.ui.comp.filterbar.FilterItem({
						label: sLabel,
						name: oField.ElementName,
						control: oControl,
						mandatory: poFormatter.getRequired(oField.FieldType)
					});
					poFilterBar.addFilterItem(oFilterItem);
					aControls.push(oControl);
					aObjects.push(oField);
				}
			}

			this._attachDependencies(aControls, aObjects);

			// add on sap enter
			this.addFilterBarOnEnter(poFilterBar);
		},

		/**
		 * on filter bar enter.
		 * @public
		 * @param {object} poFilterBar filter bar
		 */
		addFilterBarOnEnter: function (poFilterBar) {
			if (poFilterBar) {
				var aSelectionSet = this.getFilterBarSelectionSet(poFilterBar);
				if (aSelectionSet) {
					for (var i = 0; i < aSelectionSet.length; i++) {
						var oSelectionSet = aSelectionSet[i];
						if (oSelectionSet.onsapenter) {
							oSelectionSet.onsapenter = function () {
								this.fireSearch({
									selectionSet: aSelectionSet
								});
							}.bind(poFilterBar)
						}
					}
				}
			}
		},

		/** 
		 * attach dependencies to comboboxes.
		 * @private 
		 * @param paControls {array} list with controls
		 * @param paObjects {array} list with field object
		 */
		_attachDependencies: function (paControls, paObjects) {
			var oObject, oDepentendObject, oFilter;

			if (paControls.length !== 0) {
				var aControls = paControls;
				var aDepentendObjects = [];
				for (var i = 0; i <= paObjects.length - 1; i++) {
					oObject = paObjects[i];
					if (this._isInputTypeComboBox(oObject.InputType) || this._isInputTypeMultiComboBox(oObject.InputType)) {
						if (oObject.ComboBox && oObject.ComboBox.Dependency && oObject.ComboBox.Dependency !== "undefined") {
							//We have to find the field the field with dependency
							var oFrom;
							for (var j = 0; j <= aControls.length - 1; j++) {
								if (aControls[j].getField() === oObject.ComboBox.Dependency) {
									oFrom = aControls[j];
									break;
								}
							}
							//Finding the field which is dependent from
							var oTo;
							for (j = 0; j <= aControls.length - 1; j++) {
								if (aControls[j].getField() === oObject.ElementName) {
									oTo = {
										control: aControls[j],
										filters: this.convertToModelFilter(oObject.Filters)
									};
									break;
								}
							}
							if (aDepentendObjects.length !== 0) {
								var bFound = false;
								var oDepentendObject;
								for (j = 0; j <= aDepentendObjects.length - 1; j++) {
									oDepentendObject = aDepentendObjects[j];
									if (oDepentendObject.from && oDepentendObject.from.getField() === oObject.ComboBox.Dependency) {
										bFound = true;
										break;
									}
								}
								if (bFound === false) {
									aDepentendObjects.push({
										from: oFrom,
										to: [
											oTo
										]
									});
								} else {
									oDepentendObject.to.push(oTo);
								}
							} else {
								aDepentendObjects.push({
									from: oFrom,
									to: [
										oTo
									]
								});
							}
						}
					}
				}
				if (aDepentendObjects.length !== 0) {
					for (i = 0; i <= aDepentendObjects.length - 1; i++) {
						oDepentendObject = aDepentendObjects[i];
						for (j = 0; j <= aControls.length - 1; j++) {
							var oControl = aControls[j];
							if (oDepentendObject.from && oControl.getField && oControl.getField() === oDepentendObject.from.getField() && oControl.attachSelectionChange) {
								var that = this;
								oControl.attachSelectionChange(function () {
									var aFilters = [];
									if (this.from.getMetadata().getName() === "com.tgroup.lib.controls.ComboBox" && this.from.getSelectedKey() && this.from.getSelectedKey() !==
										"") {
										oFilter = new sap.ui.model.Filter(
											this.from.getField(),
											sap.ui.model.FilterOperator.EQ,
											this.from.getSelectedKey()
										);
										aFilters.push(oFilter);
									} else if (this.from.getMetadata().getName() === "com.tgroup.lib.controls.MultiComboBox" && this.from.getSelectedKeys()) {
										for (var k = 0; k < this.from.getSelectedKeys().length; k++) {
											var sKey = this.from.getSelectedKeys()[k];
											oFilter = new sap.ui.model.Filter(
												this.from.getField(),
												sap.ui.model.FilterOperator.EQ,
												sKey
											);
											aFilters.push(oFilter);
										}
									}
									for (var k = 0; k <= this.to.length - 1; k++) {
										var oCurrentTo = this.to[k];
										var oBinding = oCurrentTo.control.getBinding("items");
										//Also checking the existing filters
										/*for (var l = 0; l <= oCurrentTo.filters.length - 1; l++) {
											aFilters.push(oCurrentTo.filters[l]);
										}*/
										if (oBinding && typeof oBinding !== "undefined") {
											oBinding.filter(aFilters);
										}
										if (oCurrentTo.control.getMetadata().getName() === "com.tgroup.lib.controls.ComboBox") {
											oCurrentTo.control.setSelectedKey("");
											oCurrentTo.control.fireChange();
											if (oCurrentTo.control.fireSelectionChange) {
												oCurrentTo.control.fireSelectionChange();
											}
										} else if (oCurrentTo.control.getMetadata().getName() === "com.tgroup.lib.controls.MultiComboBox") {
											oCurrentTo.control.fireSelectionFinish({
												selectedItems: []
											});
										}
									}
								}.bind(oDepentendObject));
							}
						}
					}
				}
			}
		},

		/**
		 * get the app name from layout model.
		 * @public
		 * @return {string} app name
		 */
		getAppName: function () {
			var oLayoutModel = sap.ui.getCore().getModel("layout");
			var sAppName = oLayoutModel.getProperty("/AppName");
			return ((sAppName) ? sAppName : "");
		},

		/**
		 * show value help request. 
		 * @param {MultiInput} poInput input control
		 * @param {psModelName} psModelName name of the F4 search help model
		 */
		onValueHelpRequest: function (poInput, psModelName) {

			if (poInput) {
				// default F4Layout model
				var sLayoutModel = (poInput.data("layoutModel")) ? poInput.data("layoutModel") : "F4Layout";

				var mSetting = {
					layoutModel: sap.ui.getCore().getModel(sLayoutModel),
					title: (poInput.data("title")) ? poInput.data("title") : "",
					key: (poInput.data("key")) ? poInput.data("key") : "key",
					descriptionKey: (poInput.data("descriptionKey")) ? poInput.data("descriptionKey") : "descriptionKey",
					supportMultiselect: (poInput.data("supportMultiselect") && poInput.data("supportMultiselect") === "true") ? true : false,
					supportRanges: (poInput.data("supportRanges") && poInput.data("supportRanges") === "true") ? true : false,
					supportRangesOnly: (poInput.data("supportRangesOnly") && poInput.data("supportRangesOnly") === "true") ? true : false,
					filterMode: (poInput.data("filterMode") && poInput.data("filterMode") === "true") ? true : false,
					filterBarExpanded: (poInput.data("filterBarExpanded") && poInput.data("filterBarExpanded") === "true") ? true : false
				};

				var sAppName = (poInput.data("appName")) ? poInput.data("appName") : this.getAppName();
				var sFieldname = poInput.data("fieldname");

				if (sFieldname && sFieldname !== "") {
					// load F4 search help
					this.getF4SearchHelp(sAppName, sFieldname, function () {
						var oValueHelpDialog = this.getValueHelpDialog(poInput, psModelName, mSetting);

						if (oValueHelpDialog) {
							oValueHelpDialog.open();
							oValueHelpDialog.update();
						}
					}.bind(this));
				}
			}
		},

		/**
		 * show ranges value help request for multi input control. 
		 * @public
		 * @param {MultiInput} poMultiInput multi input control
		 */
		onRangeValueHelpRequest: function (poMultiInput) {
			// create the ranges value help dialog and open it
			var mSetting = {
				title: (poMultiInput.data("title")) ? poMultiInput.data("title") : "",
				key: (poMultiInput.data("key")) ? poMultiInput.data("key") : "key",
				descriptionKey: (poMultiInput.data("descriptionKey")) ? poMultiInput.data("descriptionKey") : "descriptionKey",
				value: (poMultiInput.data("value")) ? poMultiInput.data("value") : "",
				supportRanges: true,
				supportMultiselect: true,
				supportRangesOnly: true
			};
			var oRangeHelpDialog = this.getRangesValueHelpDialog(poMultiInput, mSetting);

			// set the ranges fields
			oRangeHelpDialog.setRangeKeyFields([{
				key: mSetting.key,
				label: mSetting.title
			}]);

			// init with already existing tokens
			oRangeHelpDialog.setTokens(poMultiInput.getTokens());

			if (oRangeHelpDialog) {
				oRangeHelpDialog.open();
				// oRangeHelpDialog.update();
			}
		},

		/**
		 * get the f4 searchhelp fields and attributes.
		 * @public
		 * @param {String} psAppName app name
		 * @param {String} psFieldname field name
		 * @param {function} pfnCallBack function
		 * 
		 */
		getF4SearchHelp: function (psAppName, psFieldname, pfnCallBack) {
			// get layout model
			var oF4Layout = sap.ui.getCore().getModel("F4Layout");

			// check F4 search help already loaded for psAppName and psFieldname
			var bExist = this.isF4SearchHelpExist(psAppName, psFieldname, oF4Layout);
			if (bExist) {
				if (pfnCallBack() && typeof pfnCallBack === "function") {
					pfnCallBack();
				}
			} else {
				// success function
				var fnSuccess = function (oData, oResponse) {
					this.convertF4SearchHelp(oData, oF4Layout);
					poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
					if (pfnCallBack() && typeof pfnCallBack === "function") {
						pfnCallBack();
					}
				}.bind(this);

				// error function
				var fnError = function (oData, oResponse) {
					poMessageHandler.showMessage(this.getView().getModel("message"));
				}.bind(this);

				poModels.readF4SearchHelp(psAppName, psFieldname, fnSuccess, fnError);
			}
		},

		/** 
		 * set filter in filterbar
		 * @param {object} poFilterBar filter bar control
		 * @param {object} poFilter filter
		 */
		setFilter: function (poFilterBar, poFilter) {
			if (poFilter && poFilterBar) {
				var sField = poFilter.sPath;
				var oControl = poFilterBar.getControl(sField);
				if (oControl) {
					var sControl = oControl.getMetadata().getName();
					//Found!
					switch (sControl) {
					case "sap.m.Input":
					case "com.tgroup.lib.controls.Input":
					case "sap.m.RatingIndicator":
						if (oControl.setValue) {
							oControl.setValue(poFilter.oValue1);
						}
						break;
					case "sap.m.DatePicker":
					case "com.tgroup.lib.controls.DatePicker":
						if (oControl.setDateValue) {
							oControl.setDateValue(poGeneralFormatter.formatDateString(poFilter.oValue1));
						}
						break;
					case "sap.m.TimePicker":
					case "com.tgroup.lib.controls.TimePicker":
						if (oControl.setDateValue) {
							oControl.setDateValue(poGeneralFormatter.formatTimeString(poFilter.oValue1));
						}
						break;
					case "sap.m.DateTimeInput":
					case "com.tgroup.lib.controls.TimePickerOld":
						if (oControl.setDateValue) {
							oControl.setDateValue(poGeneralFormatter.formatTimeString(poFilter.oValue1));
						}
						break;
					case "sap.m.CheckBox":
					case "com.tgroup.lib.controls.CheckBox":
						if (oControl.setSelected) {
							if (poFilter.oValue1) {
								oControl.setSelected(true);
							} else {
								oControl.setSelected(false);
							}
						}
						break;
					case "sap.m.ComboBox":
					case "com.tgroup.lib.controls.ComboBox":
						if (oControl.setSelectedKey) {
							oControl.setSelectedKey(poFilter.oValue1);
						}
						break;
					case "sap.m.MultiComboBox":
					case "com.tgroup.lib.controls.MultiComboBox":
						if (oControl.addSelectedKeys) {
							oControl.addSelectedKeys(poFilter.oValue1);
						}
						break;
					}
				}
			}
		},

		/** 
		 * set filters in filterbar
		 * @param {object} poFilterBar filter bar
		 * @param {array} paFilter list with filter to set
		 * @param {boolean} [pbClear] clear filter bar before
		 */
		setFilterList: function (poFilterBar, paFilter, pbClear) {
			if (poFilterBar && paFilter && paFilter.length > 0) {
				if (pbClear) {
					this.clearFilter(this.getFilterBarSelectionSet(poFilterBar));
				}
				var aFilter = paFilter;
				for (var i = 0; i < aFilter.length; i++) {
					var oFilter = aFilter[i];
					this.setFilter(poFilterBar, oFilter);
				}
			}
		},

		/**
		 * get the filters from the filter bar. 
		 * @public
		 * @param {array} paSelectionSet array of controls
		 * @return {array} aFilter array of filters
		 */
		getFilterList: function (paSelectionSet) {
			var aFilter = [];

			if (paSelectionSet) {
				// set the filters
				var oFilter;
				var sFilterPath;

				for (var j = 0; j < paSelectionSet.length; j++) {
					var oSelectionSet = paSelectionSet[j];
					var sControl = oSelectionSet.getMetadata().getName();
					sFilterPath = this.getNameOfControl(oSelectionSet);

					// input controls with different filters from placeholder
					switch (sControl) {
					case "sap.m.Input":
					case "com.tgroup.lib.controls.Input":
						// Here we do have to check whether we have suggestion items
						// Here we do have to check whether we have suggestion items
						if (oSelectionSet.getHasSuggestionItems() && oSelectionSet.getKey() !== "") {
							// We do have some so let's get the selected key!
							oFilter = this.getFilterFromValue(oSelectionSet.getKey(), sFilterPath + "Key");
							oFilter.textValue = oSelectionSet.getValue();
							aFilter.push(oFilter);
						} else {
							if (oSelectionSet.getValue() !== "") {
								oFilter = this.getFilterFromValue(oSelectionSet.getValue(), sFilterPath);
								aFilter.push(oFilter);
							}
						}
						break;
					case "sap.m.DatePicker":
					case "com.tgroup.lib.controls.DatePicker":
						// date control
						if (oSelectionSet.getDateValue() && oSelectionSet.getDateValue() !== "") {
							oFilter = new sap.ui.model.Filter(
								sFilterPath,
								sap.ui.model.FilterOperator.EQ,
								poGeneralFormatter.formatDateForFilter(oSelectionSet.getDateValue())
							);
							aFilter.push(oFilter);
						}
						break;
					case "sap.m.TimePicker":
					case "com.tgroup.lib.controls.TimePicker":
						// time control
						if (oSelectionSet.getValue() && oSelectionSet.getValue() !== "") {
							oFilter = new sap.ui.model.Filter(
								sFilterPath,
								sap.ui.model.FilterOperator.EQ,
								new sap.ui.model.odata.type.Time().parseValue(oSelectionSet.getValue(), "string")
							);
							aFilter.push(oFilter);
						}
						break;
					case "sap.m.DateTimeInput":
					case "com.tgroup.lib.controls.TimePickerOld":
						// time control
						if (oSelectionSet.getDateValue() && oSelectionSet.getDateValue() !== "") {
							var oDate = oSelectionSet.getDateValue();
							var sDate = "PT" + oDate.getHours() + "H" + oDate.getMinutes() + "M00S";
							oFilter = new sap.ui.model.Filter(
								sFilterPath,
								sap.ui.model.FilterOperator.EQ,
								sDate
							);
							aFilter.push(oFilter);
						}
						break;
					case "sap.m.CheckBox":
					case "com.tgroup.lib.controls.CheckBox":
						if (oSelectionSet.getSelected()) {
							oFilter = new sap.ui.model.Filter(
								sFilterPath,
								sap.ui.model.FilterOperator.EQ,
								oSelectionSet.getSelected()
							);
							aFilter.push(oFilter);
						}
						break;
					case "sap.m.ComboBox":
					case "com.tgroup.lib.controls.ComboBox":
						var oSelectedItem = oSelectionSet.getSelectedItem();
						if (oSelectedItem) {
							oFilter = new sap.ui.model.Filter(
								sFilterPath,
								sap.ui.model.FilterOperator.EQ,
								oSelectedItem.getKey()
							);
							oFilter.textValue = oSelectedItem.getText();
							aFilter.push(oFilter);
						}
						break;
					case "sap.m.MultiComboBox":
					case "com.tgroup.lib.controls.MultiComboBox":
						var aItemFilter = this.getFilterFromSelectedItems(oSelectionSet.getSelectedItems(), sFilterPath);
						aFilter = aFilter.concat(aItemFilter);
						break;
					case "sap.m.MultiInput":
					case "com.tgroup.lib.controls.MultiInput":
						var aTokenFilter;
						if (oSelectionSet.getHasSuggestionItems() && oSelectionSet.getSuggestionKey() && oSelectionSet.getSuggestionKey.length > 0) {
							// add tokens to filter array
							aTokenFilter = this.getFilterFromTokens(oSelectionSet.getTokens(), sFilterPath + "Key");
						} else {
							// add tokens to filter array
							aTokenFilter = this.getFilterFromTokens(oSelectionSet.getTokens(), sFilterPath);
						}
						aFilter = aFilter.concat(aTokenFilter);
						break;
					case "sap.m.RatingIndicator":
						if (oSelectionSet.getValue() !== 0.0) {
							oFilter = new sap.ui.model.Filter(
								oSelectionSet.data("field"),
								sap.ui.model.FilterOperator.EQ,
								oSelectionSet.getValue()
							);
							aFilter.push(oFilter);
						}
						break;
					default:
					}
				}
			}

			return aFilter;
		},

		/**
		 * Create a string of selected filters to display them in the filter info bar
		 * @function
		 * @public
		 * @param {array} paSelectionSet array of controls
		 * @return {string} sFilterString filter as string
		 * */
		getFilterString: function (paSelectionSet) {
			var aFilter = this.getFilterList(paSelectionSet);
			// Create filter string for info toolbar
			var oDateFormatter = sap.ui.core.format.DateFormat.getDateInstance();
			var sFilterString = aFilter.map(function (filter) {
				if (filter.oValue1 instanceof Date) {
					return oDateFormatter.format(filter.oValue1);
				} else if (filter.textValue && filter.textValue !== "") {
					return filter.textValue;
				} else {
					return filter.oValue1;
				}
			}).join(", ");

			return sFilterString;
		},

		/**
		 * get selection set from filterbar.
		 * @deprecated Use the same functionality from the generalHelper instead
		 * @public
		 * @param {object} poFilterBar filter bar control
		 * @return {array} aSelectionSet selection set
		 */
		getFilterBarSelectionSet: function (poFilterBar) {
			var aFilters = poFilterBar.getFilterGroupItems();
			var aSelectionSet = [];
			for (var i = 0; i <= aFilters.length - 1; i++) {
				aSelectionSet.push(poFilterBar.determineControlByFilterItem(aFilters[i]));
			}
			return aSelectionSet;
		},

		/**
		 * clear the filter controls.
		 * @public
		 * @param {array} paSelectionSet array of controls
		 */
		clearFilter: function (paSelectionSet) {
			if (paSelectionSet) {

				for (var j = 0; j < paSelectionSet.length; j++) {
					var oSelectionSet = paSelectionSet[j];
					var sControl = oSelectionSet.getMetadata().getName();

					// input controls with different filters from placeholder
					switch (sControl) {
					case "sap.m.Input":
					case "com.tgroup.lib.controls.Input":
						var sPreviousValue = oSelectionSet.getValue();
						oSelectionSet.setValue("");
						oSelectionSet.fireLiveChange({
							value: "",
							previousValue: sPreviousValue
						});
						break;
					case "sap.m.DatePicker":
					case "com.tgroup.lib.controls.DatePicker":
						oSelectionSet.setDateValue(undefined);
						break;
					case "sap.m.TimePicker":
					case "com.tgroup.lib.controls.TimePicker":
						// time control
						oSelectionSet.setDateValue(undefined);
						break;
					case "sap.m.DateTimeInput":
					case "com.tgroup.lib.controls.TimePickerOld":
						// time control
						oSelectionSet.setDateValue(undefined);
						break;
					case "sap.m.CheckBox":
					case "com.tgroup.lib.controls.CheckBox":
						oSelectionSet.setSelected(false);
						break;
					case "sap.m.ComboBox":
					case "com.tgroup.lib.controls.ComboBox":
						oSelectionSet.setSelectedKey("");
						oSelectionSet.fireChange();
						break;
					case "sap.m.MultiComboBox":
					case "com.tgroup.lib.controls.MultiComboBox":
						oSelectionSet.setSelectedKeys([]);
						oSelectionSet.fireSelectionFinish({
							selectedItems: []
						});
						break;
					case "sap.m.MultiInput":
					case "com.tgroup.lib.controls.MultiInput":
						oSelectionSet.setTokens([]);
						break;
					case "sap.m.RatingIndicator":
						oSelectionSet.setValue(0.0);
						break;
					default:
					}
				}
			}
		},

		/**
		 * get the field or the name of the control.
		 * @public
		 * @param {control} poControl control
		 * @return {string} sName name of the control
		 */
		getNameOfControl: function (poControl) {
			var sName = "";

			if (poControl) {
				// get field name
				if (poControl.getField && poControl.getField() !== "") {
					sName = poControl.getField();
				} else if (poControl.getName) {
					sName = poControl.getName();
				}
			}

			return sName;
		},

		/**
		 * get filter from value. dynamic filter operator.
		 * @public
		 * @param {string} psValue the value
		 * @param {string} psFilterPath attribute filter path
		 * @return {object} oFilter filter object
		 */
		getFilterFromValue: function (psValue, psFilterPath) {
			// check which filter operator
			var sFilterOperator;
			var oFilter;
			if (psValue && psValue !== "") {
				if (psValue.match(/\*/)) {
					if (psValue.match(/^\*[^\*]*$/)) {
						// end with
						sFilterOperator = sap.ui.model.FilterOperator.EndsWith;
					} else if (psValue.match(/^[^\*]*\*$/)) {
						// start with
						sFilterOperator = sap.ui.model.FilterOperator.StartsWith;
					} else {
						// contains (like)
						sFilterOperator = sap.ui.model.FilterOperator.Contains;
					}

					// replace placeholder (*)
					psValue = psValue.replace(/\*/g, "");
				} else {
					// no placeholder so equals search
					sFilterOperator = sap.ui.model.FilterOperator.EQ;
				}

				oFilter = new sap.ui.model.Filter(
					psFilterPath,
					sFilterOperator,
					psValue
				);
			}
			return oFilter;
		},

		/**
		 * check required from the filter bars.
		 * @param {array} paSelectionSet array of controls
		 * @param {string} psMessageClass message class
		 * @param {string} psMsgnr message number
		 * @param {string} psLayoutName name of the layout
		 * @return {array} aRequired array of messages with the required fields
		 */
		checkRequiredList: function (paSelectionSet, psMessageClass, psMsgnr, psLayoutName) {
			var aRequired = [];
			var oMessage;

			if (paSelectionSet && psMessageClass && psMsgnr && psMessageClass !== "" && psMsgnr !== "") {

				var oLayouts = sap.ui.getCore().getModel("layout");
				var oLayout = oLayouts.getProperty("/" + psLayoutName);

				// check control required and filter is not empty
				for (var i = 0; i < paSelectionSet.length; i++) {
					var oSelectionSet = paSelectionSet[i];
					// dynamic input fix
					if (oSelectionSet.getMetadata().getName() === "sap.m.FlexBox") {
						oSelectionSet = (oSelectionSet.getItems().length > 0) ? oSelectionSet.getItems()[0] : oSelectionSet;
					}
					var sControl = oSelectionSet.getMetadata().getName();
					var sName = this.getNameOfControl(oSelectionSet);
					var bError = false;
					// check which control
					switch (sControl) {
					case "sap.m.Input":
					case "com.tgroup.lib.controls.Input":
						// We do have to separate between version 1.28 lower and higher
						if (oSelectionSet.getRequired && oSelectionSet.getRequired()) {
							if (oSelectionSet.getValue() === "") {
								if (oSelectionSet.getHasSuggestionItems() === false) {
									bError = true;
								} else {
									if (oSelectionSet.getKey() === "") {
										bError = true;
									}
								}
							} else {
								if (oSelectionSet.getHasSuggestionItems() === true) {
									if (oSelectionSet.getKey() === "") {
										bError = true;
									}
								}
							}
						} else if (oSelectionSet.getMandatory()) {
							if (oSelectionSet.getValue() === "") {
								if (oSelectionSet.getHasSuggestionItems() === false) {
									bError = true;
								} else {
									if (oSelectionSet.getKey() === "") {
										bError = true;
									}
								}
							} else {
								if (oSelectionSet.getHasSuggestionItems() === true) {
									if (oSelectionSet.getKey() === "") {
										bError = true;
									}
								}
							}
						}
						break;
					case "sap.m.TextArea":
					case "com.tgroup.lib.controls.TextArea":
						// We do have to separate between version 1.28 lower and higher
						if (oSelectionSet.getRequired) {
							if (oSelectionSet.getRequired() && oSelectionSet.getValue() === "") {
								bError = true;
							}
						} else {
							if (oSelectionSet.getMandatory() && oSelectionSet.getValue() === "") {
								bError = true;
							}
						}
						break;
					case "sap.m.DatePicker":
					case "sap.m.TimePicker":
					case "com.tgroup.lib.controls.TimePicker":
					case "com.tgroup.lib.controls.DatePicker":
					case "sap.m.DateTimeInput":
					case "com.tgroup.lib.controls.TimePickerOld":
						// We do have to separate between version 1.28 lower and higher
						if (oSelectionSet.getRequired) {
							if (oSelectionSet.getRequired() && (!oSelectionSet.getDateValue() || oSelectionSet.getDateValue() === "")) {
								bError = true;
							}
						} else {
							if (oSelectionSet.getMandatory() && (!oSelectionSet.getDateValue() || oSelectionSet.getDateValue() === "")) {
								bError = true;
							}
						}
						break;
					case "sap.m.ComboBox":
					case "com.tgroup.lib.controls.ComboBox":
						// We do have to separate between version 1.28 lower and higher
						if (oSelectionSet.getRequired) {
							if (oSelectionSet.getRequired() && (!oSelectionSet.getSelectedItem() || oSelectionSet.getSelectedKey() === "")) {
								bError = true;
							}
						} else {
							if (oSelectionSet.getMandatory() && (!oSelectionSet.getSelectedItem() || oSelectionSet.getSelectedKey() === "")) {
								bError = true;
							}
						}
						break;
					case "sap.m.MultiComboBox":
					case "com.tgroup.lib.controls.MultiComboBox":
						// We do have to separate between version 1.28 lower and higher
						if (oSelectionSet.getRequired) {
							if (oSelectionSet.getRequired() && (!oSelectionSet.getSelectedItems() || oSelectionSet.getSelectedItems().length < 1)) {
								bError = true;
							}
						} else {
							if (oSelectionSet.getMandatory() && (!oSelectionSet.getSelectedItems() || oSelectionSet.getSelectedItems().length < 1)) {
								bError = true;
							}
						}
						break;
					case "sap.m.MultiInput":
					case "com.tgroup.lib.controls.MultiInput":
						// We do have to separate between version 1.28 lower and higher
						if (oSelectionSet.getRequired) {
							if (oSelectionSet.getRequired() && (!oSelectionSet.getTokens() || oSelectionSet.getTokens().length < 1)) {
								bError = true;
							}
						} else {
							if (oSelectionSet.getMandatory() && (!oSelectionSet.getTokens() || oSelectionSet.getTokens().length < 1)) {
								bError = true;
							}
						}
						break;
					default:
					}

					if (bError) {
						oMessage = {
							Type: "E",
							Class: psMessageClass,
							Msgnr: psMsgnr,
							Param1: (oLayout.Fields[sName]) ? oLayout.Fields[sName].Text : ""
						};
						aRequired.push(oMessage);

					}
				}
			}
			return aRequired;
		},

		/**
		 * get the filter from token array.
		 * @public
		 * @param {array} paTokens list of tokens
		 * @param {string} psFilterPath attribute filter path
		 * @return {array} aFilter array with filter
		 */
		getFilterFromTokens: function (paTokens, psFilterPath) {
			var aFilter = [];
			if (paTokens && paTokens.length > 0) {
				// get the filter
				var oToken;
				var oFilter;
				for (var i = 0; i < paTokens.length; i++) {
					oToken = paTokens[i];
					var oRange = oToken.data("range");
					if (oRange && oRange !== undefined) {
						var sOperator = (oRange.exclude) ? sap.ui.model.FilterOperator.NE : oRange.operation;
						oFilter = new sap.ui.model.Filter(
							psFilterPath,
							sOperator,
							oRange.value1,
							oRange.value2
						);
					} else {
						// get filter with placeholders
						oFilter = this.getFilterFromValue(oToken.getKey(), psFilterPath);
					}
					aFilter.push(oFilter);
				}
			}
			return aFilter;
		},

		/**
		 * get the filter from items array.
		 * @public
		 * @param {array} paItems list of itmes
		 * @param {string} psFilterPath attribute filter path
		 * @return {array} aFilter array with filter
		 */
		getFilterFromSelectedItems: function (paItems, psFilterPath) {
			var aFilter = [];
			if (paItems && paItems.length > 0) {
				// get the filter
				var oItem;
				var oFilter;
				for (var i = 0; i < paItems.length; i++) {
					oItem = paItems[i];
					// get filter with placeholders
					oFilter = this.getFilterFromValue(oItem.getKey(), psFilterPath);
					aFilter.push(oFilter);
				}
			}
			return aFilter;
		},

		/**
		 * convert odata F4 search help for json-model.
		 * @public
		 * @param {Object} poData oData
		 * @param {JSONModel} poModel? set the oData converted.
		 */
		convertF4SearchHelp: function (poData, poModel) {
			// default F4 model
			if (!poModel) {
				poModel = sap.ui.getCore().getModel("F4Layout");
			}
			if (poData && poData.results && poModel) {

				var aF4 = [];

				// convert every result object group by fieldname and function.
				for (var i = 0; i < poData.results.length; i++) {
					var oObject = poData.results[i];
					// get fieldname
					var sFieldname = (oObject.Fieldname) ? "/" + oObject.Fieldname : "";
					// set fieldname object
					if (!poModel.getProperty(sFieldname)) {
						poModel.setProperty(sFieldname, {});
					}

					// set object as msgnr in message class object.
					var sFunction = (oObject.Function && oObject.Function !== "") ? oObject.Function : "NONE";

					// low parameter array
					var aLow = [];
					if (oObject.Low && oObject.Low !== "") {
						aLow.push(oObject.Low);
					}
					if (oObject.F4LowSet && oObject.F4LowSet.results && oObject.F4LowSet.results.length > 0) {
						for (var j = 0; j < oObject.F4LowSet.results.length; j++) {
							var oLow = oObject.F4LowSet.results[j];
							if (oLow.Low && oLow.Low !== "") {
								aLow.push(oLow.Low);
							}
						}
					}
					oObject.Low = aLow;

					aF4.push(oObject);
				}
				poModel.setProperty(sFieldname + "/" + sFunction, aF4);
			}
		},

		/**
		 * check, if F4 search help already loaded and is in F4Layout model.
		 * For appName and fieldname. F4 search help with fieldname and empty appName are not relevant.
		 * @public
		 * @param {string} psAppName app name
		 * @param {string} psFieldname field name
		 * @param {JSONModel} poModel? JSON model
		 * @reutrn {boolean} bExist flag, f4 search help exist in model or not
		 */
		isF4SearchHelpExist: function (psAppName, psFieldname, poModel) {
			var bExist = false;
			var aSearchHelp;

			if (psFieldname && psFieldname !== "" && psAppName && psAppName !== "") {
				// default F4 model
				if (!poModel) {
					poModel = sap.ui.getCore().getModel("F4Layout");
				}
				if (poModel) {
					// get layout from fieldname and appname
					aSearchHelp = poModel.getProperty("/" + psFieldname + "/" + psAppName);

					if (aSearchHelp) {
						bExist = true;
					}
				}
			}

			return bExist;
		},

		/**
		 * is input type combobox.
		 * @private
		 * @public {string} psInputType input type
		 * @return {boolean} bComboBox flag, is combobox
		 */
		_isInputTypeComboBox: function (psInputType) {
			var bComboBox = false;
			if (this.inputType.comboBox === psInputType) {
				bComboBox = true;
			}
			return bComboBox;
		},

		/**
		 * is input type multi combobox.
		 * @private
		 * @public {string} psInputType input type
		 * @return {boolean} bComboBox flag, is combobox
		 */
		_isInputTypeMultiComboBox: function (psInputType) {
			var bComboBox = false;
			if (this.inputType.multiComboBox === psInputType) {
				bComboBox = true;
			}
			return bComboBox;
		},

		/**
		 * get the text from the combobox field settings.
		 * read the ComboBox or Domain values and search by Key.
		 * @param {object} poField field settings
		 * @param {string} psKey key value
		 */
		getTextFromComboBox: function (poField, pfnCallback, psKey) {
			var oModel;
			var aFilter;
			var sEntitySet;
			var sTextField;
			var sKeyField;
			var sModelName;

			// has domain values more important, as the combobox customizing
			if (poField.HasDom && poField.Domname) {
				oModel = sap.ui.getCore().getModel("dataprovider");
				aFilter = [new sap.ui.model.Filter("Domname", sap.ui.model.FilterOperator.EQ, poField.Domname)];
				sEntitySet = "DomainSet";
				sTextField = "Text";
				sKeyField = "Value";
				sModelName = "dataprovider";
			} else if (poField.ComboBox) {
				// fill items from combobox customizing
				sModelName = poField.ComboBox.ModelName;
				if (sModelName) {
					oModel = sap.ui.getCore().getModel(sModelName);
					if (!oModel) {
						var mSetting = {
							disableHeadRequestForToken: true
						};
						// model not exist, create model with service
						if (poField.ComboBox.Service && poField.ComboBox.Service !== "") {
							var sConnection = LoginHandler.getConnection();
							oModel = new sap.ui.model.odata.v2.ODataModel(sConnection + poField.ComboBox.Service, mSetting);
							sap.ui.getCore().setModel(oModel, sModelName);
						}
					}

					// get the filters
					if (poField.Filters) {
						aFilter = this.convertToModelFilter(poField.Filters);
					}

					sEntitySet = poField.ComboBox.EntitySet;
					sKeyField = poField.ComboBox.KeyField;
					sTextField = poField.ComboBox.TextField;
				}
			}

			// read the data
			if (oModel && sEntitySet && sEntitySet !== "" && sKeyField && sTextField) {
				var fnSuccess = function (poData) {
					if (poData && poData.results && poData.results.length > 0) {
						for (var i = 0; i < poData.results.length; i++) {
							var oObject = poData.results[i];
							if (oObject[sKeyField] === psKey || oObject[sKeyField] === psKey + "") {
								pfnCallback(oObject[sTextField]);
								break;
							}
						}
					}
				};
				poModels.readData(oModel, sEntitySet, fnSuccess, aFilter);
			}

			return psKey;
		},

		/**
		 * get custom control by checking field properties. 
		 * @public
		 * @param {string} psLayout layout name
		 * @param {object} poField field properties
		 * @param {map} pmSetting? setting
		 * @return {control} specified custom control
		 */
		getCustomControl: function (psLayout, poField, pmSetting, oContext) {
			var oInput;

			if (pmSetting && pmSetting.output) {
				if (poField.InputType === this.inputType.icon) {
					oInput = new sap.ui.core.Icon({
						src: "sap-icon://" + ((pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : "")
					});
				} else if (poField.InputType === this.inputType.rating) {
					oInput = new sap.m.RatingIndicator({
						value: (pmSetting && pmSetting.value) ? "{ path: '" + pmSetting.value + "', type: 'sap.ui.model.odata.type.Decimal' }" : 0.0,
						enabled: false
					});
					oInput.data("field", poField.ElementName);
				} else if (poField.InputType === this.inputType.html) {
					oInput = new sap.ui.core.HTML({
						content: "{" + pmSetting.value + "}"
					});
				} else {

					var aNavItems = this.getNavItems(poField.ElementName);

					if (aNavItems && aNavItems.length > 0) {
						oInput = new com.tgroup.lib.controls.Link({
							text: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : "",
							layoutName: psLayout,
							field: poField.ElementName,
							bindingModel: pmSetting.bindingModel,
							context: oContext
						});

					} else {
						oInput = new com.tgroup.lib.controls.Text({
							text: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : "",
							layoutName: psLayout,
							field: poField.ElementName,
							bindingModel: pmSetting.bindingModel
						});
					}
				}
			} else if (poField.Type === this.type.date) {
				// date control from annotations
				oInput = new com.tgroup.lib.controls.DatePicker({
					layoutName: psLayout,
					field: poField.ElementName,
					editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
					enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
					dateValue: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : undefined
				});
			} else {
				switch (poField.InputType) {
				case this.inputType.html:
					oInput = new sap.ui.core.HTML({
						content: "{" + pmSetting.value + "}"
					});
					break;
				case this.inputType.multiInput:
					if (pmSetting && pmSetting.bindingModel) {
						oInput = new com.tgroup.lib.controls.MultiInput({
							layoutName: psLayout,
							field: poField.ElementName,
							editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
							enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
							tokens: {
								path: (pmSetting && pmSetting.value) ? pmSetting.value : "",
								template: new sap.m.Token({
									key: {
										path: pmSetting.bindingModel + ">"
									},
									text: {
										path: pmSetting.bindingModel + ">"
									}
								})
							},
							bindingModel: pmSetting.bindingModel
						});
					} else {
						oInput = new com.tgroup.lib.controls.MultiInput({
							layoutName: psLayout,
							field: poField.ElementName,
							editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
							enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true
						});
					}
					break;
				case this.inputType.checkBox:
					oInput = new com.tgroup.lib.controls.CheckBox({
						layoutName: psLayout,
						field: poField.ElementName,
						editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
						enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
						selected: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : false
					});
					break;
				case this.inputType.label:
					if (pmSetting && pmSetting.value) {
						oInput = new com.tgroup.lib.controls.Label({
							text: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : "",
							layoutName: psLayout,
							field: poField.ElementName,
							bindingModel: pmSetting.bindingModel
						});
					} else {
						oInput = new com.tgroup.lib.controls.Label({
							layoutName: psLayout,
							field: poField.ElementName
						});
					}
					break;
				case this.inputType.comboBox:
					oInput = new com.tgroup.lib.controls.ComboBox({
						layoutName: psLayout,
						field: poField.ElementName,
						editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
						enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
						selectedKey: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : ""
					});
					break;
				case this.inputType.multiComboBox:
					oInput = new com.tgroup.lib.controls.MultiComboBox({
						layoutName: psLayout,
						field: poField.ElementName,
						editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
						enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
						selectedKeys: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : ""
					});
					break;
				case this.inputType.dateTime:
					oInput = new com.tgroup.lib.controls.DatePicker({
						layoutName: psLayout,
						field: poField.ElementName,
						editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
						enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
						dateValue: (pmSetting && pmSetting.value) ? "{" + pmSetting.value + "}" : undefined
					});
					break;
				case this.inputType.time:
					if (com.tgroup.lib.controls.TimePicker) {
						if (pmSetting && pmSetting.value) {
							oInput = new com.tgroup.lib.controls.TimePicker({
								layoutName: psLayout,
								field: poField.ElementName,
								editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
								enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
								value: {
									path: pmSetting.value,
									type: new sap.ui.model.odata.type.Time()
								}
							});
						} else {
							oInput = new com.tgroup.lib.controls.TimePicker({
								layoutName: psLayout,
								field: poField.ElementName,
								editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
								enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true
							});
						}
					} else if (com.tgroup.lib.controls.TimePickerOld) {
						if (pmSetting && pmSetting.value) {
							oInput = new com.tgroup.lib.controls.TimePickerOld({
								layoutName: psLayout,
								field: poField.ElementName,
								editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
								enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
								dateValue: {
									path: pmSetting.value,
									formatter: poGeneralFormatter.formatTime
								}
							});
						} else {
							oInput = new com.tgroup.lib.controls.TimePickerOld({
								layoutName: psLayout,
								field: poField.ElementName,
								editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
								enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true
							});
						}
					}
					break;
				case this.inputType.textArea:
					oInput = new com.tgroup.lib.controls.TextArea({
						layoutName: psLayout,
						field: poField.ElementName,
						editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
						enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
						bindingModel: pmSetting.bindingModel,
						value: "{" + pmSetting.value + "}"
					});
					break;
				case this.inputType.rating:
					oInput = new sap.m.RatingIndicator({
						value: (pmSetting && pmSetting.value) ? "{ path: '" + pmSetting.value + "', type: 'sap.ui.model.odata.type.Decimal' }" : 0.0
					});
					oInput.data("field", poField.ElementName);
					break;
				case this.inputType.password:
				default:
					// value path exist, set Binding
					if (pmSetting && pmSetting.value && pmSetting.value !== "") {
						oInput = new com.tgroup.lib.controls.Input({
							layoutName: psLayout,
							field: poField.ElementName,
							editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
							enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true,
							bindingModel: pmSetting.bindingModel,
							value: "{" + pmSetting.value + "}"
						});
					} else {
						oInput = new com.tgroup.lib.controls.Input({
							layoutName: psLayout,
							field: poField.ElementName,
							editable: (pmSetting && pmSetting.editable !== undefined) ? pmSetting.editable : true,
							enabled: (pmSetting && pmSetting.enabled !== undefined) ? pmSetting.enabled : true
						});
					}
				}
			}

			return oInput;
		},

		/**
		 * generate the functions for the value help dialog.
		 * if exist in the settings use this.
		 * fallback function if not exist.
		 * @private
		 * @param {object} poInput input field which startet the dialog
		 * @param {object} pmSetting map with settings
		 * @return {object} pmFn map with the functions
		 */
		_generateValueHelpDialogFunctions: function (poInput, pmSetting) {
			var pmFn = {};

			// set ok function
			if (pmSetting && pmSetting.ok) {
				pmFn.ok = pmSetting.ok;
			} else {
				pmFn.ok = function (oEvent) {
					var aParamTokens = oEvent.getParameter("tokens");
					var aTokens = [];

					// single selection only set the last selected
					if (!pmSetting.supportMultiselect && aParamTokens.length > 1) {
						aTokens.push(aParamTokens[aParamTokens.length - 1]);
					} else {
						// multi selection set all tokens or single selection with 1 token
						aTokens = aParamTokens;
					}

					// input or multi input.
					if (poInput.getMetadata().getName() === "sap.m.MultiInput" || poInput.getMetadata().getName() ===
						"com.tgroup.lib.controls.MultiInput") {
						poInput.setTokens(aTokens);
					} else {
						poInput.setValue(aTokens[0].getKey());
					}
					this.close();
				};
			}

			// set cancel function
			if (pmSetting && pmSetting.cancel) {
				pmFn.cancel = pmSetting.cancel;
			} else {
				pmFn.cancel = function () {
					this.close();
				};
			}

			// set afterClose function
			if (pmSetting && pmSetting.afterClose) {
				pmFn.afterClose = pmSetting.afterClose;
			} else {
				pmFn.afterClose = function () {
					if (this.getFilterBar()) {
						this.getFilterBar().destroy(true);
					}
					if (this.getTable()) {
						this.getTable().destroy(true);
					}
					this.destroy(true);
				};
			}

			return pmFn;
		},

		/**
		 * get a value help dialog. F4 search help.
		 * @public
		 * @param {object} poInput input field which startet the dialog
		 * @param {object} psModelName name of the data model
		 * @param {object} pmSetting? settings for the dialog
		 */
		getValueHelpDialog: function (poInput, psModelName, pmSetting) {
			var oValueHelpDialog;

			// check pmSetting is not undefined
			if (!pmSetting) {
				pmSetting = {};
			}

			if (poInput) {
				// get app name and fieldname
				var sAppName = poInput.data("appName");
				var sFieldname = poInput.data("fieldname");

				// get search help
				var aSearchHelp = this.getF4SearchHelpFromLayout(sAppName, sFieldname, pmSetting.layoutModel);

				// only if searchhelp exist
				if (aSearchHelp && poInput) {
					// set handling functions (fallback default functions, if not exist in the settings)
					var mFn = this._generateValueHelpDialogFunctions(poInput, pmSetting);

					oValueHelpDialog = new sap.ui.comp.valuehelpdialog.ValueHelpDialog({
						basicSearchText: poInput.getValue(),
						title: (pmSetting.title) ? pmSetting.title : "",
						supportMultiselect: (pmSetting.supportMultiselect) ? pmSetting.supportMultiselect : false,
						supportRanges: (pmSetting.supportRanges) ? pmSetting.supportRanges : false,
						supportRangesOnly: (pmSetting.supportRangesOnly) ? pmSetting.supportRangesOnly : false,
						key: (pmSetting.key) ? pmSetting.key : "",
						descriptionKey: (pmSetting.descriptionKey) ? pmSetting.descriptionKey : "",
						filterMode: (pmSetting.filterMode) ? pmSetting.filterMode : false,
						ok: mFn.ok,
						cancel: mFn.cancel,
						afterClose: mFn.afterClose
					});

					// set tokens
					// input or multi input.
					if (poInput.getMetadata().getName() === "sap.m.MultiInput" || poInput.getMetadata().getName() ===
						"com.tgroup.lib.controls.MultiInput") {
						oValueHelpDialog.setTokens(poInput.getTokens());
					} else {
						oValueHelpDialog.setTokens([new sap.m.Token({
							key: poInput.getValue
						})]);
						// set for one way no leading zeroes input value live update to simulate an two way binding
						oValueHelpDialog.getTable().attachRowSelectionChange(function (poData) {
							var sLayout = this.input.getLayoutName();
							var sField = this.input.getField();
							var oField = this.helper.getField(sLayout, sField);
							var bNoLeadingZeroes = this.helper.oFormatter.getNoLeadingZeroes(oField.NoLeadingZeroes);
							if (this.setting.key && bNoLeadingZeroes) {
								var sValue = poData.getParameter("rowContext").getProperty(this.setting.key);
								this.input.fireLiveChange({
									previousValue: this.input.getValue(),
									value: sValue
								});
							}
						}.bind({
							input: poInput,
							setting: pmSetting,
							helper: this
						}));
					}
				}

				// set table
				this._setValueHelpDialogTable(oValueHelpDialog, aSearchHelp, psModelName);

				// set ranges
				this._setValueHelpDialogRanges(oValueHelpDialog, aSearchHelp, pmSetting);

				// set filter bar
				this._setValueHelpDialogFilterBar(oValueHelpDialog, aSearchHelp, psModelName, pmSetting);

			}

			return oValueHelpDialog;
		},

		/**
		 * get ranges value help dialog for multi input.
		 * @public
		 * @param {object} poInput multi input control
		 * @param {object} pmSetting setting of the value help dialog
		 */
		getRangesValueHelpDialog: function (poInput, pmSetting) {
			var oValueHelpDialog;

			if (poInput && pmSetting) {
				// get ok, cancel and afterClose functions
				var mFn = this._generateValueHelpDialogFunctions(poInput, pmSetting);

				// create the value help dialog
				oValueHelpDialog = new sap.ui.comp.valuehelpdialog.ValueHelpDialog({
					title: (pmSetting.title) ? pmSetting.title : "",
					supportMultiselect: true,
					supportRanges: true,
					supportRangesOnly: true,
					key: (pmSetting.key) ? pmSetting.key : "",
					descriptionKey: (pmSetting.descriptionKey) ? pmSetting.descriptionKey : "",
					filterMode: false,
					ok: mFn.ok,
					cancel: mFn.cancel,
					afterClose: mFn.afterClose
				});
			}

			return oValueHelpDialog;
		},

		/**
		 * set the ranges fields for the value help dialog.
		 * @private
		 * @param {object} poValueHelpDialog the help dialog
		 * @param {object} pmSetting? settings for the dialog
		 */
		_setValueHelpDialogRanges: function (poValueHelpDialog, paSearchHelp, pmSetting) {
			// set ranges attributes from search help
			if (pmSetting.supportRanges || pmSetting.supportRangesOnly) {
				var aRanges = [];

				// set range for key attribute of the paSearch Help
				// Method "find" doesn't work for older Browsers like IE11
				var oKeyObject;
				if (typeof paSearchHelp.find === "function") {
					oKeyObject = paSearchHelp.find(function (oObj) {
						return oObj.Sfieldname === pmSetting.key;
					});
				} else {
					for (var i = 0; i <= paSearchHelp.length - 1; i++) {
						var oObj = paSearchHelp[i];
						if (oObj.Sfieldname === pmSetting.key) {
							oKeyObject = oObj;
							break;
						}
					}
				}

				if (oKeyObject) {
					aRanges.push({
						label: oKeyObject.Text,
						key: oKeyObject.Sfieldname
					});
				}

				poValueHelpDialog.setRangeKeyFields(aRanges);
			}
		},

		/**
		 * set the filter bar for the value help dialog. 
		 * @private
		 * @param {object} poValueHelpDialog the help dialog
		 * @param {object} paSearchHelp F4 search help
		 * @param {string} psModelName name of the data model
		 * @param {object} pmSetting? settings for the dialog
		 */
		_setValueHelpDialogFilterBar: function (poValueHelpDialog, paSearchHelp, psModelName, pmSetting) {
			var that = this;
			var aFilterGroupItem = [];
			var sF4Set;

			// sort search help object by selection position
			paSearchHelp.sort(function (a, b) {
				return parseInt(a.Spos, 10) - parseInt(b.Spos, 10);
			});

			// set filter group items from search help
			for (var i = 0; i < paSearchHelp.length; i++) {
				var oField = paSearchHelp[i];

				// get selection positon
				var nSpos = (oField.Spos) ? parseInt(oField.Spos, 10) : 0;
				// get F4 search help set
				sF4Set = oField.F4Set;

				if (nSpos > 0) {
					var aCustomData = [];
					var oCustomData;
					// title
					oCustomData = new sap.ui.core.CustomData({
						key: "title",
						value: oField.Text
					});
					aCustomData.push(oCustomData);

					// key
					oCustomData = new sap.ui.core.CustomData({
						key: "key",
						value: oField.Sfieldname
					});
					aCustomData.push(oCustomData);

					// create Multi Input for value help dialog
					var oMultiInput = new com.tgroup.lib.controls.MultiInput({
						name: oField.Sfieldname,
						customData: aCustomData
					});

					// add low tokens
					if (oField.Low && oField.Low.length > 0) {
						for (var j = 0; j < oField.Low.length; j++) {
							var sLow = oField.Low[j];
							if (sLow !== "") {
								// value
								oMultiInput.addToken(new sap.m.Token({
									key: sLow,
									text: sLow
								}));
							}
						}
					} else {
						// We do have to check the parameters from startup if we do have a value
						var oStartupParameters = poGeneralHelper.getStartupParameters();
						if (oStartupParameters && typeof oStartupParameters !== "undefined") {
							//Looping through the parameters
							for (var sProperty in oStartupParameters) {
								if (oStartupParameters.hasOwnProperty(sProperty)) {
									if (sProperty === oField.Sfieldname) {
										oMultiInput.addToken(new sap.m.Token({
											key: oStartupParameters[sProperty],
											text: oStartupParameters[sProperty]
										}));
									}
								}
							}
						}
					}

					var oFilterGroupItem = new sap.ui.comp.filterbar.FilterGroupItem({
						label: oField.Text,
						name: oField.Sfieldname,
						groupName: oField.Sfieldname,
						control: oMultiInput
					});
					aFilterGroupItem.push(oFilterGroupItem);
				}
			}

			var oFilterBar = new sap.ui.comp.filterbar.FilterBar({
				advancedMode: true,
				filterBarExpanded: (pmSetting.filterBarExpanded) ? pmSetting.filterBarExpanded : false,
				filterGroupItems: aFilterGroupItem,
				search: function (poEvent) {
					// get the values from the input fields and filter the table
					var aSelectionSet = poEvent.getParameter("selectionSet");
					if (aSelectionSet) {
						var aFilter = that.getFilterList(aSelectionSet);
						// filter the table data
						poValueHelpDialog.getTable().bindRows({
							path: psModelName + ">/" + sF4Set,
							filters: aFilter
						});
					}
				}
			});

			poValueHelpDialog.setFilterBar(oFilterBar);
		},

		/**
		 * set the table of the value help dialog.
		 * @private
		 * @param {object} poValueHelpDialog the help dialog
		 * @param {object} paSearchHelp F4 search help
		 * @param {string} psModelName name of the data model
		 */
		_setValueHelpDialogTable: function (poValueHelpDialog, paSearchHelp, psModelName) {

			var oTable = poValueHelpDialog.getTable();

			if (oTable) {
				var sF4Set = "";
				var oColumn;
				var aFilter = [];
				var oFilter;

				// sort search help object by list position
				paSearchHelp.sort(function (a, b) {
					return parseInt(a.Lpos, 10) - parseInt(b.Lpos, 10);
				});

				// add columns dynamic
				for (var i = 0; i < paSearchHelp.length; i++) {
					var oField = paSearchHelp[i];
					sF4Set = oField.F4Set;

					oColumn = new sap.ui.table.Column({
						label: oField.Text,
						template: new sap.m.Label({
							text: "{" + psModelName + ">" + oField.Sfieldname + "}"
						})
					});

					oTable.addColumn(oColumn);

					// set the filters
					if (oField.Low.length > 0) {
						for (var j = 0; j < oField.Low.length; j++) {
							var sLow = oField.Low[j];
							if (sLow !== "") {
								oFilter = this.getFilterFromValue(sLow, oField.Sfieldname);
								aFilter.push(oFilter);
							}
						}
					} else {
						// We do have to check the parameters from startup if we do have a value
						var oStartupParameters = poGeneralHelper.getStartupParameters();
						if (oStartupParameters && typeof oStartupParameters !== "undefined") {
							//Looping through the parameters
							for (var sProperty in oStartupParameters) {
								if (oStartupParameters.hasOwnProperty(sProperty)) {
									if (sProperty === oField.Sfieldname) {
										oFilter = this.getFilterFromValue(oStartupParameters[sProperty], oField.Sfieldname);
										aFilter.push(oFilter);
									}
								}
							}
						}
					}
				}

				// set the data model
				oTable.setModel(sap.ui.getCore().getModel(psModelName), psModelName);

				// bin rows
				oTable.bindRows({
					path: psModelName + ">/" + sF4Set,
					filters: aFilter
				});
			}

		},

		/**
		 * get the F4 search help from model.
		 * @public
		 * @param {string} psAppName app name
		 * @param {string} psFieldname field name
		 * @param {JSONModel} poModel? JSON model
		 * @param {boolean} pbFunctionSpecific only match to the function
		 * @return {object} aSearchHelp search help
		 */
		getF4SearchHelpFromLayout: function (psAppName, psFieldname, poModel) {
			var aSearchHelp;

			if (psFieldname && psFieldname !== "") {
				// default F4 model
				if (!poModel) {
					poModel = sap.ui.getCore().getModel("F4Layout");
				}
				if (poModel) {
					// get layout from fieldname and appname
					aSearchHelp = poModel.getProperty("/" + psFieldname + "/" + psAppName);

					// fallback get layout from fieldname and empty appname
					if (!aSearchHelp) {
						aSearchHelp = poModel.getProperty("/" + psFieldname + "/NONE");
					}
				}
			}

			return aSearchHelp;
		},

		/**
		 * loop fields from layout and add one column per field.
		 * @public
		 * @param {String} psId table id
		 * @param {String} psLayoutName layout name.
		 * @param {String} psModelName model name
		 * @param {Object} poBindingInfo binding Informations
		 * @param {Boolean} pbSortable sortable
		 * @param {Boolean} pbFilterable sortable
		 * @param {Object} poTable? table optional
		 * @param {Boolean} pbEditable? table editable
		 * @return {Table} table
		 */
		getTableFromLayout: function (psId, psLayoutName, psModelName, poBindingInfo, pbSortable, pbFilterable, poTable, pbEditable) {
			var oColumn;
			var oText;
			var oTable;
			var aCells = [];
			var oControl;
			var iSequence = 0;
			var VBox;
			var aColumn;
			var oSubColumn;

			// get layout
			var oLayouts = sap.ui.getCore().getModel("layout");
			var oLayout = oLayouts.getProperty("/" + psLayoutName);

			if (oLayout) {
				if (poTable) {
					oTable = poTable;
				} else {
					// create Table
					if (pbSortable || pbFilterable) {
						if (psId && psId !== "") {
							oTable = new sap.ui.table.Table(psId);
						} else {
							oTable = new sap.ui.table.Table();
						}
					} else {
						if (psId && psId !== "") {
							oTable = new sap.m.Table(psId);
						} else {
							oTable = new sap.m.Table();
						}
					}
				}

				// loop fields from layout and add one column per field.
				var oElements = oLayout.Fields;
				for (var property in oElements) {
					if (oElements.hasOwnProperty(property)) {
						var bEditable = false;
						if (pbEditable) {
							bEditable = true;
						}
						// check editable or not
						var mSetting = {
							bindingModel: psModelName,
							value: psModelName + ((psModelName && psModelName !== "") ? ">" : "") + property,
							editable: bEditable,
							output: !bEditable,
							enabled: true
						};
						oControl = this.getCustomControl(psLayoutName, oElements[property], mSetting);

						var sTable = oTable.getMetadata().getName();
						if (sTable === "com.tgroup.lib.controls.Table" || sTable === "sap.m.Table") {
							if (oElements[property].Sequence === iSequence && oElements[property].Subsequence > 0) {
								// subsequence values in the same column
								aColumn = oTable.getColumns();
								if (aColumn && aColumn.length > 0) {
									// set the new header text
									oSubColumn = aColumn[aColumn.length - 1];
									if (oElements[property].Text && oElements[property].Text !== "") {
										var oHeader = oSubColumn.getHeader();
										oHeader.setText(oHeader.getText() + "/" + oElements[property].Text);
									}
								}
								if (aCells && aCells.length > 0) {
									// add VBox Control
									var oCell = aCells[aCells.length - 1];
									oCell.addItem(oControl);
								}
							} else {
								if (oElements[property].Subsequence > 0) {
									// subsequence create vbox
									VBox = new sap.m.VBox();
									VBox.addItem(oControl);
									aCells.push(VBox);

								} else {
									// no subsequence one value column
									aCells.push(oControl);
								}

								if (oElements[property].ElementName) {
									oColumn = new sap.m.Column();
									oColumn.data("field", oElements[property].ElementName);
								} else {
									oColumn = new sap.m.Column();
								}
								oText = new sap.m.Text({
									text: oElements[property].Text
								});
								oColumn.setHeader(oText);
							}

							iSequence = oElements[property].Sequence;

						} else {
							if (oElements[property].Sequence === iSequence && oElements[property].Subsequence > 0) {
								// subsequence values in the same column
								aColumn = oTable.getColumns();
								if (aColumn && aColumn.length > 0) {
									oSubColumn = aColumn[aColumn.length - 1];
									if (oElements[property].Text && oElements[property].Text !== "") {
										oSubColumn.setLabel(oSubColumn.getLabel().getText() + "/" + oElements[property].Text);
									}
									var oTemplate = oSubColumn.getTemplate();
									oTemplate.addItem(oControl);
								}
							} else {
								if (oElements[property].Subsequence > 0) {
									// subsequence create vbox
									VBox = new sap.m.VBox();
									VBox.addItem(oControl);
									oColumn = new sap.ui.table.Column({
										label: oElements[property].Text,
										template: VBox
									});
								} else {
									// no subsequence one value column
									oColumn = new sap.ui.table.Column({
										label: oElements[property].Text,
										template: oControl
									});
								}
								if (pbSortable) {
									oColumn.setSortProperty(property);
								}
								if (pbFilterable) {
									oColumn.setFilterProperty(property);
								}
								var sWidth = poFormatter.getWidth(oElements[property].Width);
								if (sWidth) {
									oColumn.setWidth(sWidth);
								}
							}

							iSequence = oElements[property].Sequence;
						}

						oTable.addColumn(oColumn);
					}
				}

				// set template, if empty
				if ((sTable === "com.tgroup.lib.controls.Table" || sTable === "sap.m.Table") && !poBindingInfo.template) {
					// add Content ColumnsListItem
					// create Columns list Item
					var oColumnsListItem = new sap.m.ColumnListItem({
						type: sap.m.ListType.Active,
						cells: aCells
					});
					poBindingInfo.template = oColumnsListItem;
				}

				if (sTable === "com.tgroup.lib.controls.Table" || sTable === "sap.m.Table") {
					oTable.bindItems(poBindingInfo);
				} else {
					oTable.bindRows(poBindingInfo);
				}
			}

			return oTable;
		},

		/**
		 * add message handling to table.
		 * @param {Table} table
		 * @param {Model} message model
		 */
		addShowMessageToTable: function (poTable, poMessageModel) {
			if (poTable) {
				var sType = poTable.getMetadata().getName();
				if (sType === "sap.ui.table.Table" || sType === "com.tgroup.lib.controls.SortTable") {
					poTable.setEnableBusyIndicator(true);

					// function update finished => show messages. 
					var fnBusyStateChange = function (poEvent) {
						var bBusy = poEvent.getParameter("busy");
						if (!bBusy) {
							// show message
							if (poMessageModel) {
								poMessageHandler.showMessage(poMessageModel);
							}
						}
					};

					poTable.attachBusyStateChanged(fnBusyStateChange);
				} else if (sType === "sap.m.Table" || sType === "com.tgroup.lib.controls.Table") {
					// function update finished => show messages. 
					var fnUpdateFinished = function () {
						poMessageHandler.showMessage(poMessageModel);
					};

					poTable.attachUpdateFinished(fnUpdateFinished);
				}
			}
		},

		/**
		 * Method for building up the frame storing our charting object
		 * @public
		 * @param {object} poDimensions The dimensions used for our diagram
		 * @param {object} poMeasures The measures used for our diagram
		 * @param {string} psLayoutName The layout used for the diagram
		 * @param {string} psDiagram The diagram which should be displayed
		 * @param {object} poChartProperties The properties which should be setted for the diagram
		 * @param {string} psModel The name of the model
		 * @param {string} psEntitySet The entityset which stores the data
		 * @param {array} paFilters The array containing the filters
		 * @return {object} The viz frame to be displayed in our charting container
		 * */
		createVizFrame: function (poDimensions, poMeasures, psLayoutName, psDiagram, poChartProperties, psModel, psEntitySet, paFilters) {
			var mFeedMap = this._createFeedMap(poDimensions, poMeasures, psLayoutName);
			var mDataSetMap = this._createDataSetMap(poDimensions, poMeasures, psLayoutName, psModel, psEntitySet);
			return (this._createDiagram(mFeedMap, mDataSetMap, psDiagram, poChartProperties, psModel, psEntitySet, paFilters));
		},

		/**
		 * Internal method used for building up the differnt feeds for the diagram
		 * @private
		 * @param {object} poDimensions Object containing all dimensions
		 * @param {object} poMeasures Object containing all measures
		 * @param {string} psLayoutName The layout name used for finding right field texts
		 * @return {map} mFeedMap The mapping for all dimensions and measures (feeds)
		 * */
		_createFeedMap: function (poDimensions, poMeasures, psLayoutName) {
			var oLayoutModel = sap.ui.getCore().getModel("layout");
			var mFeedMap = {};

			//Starting with Dimensions
			for (var sDimensionName in poDimensions) {
				if (poDimensions.hasOwnProperty(sDimensionName)) {
					var oDimension = poDimensions[sDimensionName];
					var sDimensionText = oLayoutModel.getProperty("/" + psLayoutName + "/Fields/" + sDimensionName + "/Text");
					var sUid = "",
						sValues = "";
					for (var sProperty in oDimension) {
						if (oDimension.hasOwnProperty(sProperty)) {
							if (sProperty === "uid") {
								sUid = oDimension[sProperty];
							}
							if (sProperty === "values") {
								sValues = oDimension[sProperty];
							}
						}
					}
					if (sValues === "") {
						sValues = sDimensionText;
					}
					mFeedMap[sDimensionName] = new sap.viz.ui5.controls.common.feeds.FeedItem({
						"uid": sUid,
						"type": "Dimension",
						"values": [sValues]
					});
				}
			}

			//Continue with Measures
			for (var sMeasureName in poMeasures) {
				if (poMeasures.hasOwnProperty(sMeasureName)) {
					var oMeasure = poMeasures[sMeasureName];
					var sMeasureText = oLayoutModel.getProperty("/" + psLayoutName + "/Fields/" + sMeasureName + "/Text");
					sUid = "";
					sValues = "";
					for (sProperty in oMeasure) {
						if (oMeasure.hasOwnProperty(sProperty)) {
							if (sProperty === "uid") {
								sUid = oMeasure[sProperty];
							}
							if (sProperty === "values") {
								sValues = oMeasure[sProperty];
							}
						}
					}
					if (sValues === "") {
						sValues = sMeasureText;
					}
					mFeedMap[sMeasureName] = new sap.viz.ui5.controls.common.feeds.FeedItem({
						"uid": sUid,
						"type": "Measure",
						"values": [sValues]
					});
				}
			}

			return (mFeedMap);
		},

		/**
		 * Internal method used for building up the differnt data sets for the diagram
		 * @private
		 * @param {object} poDimensions Object containing all dimensions
		 * @param {object} poMeasures Object containing all measures
		 * @param {string} psLayoutName The layout name used for finding right field texts
		 * @param {string} psModel The model containing our data
		 * @return {map} mDataSetMap The map containing all mapping information for measures and dimensions
		 * */
		_createDataSetMap: function (poDimensions, poMeasures, psLayoutName, psModel) {
			var mDataSetMap = {};
			var oLayoutModel = sap.ui.getCore().getModel("layout");

			//Starting with Dimensions
			for (var sDimensionName in poDimensions) {
				if (poDimensions.hasOwnProperty(sDimensionName)) {
					var oDimension = poDimensions[sDimensionName];
					var sDimensionText = oLayoutModel.getProperty("/" + psLayoutName + "/Fields/" + sDimensionName + "/Text");
					var fnFormatter;
					var sName = "";
					var sDataType = "";
					for (var sProperty in oDimension) {
						if (oDimension.hasOwnProperty(sProperty)) {
							if (sProperty === "formatter") {
								fnFormatter = oDimension[sProperty];
							}
							if (sProperty === "values") {
								sName = oDimension[sProperty];
							}
							if (sProperty === "dataType") {
								sDataType = oDimension[sProperty];
							}
						}
					}
					if (sName === "") {
						sName = sDimensionText;
					}

					var mSetting = {
						name: sName,
						value: {
							path: psModel + ">" + sDimensionName
						}
					};

					if (typeof fnFormatter !== "undefined") {
						mSetting.value.formatter = fnFormatter;
					}

					if (sDataType !== "") {
						mSetting.dataType = sDataType;
					}

					mDataSetMap[sDimensionName] = new sap.viz.ui5.data.DimensionDefinition(mSetting);

				}
			}

			//Continue with Measures
			for (var sMeasureName in poMeasures) {
				if (poMeasures.hasOwnProperty(sMeasureName)) {
					var oMeasure = poMeasures[sMeasureName];
					var sMeasureText = oLayoutModel.getProperty("/" + psLayoutName + "/Fields/" + sMeasureName + "/Text");
					fnFormatter = undefined;
					sName = "";
					for (sProperty in oMeasure) {
						if (oMeasure.hasOwnProperty(sProperty)) {
							if (sProperty === "formatter") {
								fnFormatter = oMeasure[sProperty];
							}
							if (sProperty === "values") {
								sName = oMeasure[sProperty];
							}
						}
					}
					if (sName === "") {
						sName = sMeasureText;
					}

					mSetting = {
						name: sName,
						value: {
							path: psModel + ">" + sMeasureName
						}
					};

					if (typeof fnFormatter !== "undefined") {
						mSetting.value.formatter = fnFormatter;
					}

					mDataSetMap[sMeasureName] = new sap.viz.ui5.data.MeasureDefinition(mSetting);
				}
			}

			return (mDataSetMap);
		},

		/* *
		 * Making the connection between the diagram and our model
		 * @private
		 * @param {map} pmDataSetMap The dataset we need for our diagram
		 * @reutnr {object} oDataSet The dataset object containing all relevant measures and dimensions
		 * */
		_createDataSet: function (pmDataSetMap, psModel, psEntitySet, paFilters) {
			var oObject = {};
			oObject = {
				data: {
					path: psModel + ">/" + psEntitySet
				}
			};
			if (paFilters && paFilters.length !== 0) {
				oObject.data.filters = paFilters;
			}
			var oDataset = new sap.viz.ui5.data.FlattenedDataset(oObject);

			//Starting with the dimensions
			for (var sMap in pmDataSetMap) {
				if (pmDataSetMap.hasOwnProperty(sMap)) {
					var oMap = pmDataSetMap[sMap];
					if (oMap instanceof sap.viz.ui5.data.DimensionDefinition) {
						oDataset.addDimension(oMap);
					}
				}
			}

			//Continue with Measures
			for (sMap in pmDataSetMap) {
				if (pmDataSetMap.hasOwnProperty(sMap)) {
					oMap = pmDataSetMap[sMap];
					if (oMap instanceof sap.viz.ui5.data.MeasureDefinition) {
						oDataset.addMeasure(oMap);
					}
				}
			}

			return oDataset;
		},

		/* *
		 * Internal method used for creating the diagram itself
		 * @private
		 * @param {map} pmFeedMap All the feeds used for our diagram itself
		 * @param {map} pmDataSetMap All the datasets we need for our diagram
		 * @param {object} poChartModel The model containing all the relevant data for our diagram
		 * @param {string} psDiagram The string containing the diagram which should be created
		 * @param {object} poChartProperties The properties used for the chart
		 * @param {string} psModel The name of the model
		 * @param {string} psEntitySet The name of the entity set for wich the read should be made
		 * @param {array} paFilters Array of filters neeeded for the read itself
		 * @return {object} oVizFrame The viz frame which should be displayed later on
		 * */
		_createDiagram: function (pmFeedMap, pmDataSetMap, psDiagram, poChartProperties, psModel, psEntitySet, paFilters) {
			var oVizFrame = new sap.viz.ui5.controls.VizFrame();
			var oVizPopover = new sap.viz.ui5.controls.Popover();
			oVizFrame.setDataset(this._createDataSet(pmDataSetMap, psModel, psEntitySet, paFilters));

			//Adding the feeds to our diagram
			for (var sFeed in pmFeedMap) {
				if (pmFeedMap.hasOwnProperty(sFeed)) {
					var oFeed = pmFeedMap[sFeed];
					oVizFrame.addFeed(oFeed);
				}
			}

			oVizFrame.setVizProperties(poChartProperties);
			oVizFrame.setVizType(psDiagram);
			oVizPopover.connect(oVizFrame.getVizUid());

			//Time for closing the busy indicator if open
			poBusyDialog.closeBusyDialog();
			return (oVizFrame);
		},

		/**
		 * convert object from backend to and model filter. 
		 * @public
		 * @param {array} paFilter filter objects
		 * @return {array} aFilter model filters
		 */
		convertToModelFilter: function (paFilter) {
			var aFilter = [];
			if (paFilter) {
				for (var i = 0; i < paFilter.length; i++) {
					var oFil = paFilter[i];
					var oFilter = new sap.ui.model.Filter(oFil.Name, oFil.Operator, oFil.Low, oFil.High);
					aFilter.push(oFilter);
				}

			}
			return aFilter;
		},

		/**
		 * generate binding path. 
		 * @public
		 * @param {string} psModelName name of the model
		 * @param {string} psProperty name of the property
		 * @param {object} poObject object with the binding context
		 * @return {string} sBindingPath binding path
		 */
		generateBindingPath: function (psModelName, psProperty, poObject) {
			var sBindingPath = "";
			if (psProperty && psProperty !== "") {
				var oContext;
				if (psModelName && psModelName !== "") {
					oContext = poObject.getBindingContext(psModelName);
					sBindingPath += psModelName + ">";
				} else {
					oContext = poObject.getBindingContext();
				}
				sBindingPath += ((oContext) ? "" : "/") + psProperty;
			}
			return sBindingPath;
		},

		/**
		 * Functionality to get navigation items from model
		 * @public
		 * @return {array} aNavItems navigation items
		 * */
		getNavItems: function (psAttributeKey) {
			//get nav items
			var aNavItems = sap.ui.getCore().getModel('layout').getProperty("/Navigation");
			aNavItems = aNavItems.filter(function (oNavItem) {
				return oNavItem.FieldnameAttribut.toLowerCase() === psAttributeKey.toLowerCase();
			});

			return aNavItems;
		},

		/**
		 * Functionality to buildPopover onAfterRendering
		 * @public
		 * @param {array} paNavItem navigation items
		 * @param {object} poContext context
		 * @param {string} poContext text
		 * @return {object} oPopover popover
		 * */
		buildNavigationPopover: function (paNavItem, poContext, paText) {
			var oTable = new sap.m.Table();
			oTable.setWidth("12em");
			var oColumn = new sap.m.Column();
			oTable.addColumn(oColumn);
			oTable.attachItemPress(this._navigateToApp.bind({
				helper: this,
				context: poContext,
				navItem: paNavItem,
				text: paText
			}));

			for (var i = 0; i < paNavItem.length; i++) {
				var oNavItem = paNavItem[i];
				var oColumnListItem1 = new sap.m.ColumnListItem({
					type: "Active"
				});
				oColumnListItem1.addCell(new sap.m.Text({
					text: oNavItem.Text
				}));
				oTable.addItem(oColumnListItem1);
			}

			var oPopover = new sap.m.Popover({
				showHeader: false
			});
			oPopover.addContent(oTable);

			return oPopover;
		},

		/**
		 * Functionality to navigate to another app depending von url
		 * @private
		 * */
		_navigateToApp: function (oEvent) {
			var oTable = oEvent.getSource();
			var oListItem = oEvent.getParameter('listItem');
			var nIndex = oTable.indexOfItem(oListItem);
			var oNavItem = this.navItem[nIndex];
			var aParameters = oNavItem.NavigationToParameter;

			var sText = this.text;

			// build parameters object
			var oParameters = aParameters.reduce(function (accumulator, currentValue) {
				var sParameterValue = currentValue.Parametervalue;
				var sParameterName = currentValue.Parametername;
				if (currentValue.Static) {
					accumulator[sParameterName] = sParameterValue;
				} else {
					if (sParameterValue && this.context) {
						var sValue = jsonpath.query(this.context, sParameterValue);
						accumulator[sParameterName] = sValue[0] ? sValue[0] : sText;
					} else {
						accumulator[currentValue.Parametername] = sText;
					}
				}
				return accumulator;
			}.bind(this), {});

			poGeneralHelper.navToApplication(oNavItem.FieldnameUrl, "display", oNavItem.Navmode, oParameters, (sap.ui.getCore().getModel(
				"appState")) ? sap.ui.getCore().getModel("appState").getData() : undefined);

		},

		/**
		 * get control from simple form.
		 * @public
		 * @param {array} paContent content of form
		 * @param {string} psField field name
		 * @return {object} oControl control
		 */
		getControlFromSimpleForm: function (paContent, psField) {
			var oControl;
			if (psField) {
				var aContent = paContent;
				if (aContent && aContent.length > 1) {
					for (var i = 1; i < aContent.length; i = i + 2) {
						var oItem = aContent[i];
						var oContent = this.getControlFromBox(oItem, 0);
						if (oContent && oContent.getField && typeof oContent.getField === "function") {
							if (oContent.getField() === psField) {
								oControl = oContent;
								break;
							}
						}
					}
				}
			}
			return oControl;
		},

		/**
		 * get control from box.
		 * @public
		 * @param {object} poControl control
		 * @param {int} piIndex index
		 * @return {object} oControl control
		 */
		getControlFromBox: function (poControl, piIndex) {
			var oControl = poControl;
			if (poControl && !poControl.getField) {
				oControl = oItem.getItems()[piIndex];
			}
			return oControl;
		}
	};
});