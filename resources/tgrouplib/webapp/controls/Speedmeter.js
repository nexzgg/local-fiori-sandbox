/** @module com/tgroup/lib/controls */
sap.ui.define([
	"sap/ui/core/Control",
	"com/tgroup/lib/controls/lib/speedmeter/speedmeter"
], function(poControl, poGauge) {
	"use strict";

	return poControl.extend("com.tgroup.lib.controls.Speedmeter", {

		_chart: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				id: {
					type: "string",
					defaultValue: "0"
				},
				value: {
					type: "string",
					defaultValue: "0"
				},
				unit: {
					type: "string",
					defaultValue: ""
				},
				min: {
					type: "string",
					defaultValue: "0"
				},
				max: {
					type: "string",
					defaultValue: "100"
				},
				target: {
					type: "string",
					defaultValue: "0"
				},
				width: {
					type: "int",
					defaultValue: 300
				},
				tolerance: {
					type: "string",
					defaultValue: "0.33"
				},
				tolMin: {
					type: "string",
					defaultValue: "0"
				},
				tolMax: {
					type: "string",
					defaultValue: "0"
				},
				green: {
					type: "string",
					defaultValue: ""
				},
				red: {
					type: "string",
					defaultValue: ""
				},
				needle: {
					type: "string",
					defaultValue: ""
				},
				background: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function(oRM, oControl) {
			oRM.write("<html:div ");
			oRM.writeControlData(oControl);
			oRM.writeClasses();
			oRM.write("><canvas></canvas>");
			oRM.write("</html:div>");
		},

		/**
		 * on after rendering.
		 * Initialisation of the gauge
		 */
		onAfterRendering: function() {
			var oParam = {
				"width": this.getWidth(),
				"low": this.getMin(),
				"high": this.getMax(),
				"normal": this.getTarget(),
				"toleranz": this.getTolerance(),
				"tolMin": this.getTolMin(),
				"tolMax": this.getTolMax(),
				"value": this.getValue(),
				"unit": this.getUnit(),
				"green": this.getGreen(),
				"red": this.getRed(),
				"needle": this.getNeedle(),
				"background": this.getBackground()
			};

			CAT.drawingTacho2(this.getId(), oParam);
		}

	});
});