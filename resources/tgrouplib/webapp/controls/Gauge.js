/** @module com/tgroup/lib/controls */
sap.ui.define([
    "sap/ui/core/Control",
    "com/tgroup/lib/controls/lib/d3/d3",
    "com/tgroup/lib/controls/lib/c3/c3"
], function(poControl, poD3, poC3) {
    "use strict";

    return poControl.extend("com.tgroup.lib.controls.Gauge", {

      /**
       * General metadata used for the control extension
       * */
      metadata: {
          properties: {
              id: {
                  type: "string",
                  defaultValue: "0"
              },
              value: {
                  type: "string",
                  defaultValue: "0"
              },
              unit: {
                  type: "string",
                  defaultValue: ""
              },
              min: {
                  type: "string",
                  defaultValue: "0"
              },
              max: {
                  type: "string",
                  defaultValue: "100"
              },
              height: {
                  type: "int",
                  defaultValue: 130
              },
              width: {
                  type: "int",
                  defaultValue: 180
              },
              type: {
                  type: "string",
                  defaultValue: "gauge"
              },
              thickness: {
                  type: "int",
                  defaultValue: 30
              },
              showLabel: {
                  type: "boolean",
                  defaultValue: true
              },
              expand: {
                  type: "boolean",
                  defaultValue: true
              },
              colorSensibilityOrange: {
                  type: "string",
                  defaultValue: "0.2"
              },
              colorSensibilityGreen: {
                  type: "string",
                  defaultValue: "0.1"
              },
              lower: {
                  type: "string",
                  defaultValue: "0"
              },
              target: {
                  type: "string",
                  defaultValue: "50"
              },
              upper: {
                  type: "string",
                  defaultValue: "100"
              }
          }
      },

        /**
         * Functionality for rendering the control itself
         * @public
         * @param {object} poRm The object which does the rendering
         * @param {object} poControl The control itself
         * */
        renderer: function(oRM, oControl) {
            oRM.write("<html:div ");
            oRM.writeControlData(oControl);
            oRM.writeClasses();
            oRM.write(">");
            oRM.write("</html:div>");
        },

        /**
         * Returns the Gauge
         * @public
         * @param {object} _chart  The gauge
         */
        getGauge: function() {
            return this._chart;
        },

        /**
         * on after rendering.
         * Initialisation of the gauge
         */
        onAfterRendering: function() {

            //Calculating the coloring of the Gauge
            var iThresholdOrange = this.getTarget() - (this.getColorSensibilityOrange() * this.getTarget());
  					var iThresholdYellow = this.getTarget() - (this.getColorSensibilityGreen() * this.getTarget());
  					var iThresholdGreen = +this.getTarget() + +(this.getColorSensibilityGreen() * this.getTarget());
  					var iThresholdYellow2 = +this.getTarget() + +(this.getColorSensibilityOrange() * this.getTarget());

            this._chart = c3.generate({
                bindto: "#" + this.getId(),
                data: {
                    columns: [
                        [this.getUnit(), this.getValue()]
                    ],
                    type: this.getType(),

                    onclick: function(d, i) {},
                    onmouseover: function(d, i) {},
                    onmouseout: function(d, i) {}
                },
                gauge: {
                    label: {
                        format: function(value, ratio) {
                            return value;
                        },
                        show: this.getShowLabel() // to turn off the min/max labels.
                    },
                    min: this.getMin(),
                    max: this.getMax(),
                    units: this.getUnit(),
                    width: this.getThickness(),
                    expand: this.getExpand()
                },
                color: {
                    pattern: ["#FF0000", "#F97600", "#F6C600", "#60B044", "#F6C600", "#F97600", "#FF0000"], // the color levels for the values.
                    threshold: {
                        unit: "string",
                        values: [this.getLower(), iThresholdOrange, iThresholdYellow, iThresholdGreen, iThresholdYellow2, this.getUpper()]
                    }
                },
                size: {
                    width: this.getWidth(),
                    height: this.getHeight()
                }
            });
        }

    });
});