/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"sap/m/Table"
], function(poHelper, poTable) {
	"use strict";
	return poTable.extend("com.tgroup.lib.controls.Table", {

		_done: false,
		_sCurrentLayout: undefined,
		_sCurrenModelName: undefined,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				top: {
					type: "int",
					defaultValue: 0
				},
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				modelName: {
					type: "string",
					defaultValue: ""
				},
				entitySet: {
					type: "string",
					defaultValue: ""
				},
				message: {
					type: "boolean",
					defaultValue: false
				},
				editable: {
					type: "boolean",
					defaultValue: false
				}
			}
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * com/tgroup/lib/controls:renderer
		 * */
		renderer: function(oRM, oControl) {
			sap.m.TableRenderer.render(oRM, oControl);
		},

		/**
		 * Functionality for setting the layout for the table
		 * @public
		 * @param {string} psLayoutName The name of the layout
		 * */
		setLayoutName: function(psLayoutName) {
			if (psLayoutName && typeof psLayoutName !== "undefined") {
				this.setProperty("layoutName", psLayoutName, true);
				if (this._sCurrentLayout !== psLayoutName) {
					this._sCurrentLayout = psLayoutName;
					this.destroyColumns();
					this._done = false;
				}
			}
		},

		/**
		 * Functionality for setting the model for the table
		 * @public
		 * @param {string} psModelName The name of the model
		 * */
		setModelName: function(psModelName) {
			if (psModelName && typeof psModelName !== "undefined") {
				this.setProperty("modelName", psModelName, true);
				if (this._sCurrenModelName !== psModelName) {
					this._sCurrenModelName = psModelName;
					this.destroyColumns();
					this._done = false;
				}
			}
		},

		/**
		 * on after rendering. 
		 */
		onAfterRendering: function() {
			if (sap.m.Table.prototype.onAfterRendering) {
				sap.m.Table.prototype.onAfterRendering.apply(this, arguments);
			}

			if (!this._done) {
				this._renderTable();
			}
		},

		/**
		 */
		_renderTable: function() {
			var oCore = sap.ui.getCore();

			if (this.getLayoutName() && this.getEntitySet() && this.getEntitySet() !== "") {
				var sPath = poHelper.generateBindingPath(this.getModelName(), this.getEntitySet(), this);
				var oBindingInfo = {
					path: sPath
				};

				// set top. check ui table or table
				if (this.getTop() > 0) {
					oBindingInfo.length = this.getTop();
				}

				// generate the table from layout
				poHelper.getTableFromLayout(this.getId(), this.getLayoutName(), this.getModelName(), oBindingInfo, false, false,
					this, this.getEditable());

				// add message handling
				if (this.getMessage()) {
					poHelper.addShowMessageToTable(this, oCore.getModel("message"));
				}
				this._done = true;
			}
		}
	});
});