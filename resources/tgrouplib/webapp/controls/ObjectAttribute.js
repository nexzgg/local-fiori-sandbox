sap.ui.define([
	"sap/m/ObjectAttribute"
], function(poObjectAttribute) {
	"use strict";
	return poObjectAttribute.extend("com.tgroup.lib.controls.ObjectAttribute", {
		metadata: {
			defaultAggregation: "customContent"
		}
	});
});