/** @module com/tgroup/lib/controls */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper",
	"com/tgroup/lib/controls/model/formatter",
	"com/tgroup/lib/general/model/formatter",
	"sap/m/Input",
	"sap/ui/commons/RichTooltip",
	"com/tgroup/lib/login/util/loginHandler"
], function (poHelper, poFormatter, poGeneralFormatter, poInput, RichTooltip, LoginHandler) {
	"use strict";
	return poInput.extend("com.tgroup.lib.controls.Input", {
		/**
		 * Attributes for the whole controls
		 * */
		_done: false,
		_bindingTypeDone: false,

		/**
		 * General metadata used for the control extension
		 * */
		metadata: {
			properties: {
				layoutName: {
					type: "string",
					defaultValue: ""
				},
				field: {
					type: "string",
					defaultValue: ""
				},
				mandatory: {
					type: "boolean",
					defaultValue: false
				},
				bindingModel: {
					type: "string",
					defaultValue: ""
				},
				hasSuggestionItems: {
					type: "boolean",
					defaultValue: false
				},
				notWidth: {
					type: "boolean",
					defaultValue: false
				},
				key: {
					type: "string",
					defaultValue: ""
				},
				noKeyFoundClearValue: {
					type: "boolean",
					defaultValue: true
				}
			},
			events: {
				"mouseOver": {},
				"mouseOut": {}
			}
		},

		/**
		 * Functionality for hover functionality
		 * @public
		 * @param {object} poEvent The event caused by the hover
		 * */
		onmouseover: function (poEvent) {
			this.fireMouseOver();
		},

		/**
		 * Functionality for hover out functionality
		 * @public
		 * @param {object} poEvent The event caused by the mouse out
		 * */
		onmouseout: function (poEvent) {
			this.fireMouseOut();
		},

		/**
		 * Functionality for rendering the control itself
		 * @public
		 * @param {object} poRm The object which does the rendering
		 * @param {object} poControl The control itself
		 * */
		renderer: function (oRM, oControl) {
			sap.m.InputRenderer.render(oRM, oControl);
		},

		/**
		 * Functionality for setting the value
		 * @public
		 * @param {object} poValue value
		 * */
		setValue: function (poValue) {
			if (sap.m.Input.prototype.setValue) {
				sap.m.Input.prototype.setValue.apply(this, arguments);
			}
			this._setBindingType();
		},

		/**
		 * set the binding type.
		 */
		_setBindingType: function () {
			var sLayout = this.getLayoutName();
			var sField = this.getField();
			var oField = poHelper.getField(sLayout, sField);

			// Checking if we do have a falid name for the control itself
			if (!this._bindingTypeDone && sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField &&
				typeof oField !==
				"undefined") {
				// Binding
				var oBindingInfo = this.getBindingInfo("value");
				if (oBindingInfo && typeof oBindingInfo !== "undefined") {
					var oBinding = oBindingInfo.binding;
					if (oBinding && typeof oBinding !== "undefined") {
						this._bindingTypeDone = true;
						// Setting the live-update functionality for an input
						this.setValueLiveUpdate(true);
						if (poFormatter.getNoLeadingZeroes(oField.NoLeadingZeroes)) {
							this.bindProperty("value", {
								path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() : oBinding.getPath(),
								formatter: function (psValue) {
									return poGeneralFormatter.deleteLeadingZeroes(psValue);
								}
							});

							// attach live change => simulate TwoWay Binding, because if a formatter exist only one way binding possible
							this.attachLiveChange(function (poEvent) {
								var oBinding = this.getBinding("value");
								if (oBinding) {
									var oModel = oBinding.getModel();
									var sPath = oBinding.getPath();
									if (oModel && sPath) {
										var sValue = poEvent.getParameter("value");
										oModel.setProperty(sPath, sValue);
									}
								}
							});
						} else if (oField.Decimals > 0) {
							this.bindProperty("value", {
								path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() : oBinding.getPath(),
								type: new sap.ui.model.type.Float({
									maxFractionDigits: oField.Decimals
								})
							});
						} else {
							this.bindProperty("value", {
								path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() : oBinding.getPath(),
								type: poFormatter.getBindingType(oField.Type)
							});
							if (oField.InputType === poHelper.inputType.password) {
								this.setType("Password");
							} else {
								this.setType(poFormatter.getType(oField.Type));
							}
						}

						//Checking if we do have default values
						// setting the default value
						if (oField.DefaultValue && oField.DefaultValue !== "" && typeof oField.DefaultValue !== "undefined") {
							this.setValue(oField.DefaultValue);
						}
					}
				}
			}
		},

		onAfterRendering: function () {
			if (sap.m.Input.prototype.onAfterRendering) {
				sap.m.Input.prototype.onAfterRendering.apply(this, arguments);
			}
			if (!this._done) {
				var sLayout = this.getLayoutName();
				var sField = this.getField();
				var oField = poHelper.getField(sLayout, sField);

				// Checking if we do have a falid name for the control itself
				if (sLayout && typeof sLayout !== "undefined" && sField && typeof sField !== "undefined" && oField && typeof oField !==
					"undefined") {

					// Time to set the specific properties for the control (depending on the aggregation of course)
					// Important fields:
					// - Binding
					// - Type
					// - MaxLength
					// - ValueHelp
					// - Width
					// - Enabled
					// - Editable
					// - TextAlign
					// - Required
					// - Visible
					// - Tooltip

					// MaxLength
					this.setMaxLength(poFormatter.getMaxLength(oField.OutputLength));

					// has search help. multi input attach value help request
					if (oField.HasShlp === "X") {
						this.setShowValueHelp(true);
						this._addSearchHelp(this, oField);
					}

					// Width
					if (!this.getNotWidth()) {
						this.setWidth(poFormatter.getWidth(oField.Width));
					}

					// Enabled
					var bEnabled = this.getEnabled();
					// check enabled set by properties
					if (!bEnabled) {
						this.setEnabled(false);
					} else {
						this.setEnabled(poFormatter.getEnabled(oField.FieldMode));
					}

					// Editable
					var bEditable = this.getEditable();
					if (!bEditable) {
						this.setEditable(false);
					} else {
						this.setEditable(poFormatter.getEditable(oField.FieldMode));
					}

					// TextAlign
					this.setTextAlign(poFormatter.getTextAlign(oField.Align));

					// Required
					if (this.setRequired) {
						this.setRequired(poFormatter.getRequired(oField.FieldType));
					} else {
						// For version 1.28 and below
						this.setMandatory(poFormatter.getRequired(oField.FieldType));
					}
					// We also need to implement the required logic for checking it
					if (poFormatter.getRequired(oField.FieldType) === true) {
						// Add event listener for value live change
						this.attachLiveChange(function (poEvent) {
							var sValue = poEvent.getParameter("value");
							if (sValue === "") {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
							} else {
								poEvent.getSource().setValueState(sap.ui.core.ValueState.None);
							}
						});
					}

					// Visible
					this.setVisible(poFormatter.getVisible(oField.FieldMode));

					// Tooltip
					if (oField.Tooltip && typeof oField.Tooltip !== "undefined") {
						if (oField.Tooltip !== "") {
							//this.setTooltip(poFormatter.getTooltip(oField.Tooltip));
							this.setTooltip(new sap.ui.commons.RichTooltip({
								text: poFormatter.getTooltip(oField.Tooltip),
								collision: "fit"
							}));
							/*var oPopover;
							this.attachMouseOver(function() {
								if (!oPopover) {
									oPopover = new sap.m.Popover({
										placement: "Bottom",
										title: oField.Text,
										content: [
											new sap.m.Text({
												text: poFormatter.getTooltip(oField.Tooltip)
											})
										]
									});
								}
								oPopover.openBy(this);
							}.bind(this, oPopover));

							// Also attach here also the event for mouse out
							this.attachMouseOut(function() {
								if (oPopover && typeof oPopover !== "undefined") {
									oPopover.close();
									oPopover.destroy();
									oPopover = undefined;
								}
							}.bind(this, oPopover));*/
						}
					}

					// Checking for suggestion items
					if (oField.ComboBox && typeof oField.ComboBox !== "undefined") {
						this.setHasSuggestionItems(true);
						this.setShowSuggestion(true);

						var sModelPath = oField.ComboBox.ModelName;
						if (sModelPath) {
							var oModel = sap.ui.getCore().getModel(sModelPath);
							if (!oModel) {
								var mSetting = {
									disableHeadRequestForToken: true
								};
								// model not exist, create model with service
								if (oField.ComboBox.Service && oField.ComboBox.Service !== "") {
									var sConnection = LoginHandler.getConnection();
									oModel = new sap.ui.model.odata.v2.ODataModel(sConnection + oField.ComboBox.Service, mSetting);
									sap.ui.getCore().setModel(oModel, sModelPath);
								}
							}
						}

						this.setModel(oModel, sModelPath);
						var oBindingInfo = this.getBindingInfo("value");
						var oBinding = (oBindingInfo) ? oBindingInfo.binding : undefined;

						if (oBinding && typeof oBinding !== "undefined") {
							this.bindProperty("key", {
								path: (this.getBindingModel()) ? this.getBindingModel() + ">" + oBinding.getPath() + "Key" : oBinding.getPath() + "Key",
								type: poFormatter.getBindingType(oField.Type)
							});
						}
						this.bindAggregation("suggestionItems", oField.ComboBox.ModelName + ">/" + oField.ComboBox.EntitySet, new sap.ui.core.Item({
							text: "{" + oField.ComboBox.ModelName + ">" + oField.ComboBox.TextField + "}",
							key: "{" + oField.ComboBox.ModelName + ">" + oField.ComboBox.KeyField + "}"
						}));
						this.setFilterFunction(function (psTerm, oItem) {
							var sText = oItem.getText().replace(/[^a-zA-Z0-9-]/gi, "");
							var sTerm = psTerm.replace(/[^a-zA-Z0-9-]/gi, "");
							return (sText.match(new RegExp(sTerm, "i")));
						});
						this.attachSuggest(function (poEvent) {
							var sTerm = poEvent.getParameter("suggestValue");
							var aFilters = [];
							if (sTerm) {
								aFilters.push(new sap.ui.model.Filter(oField.ComboBox.TextField, sap.ui.model.FilterOperator.Contains, sTerm));
							}
							poEvent.getSource().getBinding("suggestionItems").filter(aFilters);
						});
						this.attachSuggestionItemSelected(function (poEvent) {
							var oSelectedItem = poEvent.getParameter("selectedItem");
							if (oSelectedItem) {
								var sKey = oSelectedItem.getKey();
								if (sKey && typeof sKey !== "undefined") {
									if (sKey !== "") {
										this.setKey(sKey);
									}
								}
							} else {
								this.setKey("");
							}
						}.bind(this));
						this.attachLiveChange(function (poEvent) {
							//If the value is complete empty we do have to delete the setted key
							if (poEvent.getSource().getValue() === "") {
								//The key needs always to be setted
								this.setKey("");
							}
						}.bind(this));
						this.attachChange(function () {
							// if the key is empty, so no suggestion item found => clear the value
							if (this.getNoKeyFoundClearValue() && this.getKey() === "") {
								this.setValue("");
							}
						}.bind(this))
					}
				}
				this._done = true;
			}
		},

		/**
		 * add search help event. value help dialog
		 * @private
		 * @param {object} poDisplayInput input control
		 * @param {object} poField field object
		 */
		_addSearchHelp: function (poDisplayInput, poField) {
			// set custom data with search help infos

			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "appName",
				value: poHelper.getAppName()
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "fieldname",
				value: poField.ElementName
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "title",
				value: poField.Text
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "key",
				value: poField.ElementName
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "descriptionKey",
				value: poField.ElementName
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "filterBarExpanded",
				value: "true"
			}));
			poDisplayInput.addCustomData(new sap.ui.core.CustomData({
				key: "supportMultiselect",
				value: "false"
			}));

			poDisplayInput.attachValueHelpRequest(function () {
				poHelper.onValueHelpRequest(this, "F4");
			});
		}
	});
});