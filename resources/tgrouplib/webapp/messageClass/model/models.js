/** @module com/tgroup/lib/messageClass */
sap.ui.define([
	"com/tgroup/lib/message/model/models"
], function(poMessageModel) {
	"use strict";

	return {

		/**
		 * read the messages classes for the app
		 * @public
		 * @param {String} psAppName app name
		 * @param {function} pfnSuccess success function
		 * @param {function} pfnError error function
		 */
		readMessageClasses: function(psAppName, pfnSuccess, pfnError) {
			// clear the messages
			poMessageModel.clearMessageModel();

			var oDataProvider = sap.ui.getCore().getModel("dataprovider");

			// set the app name filter
			var aFilters = [];
			var oFilter = new sap.ui.model.Filter(
				"Application",
				sap.ui.model.FilterOperator.EQ,
				psAppName
			);
			aFilters.push(oFilter);

			// Building up the map of parameters used for our gateway read
			var mParameters = {
				success: pfnSuccess,
				error: pfnError,
				filters: aFilters,
				groupId: "MessageClassGroup"
			};
			// starting the gateway read
			oDataProvider.read("/MessageClassSet", mParameters);
		}
	};

});