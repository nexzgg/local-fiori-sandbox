/** @module com/tgroup/lib/v1/messageClass */
sap.ui.define([

], function() {
	"use strict";

	return {

		/**
		 * convert odata message class for json-model.
		 * @public
		 * @param {Object} poData oData
		 * @param {JSONModel} poModel set the oData converted.
		 */
		convertMessageClass: function(poData, poModel) {
			if (poData && poData.results && poModel) {
				// convert every result object group by message class and message no.
				for (var i = 0; i < poData.results.length; i++) {
					var oObject = poData.results[i];
					// get message class
					var sArbgb = (oObject.Arbgb) ? "/" + oObject.Arbgb.replace(/\//g, "_") : "";
					// set message object
					if (!poModel.getProperty(sArbgb)) {
						poModel.setProperty(sArbgb, {});
					}

					// set object as msgnr in message class object.
					var sMsgnr = (oObject.Msgnr) ? oObject.Msgnr : "";
					poModel.setProperty(sArbgb + "/" + sMsgnr, oObject);
				}
			}
		}
	};
});