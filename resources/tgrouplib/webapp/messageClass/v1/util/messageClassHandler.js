/** @module com/tgroup/lib/v1/messageClass */
sap.ui.define([
	"com/tgroup/lib/message/v1/util/messageHandler",
	"com/tgroup/lib/messageClass/v1/util/helper",
	"com/tgroup/lib/messageClass/v1/model/models",
	"com/tgroup/lib/controls/v1/BusyDialog"
], function(poMessageHandler, poHelper, poModels, poBusyDialog) {
	"use strict";

	return {

		/**
		 * Get message class list from app Name.
		 * @public
		 * @param {String} psAppName app name
		 * @param {boolean} pbDoBusy do busy dialog open and close
		 */
		getMessageClassList: function(psAppName, pbDoBusy) {
			if (pbDoBusy) {
				poBusyDialog.openBusyDialog();
			}

			// success function
			var fnSuccess = function(oData) {
				poHelper.convertMessageClass(oData, sap.ui.getCore().getModel("messageClass"));
				if (pbDoBusy) {
					poBusyDialog.closeBusyDialog();
				}
				poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
			};

			// error function
			var fnError = function() {
				if (pbDoBusy) {
					poBusyDialog.closeBusyDialog();
				}
				poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
			};

			poModels.readMessageClasses(psAppName, fnSuccess, fnError);
		},

		/**
		 * Functionality for getting a message text depending on specific properties
		 * @public
		 * @param {mapping} pmSettings The mapping storing the settings
		 * @return {string} sMessage The message itself
		 * */
		getMessageText: function(pmSettings) {
			var sMessage = "";
			var bFound = false;
			if (pmSettings && typeof pmSettings !== "undefined") {
				var oMessageClassModel = sap.ui.getCore().getModel("messageClass");
				if (pmSettings.Number && typeof pmSettings.Number !== "undefined" && pmSettings.Number !== "") {
					if (pmSettings.ID && typeof pmSettings.ID !== "undefined" && pmSettings.ID !== "") {
						pmSettings.ID = pmSettings.ID.replace(/\//g, "_");
						var oMessage = oMessageClassModel.getProperty("/" + pmSettings.ID + "/" + pmSettings.Number);
						if (oMessage && typeof oMessage !== "undefined") {
							for (var i = 1; i <= 4; i++) {
								var sParameter = "MessageV" + i;
								if (pmSettings[sParameter]) {
									bFound = true;
									if (pmSettings[sParameter] !== "") {
										var sReplaceString = "&" + i;
										sMessage = oMessage.Text.replace(sReplaceString, pmSettings[sParameter]);
									}
								}
							}
							if (bFound === false) {
								// No parameters available --> let's just return the message
								sMessage = oMessage.Text;
							}
						}
					}
				}
			}
			return (sMessage);
		}
	};
});