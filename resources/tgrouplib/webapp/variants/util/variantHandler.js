/** @module com/tgroup/lib/variants */
sap.ui.define([
	"com/tgroup/lib/variants/util/helper",
	"com/tgroup/lib/message/util/messageHandler",
	"com/tgroup/lib/variants/model/models",
	"com/tgroup/lib/controls/BusyDialog",
	"com/tgroup/lib/message/model/models"
], function (poHelper, poMessageHandler, poModels, poBusyDialog, poMessageModels) {
	"use strict";

	return {

		/**
		 * Functionality for formatting the filter data
		 * @function
		 * @public
		 * @param {object} poFilterData The object containing the filter data
		 * */
		formatFilterData: function (poFilterData) {
			var oFilterModel = sap.ui.getCore().getModel("filter");
			var oFilterObject = {
				DefaultFilter: "",
				Filters: []
			};
			if (poFilterData && poFilterData.results && poFilterData.results.length !== 0) {
				var aFilters = poFilterData.results;
				for (var i = 0; i <= aFilters.length - 1; i++) {
					var oCurrentFilter = aFilters[i];
					if (oCurrentFilter.DefaultFilter === true) {
						oFilterObject.DefaultFilter = oCurrentFilter.FilterId;
					}
					oFilterObject.Filters.push(oCurrentFilter);
				}
				oFilterModel.setData(oFilterObject);
			} else {
				oFilterModel.setData(oFilterObject);
			}
		},

		/**
		 * Sets the filter values to the filterbar.
		 * @function
		 * @public
		 * */
		setVariantFromFilter: function (poFilterBar, paFilter) {
			var aSelectionSet = poHelper.getFilterSet(poFilterBar);

			//We have found the corresponding filter object so let's get the filterset
			//When we do have a selection set and also have the filter let's try to set the right values
			if (aSelectionSet && aSelectionSet.length && aSelectionSet.length !== 0) {
				for (var i = 0; i <= aSelectionSet.length - 1; i++) {
					var oSelectionSet = aSelectionSet[i];
					var sControl = oSelectionSet.getMetadata().getName();
					if (oSelectionSet.getField) {
						var sField = oSelectionSet.getField();

						//Compare the current selection set with the filter 
						//When we have found the right one, let's set the key!
						const aCurrent = paFilter.filter(function (oFilter) {
							return oFilter.sPath === sField;
						});
						const oValue = (aCurrent && aCurrent.length > 0) ? aCurrent[0].oValue1 : null;

						switch (sControl) {
						case "sap.m.Input":
						case "com.tgroup.lib.controls.Input":
							if (oSelectionSet.setValue) {
								if (oValue) {
									oSelectionSet.setValue(oValue);
								} else {
									oSelectionSet.setValue("");
								}
							}
							break;
						case "sap.m.DatePicker":
						case "com.tgroup.lib.controls.DatePicker":
							if (oSelectionSet.setDateValue) {
								if (oValue) {
									oSelectionSet.setDateValue(oValue);
								} else {
									oSelectionSet.setDateValue(new Date());
								}
							}
							break;
						case "sap.m.TimePicker":
						case "com.tgroup.lib.controls.TimePicker":
							if (oSelectionSet.setDateValue) {
								if (oValue) {
									oSelectionSet.setDateValue(oValue);
								} else {
									oSelectionSet.setDateValue(new Date());
								}
							}
							break;
						case "sap.m.DateTimeInput":
						case "com.tgroup.lib.controls.TimePickerOld":
							if (oSelectionSet.setDateValue) {
								if (oValue) {
									oSelectionSet.setDateValue(oValue);
								} else {
									oSelectionSet.setDateValue(new Date());
								}
							}
							break;
						case "sap.m.CheckBox":
						case "com.tgroup.lib.controls.CheckBox":
							if (oSelectionSet.setSelected) {
								if (oValue) {
									oSelectionSet.setSelected(oValue);
								} else {
									oSelectionSet.setSelected(false);
								}
							}
							break;
						case "sap.m.ComboBox":
						case "com.tgroup.lib.controls.ComboBox":
							if (oValue) {
								oSelectionSet.setSelectedKey(oValue);
							} else {
								oSelectionSet.setSelectedKey("");
							}
							break;
						case "sap.m.MultiComboBox":
						case "com.tgroup.lib.controls.MultiComboBox":
							if (oSelectionSet.setSelectedKeys) {
								if (oValue) {
									oSelectionSet.setSelectedKeys(oValue);
								} else {
									oSelectionSet.setSelectedKeys([]);
								}
							}
							break;
						}

					}

				}
			}
		},

		/**
		 * Functionality responsible for selecting a specific variant and make the selection!
		 * @function
		 * @public
		 * @param {object} poFilterBar The control itself
		 * @param {object} poEvent The event which is caused by saving
		 * @param {string} psKey The key for the new selection
		 * */
		selectVariant: function (poFilterBar, poEvent, psKey) {
			var oFilterModel = sap.ui.getCore().getModel("filter");
			var sKey;
			if (psKey && typeof psKey !== "undefined" && psKey !== "") {
				sKey = psKey;
			} else {
				sKey = poEvent.getParameter("key");
			}
			//Let's find the right object
			var aFilters = oFilterModel.getProperty("/Filters");
			var bFound = false;
			for (var i = 0; i <= aFilters.length - 1; i++) {
				var oCurrentFilter = aFilters[i];
				if (oCurrentFilter.FilterId === sKey) {
					bFound = true;
					break;
				}
			}
			//We have found the corresponding filter object so let's get the filterset
			var aSelectionSet = poHelper.getFilterSet(poFilterBar);
			//When we do have a selection set and also have the filter let's try to set the right values
			if (aSelectionSet && aSelectionSet.length && aSelectionSet.length !== 0) {
				for (i = 0; i <= aSelectionSet.length - 1; i++) {
					var oSelectionSet = aSelectionSet[i];
					var sControl = oSelectionSet.getMetadata().getName();
					if (oSelectionSet.getField) {
						var sField = oSelectionSet.getField();

						//Compare the current selection set with the filter 
						//When we have found the right one, let's set the key!
						if (oCurrentFilter) {
							for (var sProperty in oCurrentFilter) {
								if (oCurrentFilter.hasOwnProperty(sProperty)) {
									if (sProperty === sField) {
										//Found!
										switch (sControl) {
										case "sap.m.Input":
										case "com.tgroup.lib.controls.Input":
											if (oSelectionSet.setValue) {
												if (bFound === true) {
													oSelectionSet.setValue(oCurrentFilter[sProperty]);
												} else {
													oSelectionSet.setValue("");
												}
											}
											break;
										case "sap.m.DatePicker":
										case "com.tgroup.lib.controls.DatePicker":
											if (oSelectionSet.setDateValue) {
												if (bFound === true) {
													oSelectionSet.setDateValue(oCurrentFilter[sProperty]);
												} else {
													oSelectionSet.setDateValue(new Date());
												}
											}
											break;
										case "sap.m.TimePicker":
										case "com.tgroup.lib.controls.TimePicker":
											if (oSelectionSet.setDateValue) {
												if (bFound === true) {
													oSelectionSet.setDateValue(oCurrentFilter[sProperty]);
												} else {
													oSelectionSet.setDateValue(new Date());
												}
											}
											break;
										case "sap.m.DateTimeInput":
										case "com.tgroup.lib.controls.TimePickerOld":
											if (oSelectionSet.setDateValue) {
												if (bFound === true) {
													oSelectionSet.setDateValue(oCurrentFilter[sProperty]);
												} else {
													oSelectionSet.setDateValue(new Date());
												}
											}
											break;
										case "sap.m.CheckBox":
										case "com.tgroup.lib.controls.CheckBox":
											if (oSelectionSet.setSelected) {
												if (bFound === true) {
													if (oCurrentFilter[sProperty] === "X") {
														oSelectionSet.setSelected(true);
													} else {
														oSelectionSet.setSelected(false);
													}
												} else {
													oSelectionSet.setSelected(false);
												}
											}
											break;
										case "sap.m.ComboBox":
										case "com.tgroup.lib.controls.ComboBox":
											if (bFound === true) {
												oSelectionSet.setSelectedKey(oCurrentFilter[sProperty]);
											} else {
												oSelectionSet.setSelectedKey("");
											}
											break;
										case "sap.m.MultiComboBox":
										case "com.tgroup.lib.controls.MultiComboBox":
											if (oSelectionSet.setSelectedKeys) {
												if (bFound === true) {
													oSelectionSet.setSelectedKeys(oCurrentFilter[sProperty]);
												} else {
													oSelectionSet.setSelectedKeys([]);
												}
											}
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		},

		/**
		 * Functionality for saving a variant
		 * @function
		 * @public
		 * @param {object} poFilterBar The control itself
		 * @param {object} poEvent The event which is caused by saving 
		 * @param {array} paExceptions Array containing values which should not be stored
		 * @param {string} psModelName The model for the requests
		 * */
		saveVariant: function (poFilterBar, poEvent, paExceptions, psModelName) {
			//First step is to get the current selection in the filterbar
			if (poFilterBar) {
				var aFilters = poHelper.getFilters(poFilterBar);
				if (aFilters && aFilters.length && aFilters.length !== 0) {
					var aVariantFilters = [];
					//Checking if we do have specific exceptions which should not be part of the filter
					if (paExceptions && paExceptions.length && paExceptions.length !== 0) {
						for (var i = 0; i <= aFilters.length - 1; i++) {
							var oCurrentFilter = aFilters[i];
							var bFound = false;
							for (var j = 0; j <= paExceptions.length - 1; j++) {
								var sException = paExceptions[j];
								if (oCurrentFilter.sPath === sException) {
									bFound = true;
									break;
								}
							}
							if (bFound === false) {
								aVariantFilters.push(oCurrentFilter);
							}
						}
					} else {
						aVariantFilters = aFilters;
					}
					//Checking if we have an update or a save
					var oParameters = poEvent.getParameters();
					var oFilter = {
						DefaultFilter: oParameters.def,
						Name: oParameters.name
					};
					for (i = 0; i <= aVariantFilters.length - 1; i++) {
						oFilter[aVariantFilters[i].sPath] = aVariantFilters[i].oValue1;
					}
					var fnGlobal = function () {
						poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
					};

					var fnSuccess = function (poData) {
						var oFilterModel = sap.ui.getCore().getModel("filter");
						var aModelFilters = oFilterModel.getProperty("/Filters");
						aModelFilters.push(poData);
						oFilterModel.setProperty("/Filters", aModelFilters);
						poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
					};

					if (oParameters.overwrite === false) {
						//Save
						poModels.saveFilter(oFilter, fnSuccess, fnGlobal, psModelName);
					} else {
						//Update
						oFilter.FilterId = oParameters.key;
						poModels.updateFilter(oFilter, fnGlobal, psModelName);
					}
				}
			}
		},

		/**
		 * Functionality for managing variants
		 * @function
		 * @public
		 * @param {object} poFilterBar The control itself
		 * @param {object} poEvent The event which is caused by managing it 
		 * @param {string} psModelName The model for the requests
		 * */
		manageVariant: function (poFilterBar, poEvent, psModelName) {
			var oParameters = poEvent.getParameters();
			var aDeleted = oParameters.deleted;
			var aRenamed = oParameters.renamed;
			var oFilterModel = sap.ui.getCore().getModel("filter");
			var aFilters = oFilterModel.getProperty("/Filters");

			var fnError = function () {
				poBusyDialog.closeBusyDialog();
				poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
			};

			var fnSuccess = function () {
				poBusyDialog.closeBusyDialog();
				poMessageHandler.showMessage(sap.ui.getCore().getModel("message"));
			};

			var mParameters = {
				groupId: "Filters",
				batchGroupId: "Filters",
				success: fnSuccess,
				error: fnError
			};

			var mActionParameters = {
				groupId: "Filters",
				batchGroupId: "Filters"
			};

			//Start with the updates ones
			var oFilterObject = {};
			var sKey;
			var oModel = sap.ui.getCore().getModel(psModelName);
			for (var i = 0; i <= aRenamed.length - 1; i++) {
				//Building up the filter objects itself
				var oCurrentFilter = aRenamed[i];
				oFilterObject = {
					FilterId: oCurrentFilter.key,
					Name: oCurrentFilter.name
				};
				sKey = "(FilterId='" + oFilterObject.FilterId + "')";
				oModel.update("/FilterSet" + sKey, oFilterObject, mActionParameters);
			}

			//Continue with the default filter
			if (oParameters.def !== "") {
				//Let's find the right filter object from the model
				var sDefaultKey;
				if (oParameters.def === "*standard*") {
					//Find the current default filter because it needs to be deleted
					for (i = 0; i <= aFilters.length - 1; i++) {
						if (aFilters[i].DefaultFilter === true) {
							oFilterObject = {
								FilterId: aFilters[i].FilterId,
								DefaultFilter: false,
								Name: aFilters[i].Name
							};
							sKey = "(FilterId='" + aFilters[i].FilterId + "')";
							oModel.update("/FilterSet" + sKey, oFilterObject, mActionParameters);
							sDefaultKey = "";

							//Also set the default filter in our model
							aFilters[i].DefaultFilter = false;
							oFilterModel.setProperty("/Filters", aFilters);
							break;
						}
					}
				} else {
					for (i = 0; i <= aFilters.length - 1; i++) {
						if (aFilters[i].FilterId === oParameters.def) {
							oFilterObject = {
								FilterId: aFilters[i].FilterId,
								DefaultFilter: true,
								Name: aFilters[i].Name
							};
							sKey = "(FilterId='" + oFilterObject.FilterId + "')";
							oModel.update("/FilterSet" + sKey, oFilterObject, mActionParameters);
							sDefaultKey = aFilters[i].FilterId;

							//Also set the default filter in our model
							aFilters[i].DefaultFilter = true;
							oFilterModel.setProperty("/Filters", aFilters);
							break;
						}
					}
				}
				//We do also have to make a new selection in our filterbar
				this.selectVariant(poFilterBar, poEvent, sDefaultKey);
			}

			//Go on with the deleted ones
			var aDeletedIds = [];
			for (i = 0; i <= aDeleted.length - 1; i++) {
				sKey = "(FilterId='" + aDeleted[i] + "')";
				oModel.remove("/FilterSet" + sKey, mActionParameters);
				aDeletedIds.push(aDeleted[i]);
			}
			if (aDeletedIds.length !== 0) {
				var aNewFilters = [];
				for (i = 0; i <= aFilters.length - 1; i++) {
					var bFound = false;
					for (var j = 0; j <= aDeletedIds.length - 1; j++) {
						if (aFilters[i].FilterId === aDeletedIds[j]) {
							bFound = true;
						}
					}
					if (bFound === false) {
						aNewFilters.push(aFilters[i]);
					}
				}
				oFilterModel.setProperty("/Filters", aNewFilters);
			}

			poBusyDialog.openBusyDialog();
			poMessageModels.clearMessageModel();

			//Setting the batch group
			if (oModel.getDeferredBatchGroups) {
				var aDeferredBatchGroups = oModel.getDeferredBatchGroups();
				aDeferredBatchGroups.push("Filters");
				oModel.setDeferredBatchGroups(aDeferredBatchGroups);
			}
			if (oModel.getDeferredGroups) {
				var aDeferredGroups = oModel.getDeferredGroups();
				aDeferredGroups.push("Filters");
				oModel.setDeferredGroups(aDeferredGroups);
			}

			oModel.submitChanges(mParameters);
		}
	};

});