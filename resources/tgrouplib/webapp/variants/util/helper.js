/** @module com/tgroup/lib/variants */
sap.ui.define([
	"com/tgroup/lib/controls/util/helper"
], function(poControlsHelper) {
	"use strict";

	return {
		/**
		 * get filters from filterbar
		 * @public
		 * @param {object} poFilterBar filter bar control
		 * @return {array} aFilters
		 */
		getFilters: function(poFilterBar, paSelectionSet) {
			var aSelectionSet;
			if (paSelectionSet){
				aSelectionSet = paSelectionSet;
			}else{
				aSelectionSet = this.getFilterSet(poFilterBar);
			}
			var aFilters = [];
			if (aSelectionSet && aSelectionSet.length !== 0) {
				aFilters = poControlsHelper.getFilterList(aSelectionSet);
			}
			return (aFilters);
		},

		
		/**
		 * Functionality for getting the filter set
		 * @function
		 * @public
		 * @param {object} poFilterBar filter bar control
		 * @return {array} aSelectionSet The selection set of the filterbar
		 * */
		getFilterSet: function(poFilterBar){
			var aFilters = poFilterBar.getFilterGroupItems();
			var aSelectionSet = [];
			for (var i = 0; i <= aFilters.length - 1; i++) {
				aSelectionSet.push(poFilterBar.determineControlByFilterItem(aFilters[i]));
			}
			return(aSelectionSet);	
		}
	};
});