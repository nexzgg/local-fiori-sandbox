/** @module com/tgroup/lib/variants */
sap.ui.define([], function() {
	"use strict";

	return {
		/**
		 * Functionality for saving the filter object
		 * @function
		 * @public
		 * @param {object} poFilterObject The object containing our filter data
		 * @param {function} pfnCallBack The function for handling callbacks
		 * @param {string} psModelName The name of the model
		 * */
		saveFilter: function(poFilterObject, pfnSuccess, pfnCallBack, psModelName) {
			var oModel = sap.ui.getCore().getModel(psModelName);

			var mParameters = {
				success: pfnSuccess,
				error: pfnCallBack
			};

			if (oModel) {
				oModel.create("/FilterSet", poFilterObject, mParameters);
			}
		},

		/**
		 * Functionality for updating the filter object
		 * @function
		 * @public
		 * @param {object} poFilterObject The object containing our filter data
		 * @param {function} pfnCallBack The function for handling callbacks
		 * @param {string} psModelName The name of the model
		 * */
		updateFilter: function(poFilterObject, pfnCallBack, psModelName) {
			var oModel = sap.ui.getCore().getModel(psModelName);

			var mParameters = {
				success: pfnCallBack,
				error: pfnCallBack
			};

			if (oModel) {
				oModel.update("/FilterSet('" + poFilterObject.FilterId + "')", poFilterObject, mParameters);
			}
		}
	};

});