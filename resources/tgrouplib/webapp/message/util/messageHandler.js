/** @module com/tgroup/lib/message */
sap.ui.define([
	"sap/m/MessageToast",
	"sap/m/MessageBox"
], function(poMessageToast, poMessageBox) {
	"use strict";

	return {

		_oMessagePopover: undefined,
		_sModulePath: "com.tgroup.lib.message",

		/**
		 * Method for showing backend messages
		 * @public
		 * @param {object} poMessageModel The model containing our messages
		 * @param {boolean} pbLast show the last message
		 * */
		showMessage: function(poMessageModel, pbLast) {
			var aMessages = poMessageModel.getData();
			// aMessages = this.filterMessageByClass(aMessages);
			if (aMessages.length !== 0) {
				var oMessage;
				if (!pbLast) {
					aMessages = this._sortMessagesByType(aMessages);
					poMessageModel.setData(aMessages);
					oMessage = aMessages[0];
				} else {
					oMessage = aMessages[aMessages.length - 1];
				}
				switch (oMessage.type) {
					case sap.ui.core.MessageType.Information:
					case sap.ui.core.MessageType.Success:
						poMessageToast.show(oMessage.message);
						break;
					case sap.ui.core.MessageType.Error:
						if (poMessageBox.error) {
							poMessageBox.error(oMessage.message);
						} else {
							// fallback older version
							poMessageBox.show(oMessage.message, {
								icon: sap.m.MessageBox.Icon.ERROR,
								actions: sap.m.MessageBox.Action.CLOSE
							});
						}
						break;
					default:
						if (poMessageBox.warning) {
							poMessageBox.warning(oMessage.message);
						} else {
							// fallback older version
							poMessageBox.show(oMessage.message, {
								icon: sap.m.MessageBox.Icon.WARNING,
								actions: sap.m.MessageBox.Action.CLOSE
							});
						}
				}
			}
		},

		/**
		 * Method for getting specific messages and add it in our message model
		 * @public
		 * @param {array} paMessage list of messages
		 * @param {object} poMessageModel The model containing our messages
		 * @param {object} poMessageClassModel The model containing all our messages of the message classes
		 * @param {boolean} pbShowMessage flag call show message
		 * */
		addMessageList: function(paMessage, poMessageModel, poMessageClassModel, pbShowMessage) {
			if (poMessageModel && typeof poMessageModel !== "undefined" && poMessageClassModel && typeof poMessageClassModel !== "undefined") {
				if (paMessage && paMessage.length > 0) {
					var aMessages = [];
					for (var j = 0; j < paMessage.length; j++) {
						var oMsg = paMessage[j];
						// Checking if we do have the right information for getting the message
						if (oMsg.Type && oMsg.Type !== "" && oMsg.Class && oMsg.Class !== "" && oMsg.Msgnr && oMsg.Msgnr !== "") {
							// We do have the right message in our message class model
							var sMessageClass = oMsg.Class.replace(/\//g, "_");
							var oMessages = poMessageClassModel.getProperty("/" + sMessageClass);
							if (oMessages && typeof oMessages !== "undefined") {
								var oMessage = {};
								for (var sProperty in oMessages) {
									if (oMessages.hasOwnProperty(sProperty)) {
										if (sProperty === oMsg.Msgnr) {
											oMessage = oMessages[sProperty];
											break;
										}
									}
								}
								// Checking if we have found a message object
								if (oMessage && oMessage.Text) {
									var bFound = false;
									var sText = oMessage.Text;
									// We do also have to check if we do have parameters for setting the in the message
									for (var i = 1; i <= 4; i++) {
										var sParameter = "Param" + i;
										if (oMsg[sParameter]) {
											if (oMsg[sParameter] !== "") {
												bFound = true;
												var sReplaceString = "&" + i;
												sText = sText.replace(sReplaceString, oMsg[sParameter]);
											}
										}
									}
									if (bFound === false) {
										sText = oMessage.Text;
									}

									// Time to build up our message model object
									aMessages.push({
										code: oMsg.Class + "/" + oMsg.Msgnr,
										type: this._getMessageType(oMsg.Type),
										message: sText,
										description: sText
									});
								}
							}
						}
					}
					poMessageModel.setData(aMessages);

					// Time to show the message
					if (pbShowMessage) {
						this.showMessage(poMessageModel);
					}
				}
			}
		},

		/**
		 * Method for getting specific message and add it in our message model
		 * @public
		 * @param {mapping} pmSettings The mapping object containing all our relevant settings
		 * @param {object} poMessageModel The model containing our messages
		 * @param {object} poMessageClassModel The model containing all our messages of the message classes
		 * @param {boolean} pbShowMessage flag call show message
		 * */
		addMessage: function(pmSettings, poMessageModel, poMessageClassModel, pbShowMessage) {
			var aMessage = [];
			if (pmSettings) {
				aMessage.push(pmSettings);
				this.addMessageList(aMessage, poMessageModel, poMessageClassModel, pbShowMessage);
			}
		},

		/**
		 * Internal method for getting the corresponding message model message type
		 * @private
		 * @param {string} psType The type from the message
		 * @return {string} sType The right type for the message model
		 * */
		_getMessageType: function(psType) {
			var sType;
			if (psType && typeof psType !== "undefined") {
				if (psType !== "") {
					switch (psType) {
						case "S":
							sType = sap.ui.core.MessageType.Information;
							break;
						case "W":
							sType = sap.ui.core.MessageType.Warning;
							break;
						case "E":
							sType = sap.ui.core.MessageType.Error;
							break;
						default:
							sType = sap.ui.core.MessageType.Error;
					}
				}
			}
			return (sType);
		},

		/**
		 * filter messages by message class.
		 * @public
		 * @param {Array} paMessage message list
		 * @return {Array} aMessage filtered message list
		 */
		filterMessageByClass: function(paMessage) {
			var aMessages = [];
			if (paMessage && paMessage.length > 0) {
				aMessages = $.grep(paMessage, function(oMessage) {
					return oMessage.code.match(/^(?!.*IWBEP).*$/);
				});
			}
			return (aMessages);
		},

		/**
		 * Handles the Events "requestFailed" and "metadataFailed"
		 * @public
		 * @param {object} poModel The model for wich the events should be handled
		 */
		handleModelEvents: function(poModel) {
			this.handleRequestFailed(poModel);
			this.handleMetadataFailed(poModel);
		},

		/**
		 * Handles the Event "requestFailed"
		 * @public
		 * @param {object} poModel The model for wich the events should be handled 
		 */
		handleRequestFailed: function(poModel) {
			var fnRequestFailed = function(oEvent) {
				var oParams = oEvent.getParameters();
				var sErrorText = JSON.parse(oParams.response.responseText).error.message.value;

				// An entity that was not found in the service is also throwing a 404 error in oData.
				// We already cover this case with a notFound target so we skip it here.
				// A request that cannot be sent to the server is a technical error that we have to handle though
				if (oParams.response.statusCode !== "404" || (oParams.response.statusCode === 404 && oParams.response.responseText.indexOf(
						"Cannot POST") === 0)) {

					if (this._bMessageOpen) {
						return;
					}
					this._bMessageOpen = true;
					poMessageBox.error(
						sErrorText, {
							id: "serviceErrorMessageBox",
							details: oParams.response,
							actions: [poMessageBox.Action.CLOSE],
							onClose: function() {
								this._bMessageOpen = false;
							}.bind(this)
						}
					);
				}
			};

			poModel.attachRequestFailed(fnRequestFailed, this);
		},

		/**
		 * Handles the Event "metadataFailed"
		 * @public
		 * @param {object} poModel The model for wich the events should be handled 
		 */
		handleMetadataFailed: function(poModel) {
			var oResourceModel = new sap.ui.model.resource.ResourceModel({
					bundleUrl: jQuery.sap.getModulePath("com.tgroup.lib.message") + "/" + "i18n/i18n.properties",
					bundleLocale: sap.ui.getCore().getConfiguration().getLanguage()
				}),
				oResourceBundle = oResourceModel.getResourceBundle();

			var fnMetadataFailed = function(oEvent) {
				poMessageBox.error(
					oResourceBundle.getText("errorText"), {
						id: "metadataErrorMessageBox",
						details: oEvent.getParameters().response,
						actions: [poMessageBox.Action.RETRY, poMessageBox.Action.CLOSE],
						onClose: function(psAction) {
							if (psAction === poMessageBox.Action.RETRY) {
								poModel.refreshMetadata();
							}
						}
					}
				);
			};

			if (poModel.attachMetadataFailed) {
				poModel.attachMetadataFailed(fnMetadataFailed, this);
			}
		},

		/**
		 * Method for pressing the message popover button
		 * @public
		 * @param {object} poEvent The event caused by the press&nbsp;
		 * */
		onMessagePopoverPressed: function(poEvent) {
			if (!this._oMessagePopover) {
				this._sModulePath = "com.tgroup.lib.message";
				this._oMessagePopover = sap.ui.xmlfragment("com.tgroup.lib.message.view.fragments.MessagePopover", this);
				this._oMessagePopover.setModel(sap.ui.getCore().getModel("message"), "message");
			}
			this._oMessagePopover.openBy(poEvent.getSource());
		},

		/**
		 * Sort array by error type.
		 * @private
		 * @param {Array} paMessage message list. 
		 * @return {Array} aMessage sorted message list
		 */
		_sortMessagesByType: function(paMessage) {
			var aMessage = [],
				aErrorMessage = [],
				aWarningMessage = [],
				aDefaultMessage = [];

			for (var i = 0; i < paMessage.length; i++) {
				var oMessage = paMessage[i];
				switch (oMessage.type) {
					case sap.ui.core.MessageType.Warning:
						aWarningMessage.push(oMessage);
						break;
					case sap.ui.core.MessageType.Error:
						aErrorMessage.push(oMessage);
						break;
					default:
						aDefaultMessage.push(oMessage);
						break;
				}
			}

			aMessage = aErrorMessage.concat(aWarningMessage).concat(aDefaultMessage);

			return aMessage;
		}
	};
});