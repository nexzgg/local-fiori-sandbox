/** @module com/tgroup/lib/message */
sap.ui.define([
	"sap/ui/core/message/MessageParser",
	"sap/ui/core/message/Message"
], function(MessageParser, Message) {
	return MessageParser.extend("com.bsh.fo.cat.q14mobile.util.BatchMessageParser", {
		/**
		 * parse odata response.
		 * @public
		 * @param {object} poResponse response
		 * @param {object} poProcessor processor
		 */
		parse: function(poResponse, poProcessor) {
			// Supported message types
			var mMessageTypes = {
				info: sap.ui.core.MessageType.Information,
				warning: sap.ui.core.MessageType.Warning,
				error: sap.ui.core.MessageType.Error
			};

			var aMessages = [];
			var aBatchResponse = (poResponse && poResponse.__batchResponses) ? poResponse.__batchResponses : undefined;
			if (aBatchResponse && aBatchResponse.length && aBatchResponse.length !== 0) {
				var aMessageReponse = aBatchResponse[0].__changeResponses;
				if (aMessageReponse && aMessageReponse.length && aMessageReponse.length !== 0) {
					for (var j = 0; j <= aMessageReponse.length - 1; j++) {
						if (aMessageReponse[j].headers) {
							var oMessageObject = aMessageReponse[j].headers;
							if (oMessageObject) {
								for (var sProperty in oMessageObject) {
									if (oMessageObject.hasOwnProperty(sProperty)) {
										if (sProperty === "sap-message") {
											var oMessage = JSON.parse(oMessageObject[sProperty]);
											oMessage.type = mMessageTypes[oMessage.severity];
											var oMessageNew = new Message({
												message: oMessage.message,
												description: oMessage.message,
												type: oMessage.type,
												processor: poProcessor
											});
											aMessages.push(oMessageNew);
											if (oMessage.details && oMessage.details.length && oMessage.details.length !== 0) {
												//We do have additional messages
												var aSubMessages = oMessage.details;
												for (var k = 0; k <= aSubMessages.length - 1; k++) {
													oMessageNew = new Message({
														message: aSubMessages[k].message,
														description: aSubMessages[k].message,
														type: mMessageTypes[aSubMessages[k].severity],
														processor: poProcessor
													});
													aMessages.push(oMessageNew);
												}
											}
											break;
										}
									}
								}
							}
						}

					}
				}
			}

			// Fire the 'messageChange' event on the processor.
			poProcessor.fireMessageChange({
				oldMessages: this.aLastMessages,
				newMessages: aMessages
			});

			// Update aLastMessages
			this.aLastMessages = aMessages;
		}

	});
});