/** @module com/tgroup/lib/message */
sap.ui.define([], function() {
	"use strict";

	return {

		/**
		 * Variables used in the models
		 * */
		_oComponent: undefined,

		/**
		 * General function responsible for creating and initializing all models
		 * @public
		 * @param {object} poComponent The complete component for setting models to it
		 * */
		declareMessageModel: function(poComponent) {
			this._oComponent = poComponent;
			// Setting the message model for the core and for the component
			var oMessageModel = sap.ui.getCore().getMessageManager().getMessageModel();
			poComponent.setModel(oMessageModel, "message");
			sap.ui.getCore().setModel(oMessageModel, "message");
		},

		/**
		 * clear the messages from the message model.
		 * @public
		 */
		clearMessageModel: function() {
			var oMessageManager = sap.ui.getCore().getMessageManager();
			if (oMessageManager) {
				oMessageManager.removeAllMessages();
			}
		}
	};

});