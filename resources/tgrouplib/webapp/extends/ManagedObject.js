/**
 * Root Namespace for the jQuery plug-in provided by SAP SE. The hrportalcore extends this namespace
 * for SAPUI5 specific functionality.
 * 
 * @external "jQuery.sap"
 * @see https://sapui5.hana.ondemand.com/#docs/api/symbols/jQuery.sap.html
 */

sap.ui.define([], function() {

	jQuery.sap.extendMethod(sap.ui.base.ManagedObject, "updateBindings", function() {
		var that = this,
			sName, _fnProcessFilter, _fnProcessFilters, _fnSetFilterValueBinding, _fnInitFilterSettings;

		_fnProcessFilter = function(oFilter, oBindingInfo) {
			if (oFilter.oValue1 && typeof oFilter.oValue1.match === "function" && oFilter.oValue1.match(/\{.*\}/)) {
				var bSetFilterValueBinding,
					oContext,
					oValue1BindingInfo = sap.ui.base.BindingParser.simpleParser(oFilter.oValue1),
					bIsContext = !(oValue1BindingInfo.path && oValue1BindingInfo.path.substring(0, 1) === "/"),
					oModel = that.getModel(oBindingInfo.model);
				oFilter.oValue1BindingInfo = oValue1BindingInfo;

				// Check if model or context is available 
				if (bIsContext) {
					oContext = that.getBindingContext(oValue1BindingInfo.model);
					bSetFilterValueBinding = oModel && oContext;
				} else {
					bSetFilterValueBinding = !!oModel;
				}

				// Process setter 
				if (bSetFilterValueBinding) {
					_fnSetFilterValueBinding(that, oFilter, oContext, oBindingInfo);
				} else {
					that.attachModelContextChange(arguments, function(oEvent, args) { // model not yet set; try again when context changed 
						_fnProcessFilter.apply(this, args);
					}, this);
				}
			}
			if (oFilter.oValue1Binding) { // Binding ready - set fixed value 
				oFilter.oValue1 = oFilter.oValue1Binding.getModel().getProperty(oFilter.oValue1Binding.getPath(), oFilter.oValue1Binding.getContext());
				oFilter.fnTest = null; // reset fnTest (this seems like cheating...) 
			}
		};

		/*		 
		 * Loop through filters and process non-MultiFilters for possible property binding		 
		 */
		_fnProcessFilters = function(aFilters, oBindingInfo) {
			for (var i = 0; i < aFilters.length; i++) {
				if (aFilters[i]._bMultiFilter) {
					_fnProcessFilters(aFilters[i].aFilters, oBindingInfo);
				} else {
					_fnProcessFilter(aFilters[i], oBindingInfo);
				}
			}
		};

		/*		 
		 * Set binding on filter once model is available.		 
		 * Binding is set as property oValue1Binding on filter.		 
		 */
		_fnSetFilterValueBinding = function(_oObject, oFilter, oContext, oBindingInfo) {
			// Keep binding as property on filter		 
			oFilter.oValue1Binding = new sap.ui.model.PropertyBinding(
				_oObject.getModel(oFilter.oValue1BindingInfo.model),
				oFilter.oValue1BindingInfo.path,
				oContext
			);
			// Listen for changes the the bound property and update the filter 
			oFilter.oValue1Binding.attachChange(function() {
				_fnProcessFilters(this.filters);
				var _oBinding = _oObject.getBinding(this.name),
					bFilter = !!_oBinding;

				// Avoid OData v2 lacks 
				if (jQuery.sap.instanceOf(_oBinding, "sap.ui.model.odata.v2.ODataListBinding")) {
					var sOldFilterParams = _oBinding.sFilterParams,
						sNewFilterParams;
					_oBinding.createFilterParams(this.filters);
					sNewFilterParams = _oBinding.sFilterParams;
					bFilter = sOldFilterParams !== sNewFilterParams;
					_oBinding.sFilterParams = sOldFilterParams;
				}

				// Filter it 
				if (bFilter) {
					_oBinding.filter(this.filters, "Application");
				}
			}.bind({
				name: sName,
				filters: oBindingInfo.filters
			}));
		};

		/* 
		 * Checks whether a binding can be created for the given oBindingInfo 
		 * @param oBindingInfo 
		 * @returns {boolean} 
		 * @ported from 1.. 
		 */
		var _fnCreate = function(_oBindingInfo) {
			var aParts = _oBindingInfo.parts,
				i;

			if (aParts) {
				for (i = 0; i < aParts.length; i++) {
					if (!that.getModel(aParts[i].model)) {
						return false;
					}
				}
				return true;
			} else { // List or object binding 
				return !!that.getModel(_oBindingInfo.model);
			}
		};

		// Initialize recursively filter settings and convert objects to sap.ui.model.Filter's 
		_fnInitFilterSettings = function(aFilters) {
			if (jQuery.isArray(aFilters) || (typeof aFilters === "object" && (aFilters = [aFilters]))) {
				return aFilters.map(function(oSettings) {
					var oFilter, _sKey, _oSettings = {},
						_oExtends = {};
					jQuery.each(oSettings, function(sKey, oValue) {
						if (sKey && sKey.substring(0, 1) === "_") {
							_oExtends[sKey] = oValue;
						} else if (sKey === "aFilters" && jQuery.isArray(oValue)) {
							_oSettings.filters = _fnInitFilterSettings(oValue);
						} else {
							_sKey = sKey.slice(1);
							_sKey = _sKey.slice(0, 1).toLowerCase() + _sKey.slice(1);
							_oSettings[_sKey] = oValue;
						}
					});
					oFilter = new sap.ui.model.Filter(_oSettings);
					jQuery.extend(oFilter, oSettings);
					return oFilter;
				});
			}
			return [];
		};

		// create property and aggregation bindings if they don't exist yet 
		var oBI;
		for (var sIndex in this.mBindingInfos) {
			if (this.mBindingInfos.hasOwnProperty(sIndex)) {
				oBI = this.mBindingInfos[sIndex];
				sName = sIndex;
				if (!oBI.binding && _fnCreate(oBI) && oBI.factory && oBI.filters) {
					if (oBI.copyFilters) {
						oBI.filters = _fnInitFilterSettings(jQuery.parseJSON(JSON.stringify(oBI.filters)));
					}
					_fnProcessFilters(jQuery.isArray(oBI.filters) ? oBI.filters : [oBI.filters], oBI);
				}
			}
		}
	}, true);

});