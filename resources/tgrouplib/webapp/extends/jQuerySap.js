/**
 * Root Namespace for the jQuery plug-in provided by SAP SE. The hrportalcore extends this namespace
 * for SAPUI5 specific functionality.
 * 
 * @external "jQuery.sap"
 * @see https://sapui5.hana.ondemand.com/#docs/api/symbols/jQuery.sap.html
 */

sap.ui.define([

], function() {

	/**
	 * Checks, if a given object is instance of a sType. It also check the parents class.
	 * 
	 * @param {object} oObject The object
	 * @param {string} sType The type, for example sap.m.InputBase
	 * @returns {boolean}
	 * @function external:"jQuery.sap".instanceOf
	 * @deprecated Use oObject instanceof sap.m.Dialog instead
	 */
	jQuery.sap.instanceOf = function(oObject, sType) {
		if (!oObject || !oObject.getMetadata) {
			return false;
		}
		var oP = oObject.getMetadata();
		if (oP.getName() === sType) {
			return true;
		}
		while (oP.getParent && (oP = oP.getParent())) {
			if (oP.getName() === sType) {
				return true;
			}
		}
		return false;
	};

	var oExtendMethod = {
		result: null,
		fn: null
	};

	/**
	 * Extend an existing prototype method with an own method.
	 * 
	 * @param {mixed|"result"|"fn"} mObject Namespace to the object (string) or the object itself
	 * @param {string} sMethodName The method name of the prototype
	 * @param {function} fMethod Your own method
	 * @param {boolean} [bAtEnd] If true the parent method is executed at the end of your method (or "never" for never call the parent function)
	 * @param {boolean} [bUseDirect] Set true to override the function directly from the object and not the prototype
	 * @function external:"jQuery.sap".extendMethod
	 * @example <caption>Get result of previous function (only works if bAtEnd is false</caption>
	 * var mResult = jQuery.sap.extendMethod("result");
	 * @example <caption>Get old function calle</caption>
	 * jQuery.sap.extendMethod("fn").apply(this, arguments);
	 */
	jQuery.sap.extendMethod = function(mObject, sMethodName, fMethod, bAtEnd, bUseDirect) {
		// Check getters
		if (arguments.length === 1 && typeof mObject === "string") {
			return oExtendMethod[mObject];
		}

		// Extend method
		var oObject = typeof mObject === "string" ? jQuery.sap.getObject(mObject) : mObject,
			fNoPrototype = oObject[sMethodName],
			fPrototype = oObject.prototype ? oObject.prototype[sMethodName] : null,
			fOld = bUseDirect ? fNoPrototype : fPrototype,
			fNew = function() {
				var mResult;

				// Reset results
				oExtendMethod = {
					result: undefined,
					fn: fOld
				};

				if (!bAtEnd && bAtEnd !== "never" && typeof fOld === "function") {
					oExtendMethod.result = fOld.apply(this, arguments);
				}
				mResult = fMethod.apply(this, arguments);
				if (bAtEnd && bAtEnd !== "never" && typeof fOld === "function") {
					fOld.apply(this, arguments);
				}
				return mResult;
			};

		if (bUseDirect) {
			oObject[sMethodName] = fNew;
		} else {
			oObject.prototype[sMethodName] = fNew;
		}
	};

});