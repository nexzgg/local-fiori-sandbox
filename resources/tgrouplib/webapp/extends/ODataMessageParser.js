/**
 * Root Namespace for the jQuery plug-in provided by SAP SE. The hrportalcore extends this namespace
 * for SAPUI5 specific functionality.
 * 
 * @external "jQuery.sap"
 * @see https://sapui5.hana.ondemand.com/#docs/api/symbols/jQuery.sap.html
 */

sap.ui.define([
	"sap/ui/model/odata/ODataMessageParser"
], function() {

	jQuery.sap.extendMethod(sap.ui.model.odata.ODataMessageParser, "_propagateMessages", function(aMessages, mRequestInfo, mGetEntities,
		mChangeEntities) {
		var i, sTarget;

		var mAffectedTargets = this._getAffectedTargets(aMessages, mRequestInfo.url, mGetEntities, mChangeEntities);

		// All messages with targets are part of the changed targets by definition. All Messages that
		// come back from the server belong to affected entities/sets
		// TODO: Check if this is necessary, since only messages for requested entities/sets should be returned from the service...
		for (i = 0; i < aMessages.length; ++i) {
			sTarget = aMessages[i].getTarget();
			if (!mAffectedTargets[sTarget]) {
				jQuery.sap.log.error(
					"Service returned messages for entities that were not requested. " +
					"This might lead to wrong message processing and loss of messages"
				);
				mAffectedTargets[sTarget] = true;
			}
		}

		var aRemovedMessages = [];
		var aKeptMessages = [];
		for (i = 0; i < this._lastMessages.length; ++i) {
			sTarget = this._lastMessages[i].getTarget();
			if (mAffectedTargets[sTarget]) {
				// Message belongs to targets handled/requested by this request
				aRemovedMessages.push(this._lastMessages[i]);
			} else {
				// Message is not affected, i.e. should stay
				aKeptMessages.push(this._lastMessages[i]);
			}
		}

		this.getProcessor().fireMessageChange({
			oldMessages: aRemovedMessages,
			newMessages: aMessages
		});

		this._lastMessages = aKeptMessages.concat(aMessages);
	}, "never", false);

});