/** @module com/tgroup/lib/login */
sap.ui.define([

], function () {
	"use strict";

	return {

		_host: undefined,
		_port: undefined,
		_client: undefined,

		/**
		 * do the logoff.
		 * @public
		 * @param {function} poCallback callback function
		 */
		logoff: function (poCallBack) {
			var sConnection = this.getConnection();

			var sClient = "";
			if (this._client && this._client !== "") {
				sClient = "?sap-client=" + this._client;
			}
			this._connection = sConnection;

			var sURL = sConnection + "/sap/public/bc/icf/logoff" + sClient;
			var oRequest = new XMLHttpRequest();

			oRequest.open("GET", sURL, false);
			oRequest.onreadystatechange = function () {
				if (oRequest.readyState === 4) {
					if (oRequest.status === 200) {
						poCallBack(oRequest.responseText, true);
					} else {
						poCallBack(oRequest.responseText, false);
					}
				}
			};

			try {
				oRequest.send();
			} catch (oEx) {
				jQuery.sap.log.error(oEx);
			}
		},

		/**
		 * do the login.
		 * @public
		 * @param {function} poCallback callback function
		 * @param {string} psEncodeUser encoded user password
		 * @param {string} path to host
		 */
		login: function (poCallBack, psEncodeUser, psPath) {
			var sConnection = this.getConnection();

			// add login path
			if (psPath) {
				sConnection += psPath;
			}

			var sClient = "";
			if (this._client && this._client !== "") {
				sClient = "?sap-client=" + this._client;
			}
			this._connection = sConnection;
			var sURL = sConnection + sClient;
			var oRequest = new XMLHttpRequest();

			oRequest.open("GET", sURL, false);
			oRequest.setRequestHeader("Authorization", "Basic " + psEncodeUser);
			oRequest.onreadystatechange = function () {
				if (oRequest.readyState === 4) {
					if (oRequest.status === 200) {
						poCallBack(oRequest.responseText, true);
					} else {
						poCallBack(oRequest.responseText, false);
					}
				}
			};

			try {
				oRequest.send();
			} catch (oEx) {
				jQuery.sap.log.error(oEx);
			}
		},

		/**
		 * get the host property.
		 * @public
		 * @return {string} sHost host
		 */
		getHost: function () {
			return this._host;
		},

		/**
		 * get the port property.
		 * @public
		 * @return {string} sPort port
		 */
		getPort: function () {
			return this._port;
		},

		/**
		 * get the client property.
		 * @public
		 * @return {string} sClient client
		 */
		getClient: function () {
			return this._client;
		},

		/**
		 * set property host. 
		 * @param {string} psHost host
		 */
		setHost: function (psHost) {
			this._host = psHost;
		},

		/**
		 * set property port. 
		 * @param {string} psPort port
		 */
		setPort: function (psPort) {
			this._port = psPort;
		},

		/**
		 * set property client. 
		 * @param {string} psClient client
		 */
		setClient: function (psClient) {
			this._client = psClient;
		},

		/**
		 * get connection string.
		 * @return {string} sConnection connection string
		 */
		getConnection: function () {
			var sConnection = (this._host) ? this._host : "";
			if (this._port && this._port !== "") {
				sConnection += ":" + this._port;
			}
			return sConnection;
		}
	};
});