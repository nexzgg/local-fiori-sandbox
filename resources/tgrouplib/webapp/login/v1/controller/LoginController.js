/** @module com/tgroup/lib/v1/login */
sap.ui.define([
	"com/tgroup/lib/general/v1/controller/BaseController",
	"com/tgroup/lib/login/v1/lib/base64"
], function(poBaseController, poBase64) {
	"use strict";

	return poBaseController.extend("com.tgroup.lib.login.v1.controller.LoginController", {

		_user: undefined,
		_password: undefined,
		_host: undefined,
		_port: undefined,
		_client: undefined,
		_language: undefined,
		_valid: false,
		_encodeUser: undefined,

		/**
		 * do login.
		 * @public
		 */
		onLoginPressed: function() {
			var oView = this.getView();
			var oUser = oView.byId("inpUser");
			var oPassword = oView.byId("inpPassword");
			var oHost = oView.byId("inpHost");
			var oPort = oView.byId("inpPort");
			var oClient = oView.byId("inpClient");
			var oLanguage = oView.byId("cbLanguage");
			var oMessage = this.getView().getModel("message");

			// clear message
			this.base.messageModels.clearMessageModel();

			// check input fields exist
			if (oUser && oPassword && oLanguage) {
				/** @module com/tgroup/lib/login */this._
				user = oUser.getValue();
				this._password = oPassword.getValue();
				this._language = oLanguage.getSelectedKey();
				this._host = (oHost) ? oHost.getValue() : "";
				this._port = (oPort) ? oPort.getValue() : "";
				this._client = (oPort) ? oClient.getValue() : "";

				// set data in loginhandler
				this.base.loginHandler.setHost(this._host);
				this.base.loginHandler.setPort(this._port);
				this.base.loginHandler.setClient(this._client);

				// validate
				this._validate();
			} else {
				// unknown error
				this.setValid(false);
				var aMessages = oMessage.getData();
				var oi18nLabel = this.getView().getModel("i18nLogin");
				var oBundle = oi18nLabel.getResourceBundle();
				var sMessage = oBundle.getText("error");

				aMessages.push({
					code: "Error",
					type: sap.ui.core.MessageType.Error,
					message: sMessage,
					description: sMessage
				});

				this.setValid(false);
			}

			// inputs are not valid
			if (!this.isValid()) {
				// show error messages
				this.base.messageHandler.showMessage(oMessage);
			} else {
				// encode user
				this.setEncodeUser();
			}
		},

		/**
		 * set the Authorization for the user password.
		 * @public
		 */
		setEncodeUser: function() {
			this._encodeUser = Base64.encode(this.getUser() + ":" + this.getPassword());
		},

		/**
		 * get encode user string.
		 * @public
		 * @return {sEncodeUser} sEncodeUser encoded user data
		 */
		getEncodeUser: function() {
			return this._encodeUser;
		},

		/**
		 * validate inputs mandatory.
		 */
		_validate: function() {
			var oMessage = this.getView().getModel("message");
			var aMessages = oMessage.getData();
			var oi18nLabel = this.getView().getModel("i18nLogin");
			var oBundle = oi18nLabel.getResourceBundle();
			var sMessage = "";
			this.setValid(true);

			// validate user
			if (this._user === "") {
				sMessage = oBundle.getText("mandatory", [
					oBundle.getText("user")
				]);

				aMessages.push({
					code: oBundle.getText("user"),
					type: sap.ui.core.MessageType.Error,
					message: sMessage,
					description: sMessage
				});

				this.setValid(false);
			}

			// validate password
			if (this._password === "") {
				sMessage = oBundle.getText("mandatory", [
					oBundle.getText("password")
				]);

				aMessages.push({
					code: oBundle.getText("password"),
					type: sap.ui.core.MessageType.Error,
					message: sMessage,
					description: sMessage
				});

				this.setValid(false);
			}

			oMessage.setData(aMessages);
		},

		/**
		 * get the connection string.
		 * @public
		 * @return {string} sConnection connection string
		 */
		getConnectionString: function() {
			var sConnection = this.getHost();
			if (this.getPort() !== "") {
				sConnection += ":" + this.getPort();
			}
			return sConnection;
		},

		/**
		 * get the valid property.
		 * @public
		 * @return {boolean} bValid valid
		 */
		isValid: function() {
			return this._valid;
		},

		/**
		 * get the user property.
		 * @public
		 * @return {string} sUser user
		 */
		getUser: function() {
			return this._user;
		},

		/**
		 * get the password property.
		 * @public
		 * @return {string} sPassword password
		 */
		getPassword: function() {
			return this._password;
		},

		/**
		 * get the host property.
		 * @public
		 * @return {string} sHost host
		 */
		getHost: function() {
			return this._host;
		},

		/**
		 * get the port property.
		 * @public
		 * @return {string} sPort port
		 */
		getPort: function() {
			return this._port;
		},

		/**
		 * get the client property.
		 * @public
		 * @return {string} sClient client
		 */
		getClient: function() {
			return this._client;
		},

		/**
		 * get the language property.
		 * @public
		 * @return {string} sLanguage language
		 */
		getLanguage: function() {
			return this._language;
		},

		/**
		 * set property user. 
		 * @param {string} psUser user
		 */
		setUser: function(psUser) {
			this._user = psUser;
		},

		/**
		 * set property valid. 
		 * @param {boolean} pbValid valid
		 */
		setValid: function(pbValid) {
			this._valid = pbValid;
		},

		/**
		 * set property password. 
		 * @param {string} psPassword password
		 */
		setPassword: function(psPassword) {
			this._password = psPassword;
		},

		/**
		 * set property host. 
		 * @param {string} psHost host
		 */
		setHost: function(psHost) {
			this._host = psHost;
		},

		/**
		 * set property port. 
		 * @param {string} psPort port
		 */
		setPort: function(psPort) {
			this._port = psPort;
		},

		/**
		 * set property client. 
		 * @param {string} psClient client
		 */
		setClient: function(psClient) {
			this._client = psClient;
		},

		/**
		 * set property language. 
		 * @param {string} psLanguage language
		 */
		setLanguage: function(psLanguage) {
			this._language = psLanguage;
		}

	});

});