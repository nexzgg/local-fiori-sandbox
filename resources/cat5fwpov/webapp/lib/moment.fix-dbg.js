/*global define */
// d3 = null;
var defineOld = define;
define = sap.ui.define;
sap.ui.define(["../lib/moment.min"], function () {
    define = defineOld;
	// d3 = d3v3;
    return {version: "1.0"};
});
// debugger;
// (function (global, factory) {
// 	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
// 	typeof define === 'function' && define.amd ? define([], factory) :
// 	(factory({}));
// }(this, (function () { 'use strict';
// 	exports.fix = null;
// 	oldD3 = d3;
// 	global.d3 = null; 
// 	requirejs(["../lib/d3.v4"], function fixImport(){
// 			global.d3v4 = d3;
// 		})
// 	global.d3 = oldD3;
// 	return exports
// })));