sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		
		readSensorValues: function(poModel, pfnSuccess, pfnError, paFilters) { 
			/* Building up the map of parameters used for our gateway read */
			var mParameters = {
				success: pfnSuccess,
				error: pfnError,
				filters: paFilters
			}; 
			
			/* starting the gateway read */
			poModel.read("/xCATxC_SENSOR_VALUES_QRY", mParameters);
		},
	};
});