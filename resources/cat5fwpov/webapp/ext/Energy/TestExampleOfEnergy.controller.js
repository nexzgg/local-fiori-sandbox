(function () {
		"use strict";

		/* controller for custom card  */

		sap.ui.controller("com.tgroup.cat.wp.cat5fwpov.ext.Energy.Energy", {
				onInit: function () {

				},

				onAfterRendering: function () {

				},

				onExit: function () {

				},

				onTestButtonPress: function () {
					debugger;
					console.log("error!");
						var view = this.getView();
					view.byId("realtimeChart").updateData({
						"Sensor": "1",
						"data": "85 ",
						"datetime": "2019-03-08T15:08:29,1228580"
					});
				}
			});
})();
			// sap.ui.define([
			// 	"sap/ui/core/mvc/Controller",
			// 	"sap/ui/core/ws/SapPcpWebSocket",
			// 	"com/tgroup/cat/wp/cat5fwpov/model/models",
			// 	"sap/ui/model/Filter",
			// 	"sap/ui/model/FilterOperator"
			// ], function (Controller, SapPcpWebSocket, models, Filter, FilterOperator) {
			// 	"use strict";

			// 	return Controller.extend("com.tgroup.cat.wp.cat5fwpov.ext.Energy.Energy", {
			// 		_wsurl: 'wss://sapi01ec501.team-con.de:44300/sap/bc/apc/cat/pcse_apc_1000',
			// 		_ws: null,
			// 		_wsHeartbeat: "--HEARTBEAT--",
			// 		_resizeHandler: null,

			// 		onTestButtonPress: function () {
			// 			console.log("executed...")
			// 			debugger;
			// 			var view = this.getView();
			// 			view.byId("realtimeChart").updateData({
			// 				"Sensor": "1",
			// 				"data": "85 ",
			// 				"datetime": "2019-03-08T15:08:29,1228580"
			// 			});
			// 		},

			// 		/**
			// 		 * Called when a controller is instantiated and its View controls (if available) are already created.
			// 		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
			// 		 * @memberOf com.tgroup.cat.realmon.cat5frealdash.view.RealtimeCharts
			// 		 */
			// 		onInit: function () {
			// 			var chart = this.getView().byId("realtimeChart");
			// 			chart.attachEvent("modelContextChange", this._modelContextChange, chart);
			// 			this._initMockData();
			// 			chart.bindProperty("lineItems", "push>/takt");
			// 			chart.setTimeframe("00:10:00");
			// 			if (typeof (USE_MOCK_DATA) == 'undefined' || USE_MOCK_DATA == null || !USE_MOCK_DATA) {
			// 				this.connectToAPC();
			// 				if (this._ws) {
			// 					console.log("Websocket connected.");
			// 					this.attachEvents();
			// 				}
			// 			}
			// 		},
			// 		_initDataWithOData: function (oModel, oTimeFrom, oTimeTo, sSensorTagId) {
			// 			var oPushModel = this.getOwnerComponent().getModel("push");
			// 			var filters = [];
			// 			filters.push(new Filter({
			// 				path: "Timestamp",
			// 				operator: FilterOperator.BT,
			// 				value1: moment(oTimeFrom).toDate(),
			// 				value2: moment(oTimeTo).toDate()
			// 			}));
			// 			filters.push(new Filter({
			// 				path: "Tag",
			// 				operator: FilterOperator.EQ,
			// 				value1: sSensorTagId
			// 			}));
			// 			models.readSensorValues(oModel,
			// 				function (oData, oResponse) {
			// 					var aData = oData.results.map(function (d) {
			// 						return {
			// 							"Sensor": d.Tag,
			// 							"data": d.FloatValue,
			// 							"datetime": moment.utc(moment(d.Timestamp) - moment.duration("00:" + moment().utcOffset() + ":00")).format()
			// 						};
			// 					})
			// 					aData = aData.sort(function (e, t) {
			// 						return moment(e.datetime).valueOf() - moment(t.datetime).valueOf()
			// 					})
			// 					oPushModel.setProperty("/takt", aData);
			// 				},
			// 				function (oResponse) {
			// 					console.warn("Can not read model!: " + oResponse.statusCode + " " + oResponse.responseText);
			// 				}, filters)
			// 		},
			// 		_onObjectMatched: function (oEvent) {
			// 			var that = this;
			// 			this._resizeHandler = function (oEvent) {
			// 				that.onResize(oEvent);
			// 			};
			// 			window.addEventListener("resize", this._resizeHandler);
			// 			var sensorData = this.getOwnerComponent().getModel("sensorData");
			// 			// TODO: WHY DO WE NEED TO ADD THE OFFSET?!
			// 			this._initDataWithOData(sensorData, (moment() - moment.duration("08:15:00") + moment.duration("00:" + moment().utcOffset() +
			// 				":00")), (moment() + moment.duration("00:" + moment().utcOffset() + ":00")), 1000)
			// 		},
			// 		onBeforeRouteMatched: function (oEvent) {
			// 			if (this._resizeHandler != null) {
			// 				window.removeEventListener(this._resizeHandler);
			// 				this._resizeHandler = null;
			// 			}
			// 		},
			// 		onExit: function (oEvent) {
			// 			if (this._resizeHandler != null) {
			// 				window.removeEventListener(this._resizeHandler);
			// 				this._resizeHandler = null;
			// 			}
			// 		},
			// 		onResize: function (oEvent) {
			// 			this.getView().byId("realtimeChart").rerenderChart();
			// 		},

			// 		handleSelectionChange: function (oEvent) {
			// 			var chart = this.getView().byId("realtimeChart");
			// 			var oItem = oEvent.getParameter("selectedItem");
			// 			if (oItem.getKey() === "0") {
			// 				chart.setTimeframe("00:10:00");
			// 				chart.rerenderChart();
			// 			} else if (oItem.getKey() === "1") {
			// 				chart.setTimeframe("01:00:00");
			// 				chart.rerenderChart();
			// 			} else if (oItem.getKey() === "2") {
			// 				chart.setTimeframe("04:00:00");
			// 				chart.rerenderChart();
			// 			} else if (oItem.getKey() === "3") {
			// 				chart.setTimeframe("08:00:00");
			// 				chart.rerenderChart();
			// 			}
			// 		},
			// 		onTestButtonPress: function () {
			// 			var view = this.getView();
			// 			view.byId("realtimeChart").updateData({
			// 				"Sensor": "1",
			// 				"data": "85 ",
			// 				"datetime": "2019-02-06T14:44:29,1228580"
			// 			});
			// 		},
			// 		connectToAPC: function () {
			// 			this._ws = new SapPcpWebSocket(this._wsurl, SapPcpWebSocket.SUPPORTED_PROTOCOLS.v10);
			// 		},
			// 		attachEvents: function () {
			// 			var that = this;
			// 			this._ws.attachMessage(function (oEvent) {
			// 				var sData = oEvent.getParameter("data");
			// 				if (sData !== that._wsHeartbeat) {
			// 					that.handleMessage(sData);
			// 				}
			// 			});
			// 			this._ws.attachClose(function (oEvent) {
			// 				console.warn("Websocket was closed.");
			// 				console.warn(oEvent.getParameters());
			// 				that.activateHeartbeat();
			// 			});
			// 			this._ws.attachError(function (oEvent) {
			// 				console.warn("Websocket broke.");
			// 				console.warn(oEvent.getParameters());
			// 			});
			// 			this.activateHeartbeat();
			// 		},
			// 		handleMessage: function (message) {
			// 			var oData = JSON.parse(message);
			// 			console.log(JSON.stringify(oData));
			// 			if (oData.hasOwnProperty("Sensor")) {
			// 				var pushModel = this.getOwnerComponent().getModel("push");
			// 				var takt = pushModel.getProperty("/takt");
			// 				if (typeof (takt) !== "undefined" && takt != null) {
			// 					var saveFrame = moment.duration("08:15:00");
			// 					var dateFrom = moment(moment() - saveFrame);
			// 					takt.filter(function (d) {
			// 						return moment(d.datetime).isSameOrAfter(dateFrom);
			// 					});
			// 					pushModel.setProperty("/takt", takt.concat([oData]));
			// 				}
			// 			}
			// 			// this.getView().byId("realtimeChart").updateData(oData);
			// 		},
			// 		reconnectToAPC: function () {
			// 			var oldSocket = this._ws;
			// 			setTimeout(function () {
			// 				oldSocket.destroy();
			// 			}, 1000);
			// 			this.connectToAPC();
			// 		},
			// 		closeConnection: function () {
			// 			// TODO: Implement!?
			// 			// Does this close too much: this.ws.fireClose();
			// 			// this.ws.close();
			// 		},
			// 		activateHeartbeat: function () {
			// 			var that = this;
			// 			console.log("Activate Heartbeat.");
			// 			this._wsHeartbeatId = setInterval(function () {
			// 				that._ws.send(that._wsHeartbeat);
			// 			}, 20000);
			// 		},
			// 		deactivateHeartbeat: function () {
			// 			if (typeof (this._wsHeartbeatId) !== 'undefined' && this._wsHeartbeatId != null) {
			// 				console.log("Deactivate Heartbeat.");
			// 				var id = this._wsHeartbeatId;
			// 				this._wsHeartbeatId = null;
			// 				clearInterval(id);
			// 			}
			// 		},
			// 		_initMockData: function () {
			// 			var seData = [];
			// 			var aTimeData = [];
			// 			// if(typeof(USE_MOCK_DATA) !== 'undefined' && USE_MOCK_DATA != null && USE_MOCK_DATA){
			// 			// 	var happens = moment.utc();
			// 			// 	for (var i=0; i<500; i++) {
			// 			// 		var val = (Math.random()*100);
			// 			// 		seData.push({"Sensor":"1","data":val,"datetime":happens.format("YYYY-MM-DD[T]HH:mm:ss,SSSS000")});
			// 			// 		happens = moment.utc(happens - moment.duration("00:00:05"));
			// 			// 	}
			// 			// 	seData = seData.reverse();
			// 			//  	// this.setLineItems(seData.filter(this._filterSeData, this).map(this._mapSeData, this));
			// 			//  	this.getOwnerComponent().getModel("push").setProperty("/takt", seData);
			// 			//  	// this.getView().setLineItems(seData);
			// 			//  	// this.setTimelineItems(aTimeData.filter(this._filterTimeData, this).map(this._mapTimeData, this));
			// 			//  	this.getOwnerComponent().getModel("push").setProperty("/time", aTimeData);
			// 			// }
			// 		},
			// 		// TODO: Why do we need to rerender the chart manualy if the data changes ?!
			// 		_modelContextChange: function (oEvent, oContext) {
			// 			if (typeof (this.getBinding("lineItems")) !== 'undefined') {
			// 				this.getBinding("lineItems").attachDataStateChange(function () {
			// 					if (typeof (this.svg) !== 'undefined' || this.svg != null) {
			// 						this.rerenderChart();
			// 					}
			// 				}, this);
			// 			}
			// 		},
			// 	/**
			// 	 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			// 	 * (NOT before the first rendering! onInit() is used for that one!).
			// 	 * @memberOf com.tgroup.cat.realmon.cat5frealdash.view.RealtimeCharts
			// 	 */
			// 	onBeforeRendering: function () {

			// 	},

			// 	/**
			// 	 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
			// 	 * This hook is the same one that SAPUI5 controls get after being rendered.
			// 	 * @memberOf com.tgroup.cat.realmon.cat5frealdash.view.RealtimeCharts
			// 	 */
			// 	onAfterRendering: function () {
			// 		console.log("OnAfterRendering")
			// 	},

			// 	/**
			// 	 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
			// 	 * @memberOf com.tgroup.cat.realmon.cat5frealdash.view.RealtimeCharts
			// 	 */
			// 	onExit: function () {

			// 	}
			// });

			// });