/*global d3v4*/
/*global moment*/

sap.ui.define(
	["sap/ui/core/Control","../lib/d3.v4.fix","../lib/moment.fix","../mockdata/timeline"],
	function(Control, fix, momentfix, data){
		var sExtDateProperty = "datetime";
		var sExtDateFromProperty = "datetimeFrom";
		var sExtDateToProperty = "datetimeTo";
		var sValueProperty = "data";
		return Control.extend("RealtimeLineChart", {
			 metadata : { // Properties, Aggregations & Events
				 properties : {
				 	// TODO: Add a max timeframe for which the data should be saved
				 	timeframe: {type: "string", defaultValue: "02:00:00"},
				 	width: {type: "string", defaultValue: "800"}, 
				 	height: {type: "int", defaultValue: 250},
				 	lineItems: {type: "any", defaultValue: []},
				 	timelineItems: {type: "any", defaultValue: []},
					timeformat: {type: "string", defaultValue: "YYYY-MM-DDTHH:mm:ss"},
				 	yaxisextendfactor: {type: "int", defaultValue: 1.15},
				 	productiontimesheight: {type: "int", defaultValue: 15},
				 	xaxistimeframe: {type: "int", defaultValue: 180},
				 	margintop: {type: "int", defaultValue: 20},
				 	marginright: {type: "int", defaultValue: 20},
				 	marginbottom: {type: "int", defaultValue: 30},
				 	marginleft: {type: "int", defaultValue: 40},
				 	productiontimesmargintop: {type: "int", defaultValue: 7},
				 	productiontimesmarginright: {type: "float", defaultValue: 1.5},
    			 },
				 events: {},
				 aggregations: {}
			 },
			 _getInnerWidth: function (){
			 	return this.getWidth() - this.getMarginright() - this.getMarginleft();
			 },
			 _getInnerHeight: function (){
			 	return this.getHeight() - this.getMarginbottom() - this.getMargintop();
			 },
			 _parseLocalTS2UTC: function(ts, typ) {
			 	var dUtc = moment.utc(moment(ts)).toDate();
			 	
			    var typ = typ || 'ts'; // ts = timestamp | date = jsdate-object
			
			    switch(typ){
			      case 'ts':
			        return dUtc.format(this.getTimeformat());
			        break;
			      case 'date':
			        return dUtc;
			        break;
			      default:
			        return dUtc.format(this.getTimeformat());
			    }
			 },
			 _filterSeData: function (d, i, a) {
			    var tagTsp = (sExtDateProperty in d) ? d[sExtDateProperty] : "";
				if (tagTsp !== "") {
			      var jsDate = moment.utc(tagTsp).toDate();
			      if (jsDate && Object.prototype.toString.call(jsDate) === "[object Date]" && !isNaN(jsDate)) {
			        d.date = jsDate;
			        return true;
			      }
			    }
			    return false;
			  },
			
			  _mapSeData: function (d, i, a) {
			  	return {
			      tagTsp: moment.utc(d[sExtDateProperty]).format(this.getTimeformat()),
			      date: d.date || moment.utc(d[sExtDateProperty]).toDate(),
			      tagValue: !isNaN(d.data) ? +parseFloat(d[sValueProperty]) : 0
			    };
			 },
			 _filterTimeData: function (d, i, a) {
			    var tstmpVon = d[sExtDateFromProperty] || "";
			    var tstmpBis = d[sExtDateToProperty] || "";
			    var jsDate;
			    var ttype = d.ttype || "";
			
			    if (ttype == "") {
			      return false;
			    }
			
			    if (tstmpVon !== "" && tstmpBis !== "") {
			      // von
			      jsDate = this._parseLocalTS2UTC(tstmpVon,'date');
			      if (jsDate && Object.prototype.toString.call(jsDate) === "[object Date]" && !isNaN(jsDate)) {
			        d.dateFrom = jsDate;
			      } else {
			        return false;
			      }
			
			      // bis
			      jsDate = this._parseLocalTS2UTC(tstmpBis,'date');
			      if (jsDate && Object.prototype.toString.call(jsDate) === "[object Date]" && !isNaN(jsDate)) {
			        d.dateTo = jsDate;
			      } else {
			        return false;
			      }
			      return true;
			    }
			    return false;
			 },
			 _mapTimeData: function (d, i, a) {
			    return {
			      dateFrom: d.dateFrom,
			      dateTo: d.dateTo,
			      zzZecob: d.zzZecob,
			      ttype: d.ttype
			    };
			},
			_initChart: function(){
				this.svg = d3v4.select("#time_fragment svg")
			    	.attr("width", this.getWidth)
			    	.attr("height", this.getHeight)
			    	.attr('preserveAspectRatio','xMinYMin');
				this.svgG = this.svg.append("g")
					.attr("transform", "translate(" + this.getMarginleft() + "," + this.getMargintop() + ")");
				// -> clip path
				this.svgG.append("defs").append("clipPath")
			    	.attr("id", "clip")
			    	.append("rect")
			    	.attr("width", this._getInnerWidth())
			    	.attr("height", this._getInnerHeight());
				//  -> zoom settings
				// var zoom = d3v4.zoom()
    			//  .scaleExtent([1, 32])
    			//	.translateExtent([[0, 0], [this._getInnerWidth(), this._getInnerHeight()]])
    			//	.extent([[0, 0], [this._getInnerWidth(), this._getInnerHeight()]]);
        			//.on("zoom", zoomed);

				// Init Axes
    			this.x = d3v4.scaleTime().range([0, this._getInnerWidth()]);
    			this.xAxis = d3v4.axisBottom(this.x).tickFormat(function(d, i, n) { return !n[i + 1] && d3v4.timeFormat("%H:%M")(d, i, n) === "00:00" ? "24:00" : d3v4.timeFormat("%H:%M")(d, i, n);} )
    				.tickSizeOuter(0).tickSizeInner(0).tickPadding(5);

				this.y = d3v4.scaleLinear().range([this._getInnerHeight() - this.getProductiontimesheight(), 0]);
    			this.yAxis = d3v4.axisLeft(this.y).ticks(5).tickSizeOuter(0).tickSizeInner(0).tickPadding(5);
    			var that = this;
    			this.custYAxis = function (g){that._customYAxis(g);}
				
				// Set Domain
				// TODO: Make get min/max date dynamic
				var timeframe = moment.duration(this.getTimeframe());
				var datefrom = (moment()-timeframe),
					dateto = Date.now();
				this._setDomain(datefrom, dateto);

				// -> lines / areas
				var that = this;
				this.area = d3v4.area()
    				.curve(d3v4.curveStepAfter)
    				.x(function(d) { return that.x(d.date); })
    				.y0(this._getInnerHeight() - this.getProductiontimesheight())
    				.y1(function(d) { return that.y(d.tagValue); });
				
				this.arealine = d3v4.line()
    				.curve(d3v4.curveStepAfter)
					.x(function(d) { return that.x(d.date); })
    				.y(function(d) { return that.y(d.tagValue); });
			},
			_drawLines: function() {
    			var gAreaLine = this.svgG.append("g").attr("class", "ar");
    			// TODO: Not needed if date contains only valide timestamps
    			var timeframe = moment.duration(this.getTimeframe());
				var datefrom = (moment()-timeframe);
				var dateto = Date.now();
				var lineData = this.getLineItems().filter(this._filterSeData, this).map(this._mapSeData, this);
				lineData = lineData.filter(function (d) {return d.date >= datefrom && d.date <= dateto;});
				var aTimelineData = this.getTimelineItems().filter(this._filterTimeData, this).map(this._mapTimeData, this);
				aTimelineData = this.getTimelineItems().filter(function(d) {
			    	if (d.dateFrom >= datefrom || d.dateTo >= datefrom) return true;
			    });
				
			    gAreaLine.append("path")
			    	.data([lineData])
			    	.attr("class", "area")
			    	.attr("d", this.area);

    			// add the line/path of area to g.
    			gAreaLine.append("path")
			    	.data([lineData])
			    	.attr("class", "area__line")
			    	.attr("d", this.arealine);
				var that = this;
			    gAreaLine.append("g")
			    	.attr("class", "productiontimes")
			    	.selectAll('rect')
			    	.data(aTimelineData)
			    	.enter().append('rect')
			    	.attr("class", function(d) { return "productiontime--" + d.ttype.toLowerCase(); })
			    	.attr("width", function(d) { var width = that.x(d.dateTo) - that.x(d.dateFrom)- that.getProductiontimesmarginright(); return width > 0 ? width : 0; })
			    	.attr('height', that.getProductiontimesheight() - that.getProductiontimesmargintop())
			    	.attr('x', function(d) { return that.x(d.dateFrom); })
			    	.attr('y', that._getInnerHeight() - that.getProductiontimesheight() + that.getProductiontimesmargintop());
			},
			_setDomain: function(datefrom, dateto){
				var lineData = this.getLineItems().filter(this._filterSeData, this).map(this._mapSeData, this);
				lineData = lineData.filter(function (d) {return d.date >= datefrom && d.date <= dateto;});
				this.x.domain([datefrom, dateto]);
    			this.y.domain([0, d3v4.max(lineData, function(d) { return d.tagValue; }) * this.getYaxisextendfactor()]);
			},
			// -> draw axes
			_drawAxes: function() {
    			this.svgG.append("g")
			    	.attr("class", "axis axis__x")
			    	.attr("transform", "translate(0," + (this._getInnerHeight()) + ")")
			    	.call(this.xAxis);

			    this.svgG.append("g")
			    	.attr("class", "axis axis__y")
			    	.call(this.custYAxis);
			},
			_customYAxis: function (g) {
				var s = g.selection ? g.selection() : g;
				g.call(this.yAxis);
				s.select(".domain").remove();
			},
			updateData: function(oData){
				// TODO: Verify why array needed
				var newData = Array.isArray(oData) ? oData : [oData];
			    // -> update/prepare data
			    newData = newData.filter(this._filterSeData, this).map(this._mapSeData, this);
				
				// parse data and push to sensor data array
			    this.getLineItems(this.getLineItems().concat(newData));
			
				this.rerenderChart();
			},
			resizeChart: function() {
				this.svg.attr("width", this.getWidth()).attr("height", this.getHeight());
				this.svgG.attr("width", this._getInnerWidth()).attr("height", this._getInnerHeight());
				this.x.range([0, this._getInnerWidth()]).call();
    			this.y.range([this._getInnerHeight() - this.getProductiontimesheight(), 0]).call();
    			this.svgG.select("#clip rect").attr("width", this._getInnerWidth()).attr("height", this._getInnerHeight());
			},
			rerenderChart: function(){
				this.resizeChart();
				var timeframe = moment.duration(this.getTimeframe());
				var datefrom = (moment()-timeframe);
				var dateto = Date.now();
				var lineData = this.getLineItems().filter(this._filterSeData, this).map(this._mapSeData, this);
				if(lineData.length > 0){
					// new maxDate is last entry
				    var maxDate = lineData[lineData.length-1].date;
				    // override initial start dateto
				    if (dateto < maxDate) {
				      dateto = maxDate;
				    }
				}
			
			    // process data to remove too late data items
				var aLineData = lineData.filter(function(d) {
			      if (d.date >= datefrom) return true;
			    });
			    var aTimelineData = this.getTimelineItems().filter(function(d) {
			      if (d.dateFrom >= datefrom || d.dateTo >= datefrom) return true;
			    });
			    if(aTimelineData.length > 0){
			    	aTimelineData[aTimelineData.length - 1].dateTo = maxDate;
			    }
			
			    // -> redraw
			    // bind new domain
			    this._setDomain(datefrom, dateto);
			    // area
			    this.svg.select('.area')
			       .data([aLineData])
			       .attr("d", this.area);
			    // area line
			    this.svg.select('.area__line')
			      .data([aLineData])
			      .attr("d", this.arealine);
			    // productiontimes
			    var that = this;
			    var ti = this.svg.selectAll('g.productiontimes rect')
			      .attr('x', function(d) { return that.x(d.dateFrom); })
			      .attr("width", function(d) { var width = that.x(d.dateTo) - that.x(d.dateFrom); return width > 0 ? width : 0; });
			
			    // axes
			    this.svgG.select(".axis__x")
			      .call(this.xAxis);
			    this.svgG.select(".axis__y")
			      .call(this.custYAxis);
			},
			init: function() {
				this.getWidth = function() { return $("#time_fragment").width();};
				this.getHeight = function() { return 200;};
				// this.getWidth = function() { return 1040;};
				// this.getHeight = function() { return 1040;};
			}, //Initialisierung
			onclick: function (e) {}, //Event-Handler
			renderer : function(oRm, oControl) {
				// TODO: extract ID (make it dynamic)
			 	oRm.write("<div id=\"time_fragment\">");
			 	oRm.write("<svg></svg>");
				oRm.write("</div>");
			}, //html-Anweisungen	
			onAfterRendering: function(){
				if (sap.ui.core.Control.prototype.onAfterRendering) {
    				sap.ui.core.Control.prototype.onAfterRendering.apply(this, arguments);
    			}
    			// TODO: Why does the chart sometimes disappear?(Like the renderer was recalled)
    			if(typeof(this.svg) === 'undefined' || this.svg == null || d3v4.select("#time_fragment svg").selectAll("g").empty()){
    				this._initChart();
					this._drawAxes();
					this._drawLines();
    			} else {
    				this.rerenderChart();
    			}
			}
		});
});