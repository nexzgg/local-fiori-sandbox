sap.ui.define(["sap/ui/model/Filter", "sap/ui/model/FilterOperator"], function (Filter, FilterOperator) {
	"use strict";
	return sap.ui.controller("com.tgroup.cat.wp.cat5fwpov.controller.customMain", {
		refreshInterval: null,

		// WORKS AFTER UI5 1.85
		// modifyStartupExtension: function (oCustomSelectionVariant) {
		// 	var sUrl = window.location.href;
		// 	var sToken = sUrl.split("arbpl=<").pop();
		// 	var sFilter = sToken.split(">")[0];
		//  oCustomSelectionVariant.addSelectOption("Arbpl", "I", "EQ", sFilter);
		// }

		onInit: function () {
			this.refreshInterval = setInterval(this.onRefresh, 10000, this);
		},

		onRefresh: function (oController) {
			var oModel = oController.getOwnerComponent().getModel();
			oModel.read("/LoggedInPersonnelSet", {
				success: function (response) {
					console.log(response.results);
					oModel.aBindings.forEach(function (b) {
						if (b.sPath == '/LoggedInPersonnelSet') {
							b.refresh()
						}
					})
				},
				error: function (e) {
					console.log("/LoggedInPersonnelSet refresh faild");
				}
			});
		},

		//TODO: Doesn work yet
		onAfterRendering: function () {
			//Get reference of Global FIlter
			var oGlobalFilter = this.getView().byId("ovpGlobalFilter");

			//Create JSON data to be defaulted

			var sUrl = window.location.href;
			sUrl = decodeURI(sUrl);
			var sToken = sUrl.split("arbpl=<").pop();
			var sFilter = sToken.split(">")[0];

			var oDefaultFilter = {
				Arbpl: {
					items: [],
					ranges: [{
						exclude: false,
						operation: "EQ",
						value1: sFilter,
						keyField: "Arbpl",
						tokenText: null
					}],
					value: null
				}
			};

			//Default the Goabl filter values
			oGlobalFilter.setFilterData(oDefaultFilter);
		},

	});
});