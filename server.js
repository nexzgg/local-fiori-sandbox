const express = require("express");

const sapui5 = require("sapui5-runtime");
const httpProxy = require('http-proxy');
const odataProxy = httpProxy.createProxyServer({
    target: 'http://example.com',
    secure: false
});

// initialize environment variables
require("dotenv").config();

// proxy initializtion
if (process.env.HTTP_PROXY) {
    const HttpsProxyAgent = require("https-proxy-agent");
    oConfig.agent = new HttpsProxyAgent(process.env.HTTP_PROXY);
}

let app = express();
["appconfig", "apps"].forEach(function (sPath) {
    app.use("/" + sPath, express.static(sPath));
});

app.use('/resources', express.static(sapui5));
app.use('/resources', express.static('libs'));
app.use('/test-resources', express.static('test-resources'));
app.get("/", async (req, res) => {
    res.redirect('/test-resources/fioriSandbox.html');
});

app.route("/sap/opu/odata/*$").all(function (req, res) {
    odataProxy.web(req, res);
});

app.listen(process.env.PORT || 3000);